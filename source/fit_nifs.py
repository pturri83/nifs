"""Functions to fit NIFS data.

References:
    [ref1] Schulze et al., 2018
    [ref2] Planck Collaboration et al., 2020
"""


import copy
import warnings

from astropy import cosmology, units
from astropy.modeling import models, fitting
import numpy as np

from source import cube_nifs, params_nifs


# Calculate parameters
v = params_nifs.c * (((params_nifs.z + 1) ** 2) - 1) / (((params_nifs.z + 1) **
                                                         2) + 1)
v_min = v - params_nifs.dv
v_min_b = v - (5 * params_nifs.dv)
v_max = v + params_nifs.dv
v_max_b = v + (5 * params_nifs.dv)
v_min_sqrt = np.sqrt((1 + (v_min / params_nifs.c)) / (1 - (v_min /
                                                           params_nifs.c)))
v_min_sqrt_b = np.sqrt((1 + (v_min_b / params_nifs.c)) / (1 - (v_min_b /
                                                               params_nifs.c)))
v_max_sqrt = np.sqrt((1 + (v_max / params_nifs.c)) / (1 - (v_max /
                                                           params_nifs.c)))
v_max_sqrt_b = np.sqrt((1 + (v_max_b / params_nifs.c)) / (1 - (v_max_b /
                                                               params_nifs.c)))
v_sqrt = np.array([v_min_sqrt, v_max_sqrt])
v_sqrt_b = np.array([v_min_sqrt_b, v_max_sqrt_b])


def gauss1d_prof(r, height, sigma, bkg):
    """Calculate the 1D Gaussian profile function.

    Parameters:
        :param r: Distance from the center (px)
        :type r: float, numpy.ndarray [x] (float)
        :param height: Peak
        :type height: float
        :param sigma: STD (px)
        :type sigma: float
        :param bkg: Background level
        :type bkg: float

    Returns:
        :return y: 1D Gaussian profile function
        :rtype y: float, numpy.ndarray [x] (float)
    """

    # Calculate the 1D Gaussian profile
    y = (height * np.exp(-((r / sigma) ** 2) / 2)) + bkg

    return y


def gauss2d_fit(data, pos_guess='max'):
    """Fit a 2D Gaussian to an image.

    Parameters:
        :param data: Data to fit
        :type data: numpy.ndarray [y, x] (float)
        :param pos_guess: Method to select the initial position guess: 'max'
            for the maximum pixel (if within a buffer from the edges), 'center'
            for the center of the image
        :type pos_guess: str, default 'max

    Returns:
        :return params_out: Gaussian fitting parameters [height, background,
            x0, y0, sigma1, sigma2, theta] ([_, _, px, px, px, px, rad])
        :rtype params_out: list [7] (float)
    """

    # Fit the Gaussian
    bkg = np.median(data)
    data -= bkg
    max_pos = np.where(data == np.amax(data))
    max_buffer = 5

    if ((max_pos[0] > max_buffer) &
        (max_pos[0] < (data.shape[0] - max_buffer)) &
        (max_pos[1] > max_buffer) &
        (max_pos[1] < (data.shape[1] - max_buffer))) | \
            (pos_guess == 'max'):
        start_pos = max_pos
    else:
        start_pos = np.array(data.shape) / 2

    g_init = models.Gaussian2D(amplitude=np.max(data), x_mean=start_pos[1],
                               y_mean=start_pos[0], x_stddev=2, y_stddev=2,
                               theta=0)
    fit_g = fitting.LevMarLSQFitter()
    y, x = np.mgrid[: data.shape[0], : data.shape[1]]
    warnings.simplefilter('ignore')
    g_out = fit_g(g_init, x, y, data)
    warnings.simplefilter('default')
    params_out = [g_out.amplitude.value, bkg, g_out.x_mean.value,
                  g_out.y_mean.value, g_out.x_stddev.value,
                  g_out.y_stddev.value, g_out.theta.value]
    params_out[6] = np.mod(params_out[6], np.pi)

    return params_out


def gauss_fwhm(s):
    """Calculate the FWHM of a Gaussian profile from the width parameter.

    Parameters:
        :param s: Width parameter (sigma)
        :type s: float

    Returns:
        :return fwhm: FWHM
        :rtype fwhm: float
    """

    fwhm = 2 * s * np.sqrt(2 * np.log(2))

    return fwhm


def moffat1d_prof(r, height, s, alpha, bkg):
    """Calculate the 1D Moffat profile function.

    Parameters:
        :param r: Distance from the center (px)
        :type r: float, numpy.ndarray [x] (float)
        :param height: Peak
        :type height: float
        :param s: Width parameter (px)
        :type s: float
        :param alpha: Exponential
        :type alpha: float
        :param bkg: Background level
        :type bkg: float

    Returns:
        :return y: 1D Moffat profile function
        :rtype y: float, numpy.ndarray [x] (float)
    """

    # Calculate the 1D Moffat profile
    y = (height * ((1 + ((r / s) ** 2)) ** (-alpha))) + bkg

    return y


def moffat2d_prof(x, y, amplitude=1, x_0=0, y_0=0, s1=2, s2=2, theta=0.1,
                  alpha=params_nifs.moffat_alpha):
    """Calculate the 2D Moffat profile function.

    Parameters:
        :param x: x coordinate (px)
        :type x: float
        :param y: y coordinate (px)
        :type y: float
        :param amplitude: Peak
        :type amplitude: float, default 1
        :param x_0: Central x coordinate (px)
        :type x_0: float, default 0
        :param y_0: Central y coordinate (px)
        :type y_0: float, default 0
        :param s1: First width parameter (px)
        :type s1: float, default 2
        :param s2: Second width parameter (px)
        :type s2: float, default 2
        :param theta: Position angle (rad)
        :type theta: float, default 0.1
        :param alpha: Exponential parameter
        :type alpha: float, default 'params_nifs.moffat_alpha'

    Returns:
        :return prof: 2D Moffat profile function
        :rtype prof: float
    """

    rr_gg = (((x - x_0) ** 2) * (((np.cos(theta) / s1) ** 2) +
                                 ((np.sin(theta) / s2) ** 2))) + \
            (((y - y_0) ** 2) * (((np.sin(theta) / s1) ** 2) +
                                 ((np.cos(theta) / s2) ** 2))) + \
            (((x - x_0) * (y - y_0)) * ((np.sin(2 * theta) / (s1 ** 2)) -
                                        (np.sin(2 * theta) / (s2 ** 2))))
    prof = amplitude * ((1 + rr_gg) ** (-alpha))

    return prof


@models.custom_model
def moffat2d(x, y, amplitude=1, x_0=0, y_0=0, s1=2, s2=2, theta=0.1,
             alpha=params_nifs.moffat_alpha):
    """2D Moffat function.

    Parameters:
        :param x: x coordinate (px)
        :type x: float
        :param y: y coordinate (px)
        :type y: float
        :param amplitude: Peak
        :type amplitude: float, default 1
        :param x_0: Central x coordinate (px)
        :type x_0: float, default 0
        :param y_0: Central y coordinate (px)
        :type y_0: float, default 0
        :param s1: First width parameter (px)
        :type s1: float, default 2
        :param s2: Second width parameter (px)
        :type s2: float, default 2
        :param theta: Position angle (rad)
        :type theta: float, default 0.1
        :param alpha: Exponential parameter
        :type alpha: float, default 'params_nifs.moffat_alpha'

    Returns:
        :return prof: 2D Moffat profile function
        :rtype prof: float
    """

    func = moffat2d_prof(x, y, amplitude=amplitude, x_0=x_0, y_0=y_0, s1=s1,
                         s2=s2, theta=theta, alpha=alpha)

    return func


def moffat2d_fit(data, bivariate=True, pos_guess='max', bkg='median',
                 iterate=True, fwhm_box=1.5, model_fit=None, alpha_fix=True):
    """Fit a 2D Moffat to an image. If a fitting model is provided, only peak
    and background are fitted.
    The fitting can be iterated on a box, to avoid large noise and systematic
    errors from the background.

    Parameters:
        :param data: Data to fit
        :type data: numpy.ndarray [y, x] (float)
        :param bivariate: Use a bivariate profile
        :type bivariate: bool, default True
        :param pos_guess: Method to select the initial position guess: 'max'
            for the maximum pixel (if within a buffer from the edges), 'center'
            for the center of the image, [y, x] for the coordinates (px))
        :type pos_guess: str or list [2] (float), default 'max'
        :param bkg: Method to remove the background: 'median' for the median
            value, 'zero' for using 0
        :type bkg: str, default 'median'
        :param iterate: Iterated the fitting in a box
        :type iterate: bool, default True
        :param fwhm_box: Size of the fitting box in number of FWHM
        :type fwhm_box: float, default 1.5
        :param model_fit: 2D Moffat model to fit only in peak and background
            (peak, background, central x coordinate (px), central y coordinate
            (px), first width parameter (px), second width parameter (px),
            position angle (rad), exponential parameter)
        :type model_fit: list [8] (float), optional
        :param alpha_fix: Fix the exponential parameter to the default value
        :type alpha_fix: bool, default True

    Returns:
        :return params_out: 2D Moffat fitting parameters (peak, background,
            central x coordinate (px), central y coordinate (px), first width
            parameter (px), second width parameter (px), position angle (rad),
            exponential parameter)
        :rtype params_out: list [8] (float)
        :return errs_out: 2D Moffat fitting errors (STD) (peak, background,
            central x coordinate (px), central y coordinate (px), first width
            parameter (px), second width parameter (px), position angle (rad),
            exponential parameter)
        :rtype errs_out: list [8] (float)
    """

    # Fit the Moffat
    scale = np.max(data)
    data_copy = data / scale  # Scale all the image, so the fitter doesn't
    # reach immediately the target tolerance
    data_bkg = data_copy[np.where(data_copy < np.percentile(data_copy, 95))]

    if bkg == 'median':
        bkg_val = np.median(data_copy)
    else:
        bkg_val = 0

    data_copy -= bkg_val

    # Iterate the fitting twice, with different boxes
    small_size = 20
    params_out = None
    errs_out = None

    if iterate:
        n_iter = 2
    else:
        n_iter = 1

    if isinstance(pos_guess, list):
        small_c = np.array(pos_guess)
    elif pos_guess == 'max':
        max_where_all = np.where(data_copy == np.max(data_copy))
        small_c = np.array([max_where_all[0][0], max_where_all[1][0]])
    else:
        small_c = np.array(data_copy.shape) / 2

    for i_iter in range(n_iter):

        # Select fitting box
        small_min = small_c - (small_size / 2)
        small_min_y = int(small_min[0])
        small_min_x = int(small_min[1])
        small_max = small_c + (small_size / 2)
        small_max_y = int(small_max[0])
        small_max_x = int(small_max[1])
        data_copy2 = data_copy[small_min_y: small_max_y,
                               small_min_x: small_max_x]

        # Guessing model
        if model_fit:
            m_init = moffat2d(amplitude=1, x_0=(model_fit[2] - small_min_x),
                              y_0=(model_fit[3] - small_min_y),
                              s1=model_fit[4], s2=model_fit[5],
                              theta=model_fit[6], alpha=model_fit[7])
            m_init.x_0.fixed = True
            m_init.y_0.fixed = True
            m_init.s1.fixed = True
            m_init.s2.fixed = True
            m_init.theta.fixed = True
            m_init.alpha.fixed = True
        else:
            max_where = np.where(data_copy2 == np.max(data_copy2))
            start_pos = np.array([max_where[0][0], max_where[1][0]])
            m_init = moffat2d(x_0=start_pos[1], y_0=start_pos[0])
            m_init.s1.bounds = [1, 5]
            m_init.s2.bounds = [1, 5]

            if not bivariate:
                m_init.s2.tied = tie_univariate

            if alpha_fix:
                m_init.alpha.fixed = True

        # Fit the Moffat
        fit_m = fitting.LevMarLSQFitter()
        y, x = np.mgrid[: data_copy2.shape[0], : data_copy2.shape[1]]
        warnings.simplefilter('ignore')
        m_out = fit_m(m_init, x, y, data_copy2)
        warnings.simplefilter('default')
        params_out = [(m_out.amplitude.value * scale), (bkg_val * scale),
                      (m_out.x_0.value + small_min_x),
                      (m_out.y_0.value + small_min_y), m_out.s1.value,
                      m_out.s2.value, m_out.theta.value, m_out.alpha.value]

        if bivariate:
            params_out[6] = np.mod(params_out[6], np.pi)
        else:
            params_out[6] = 0

        # Calculate errors
        bkg_err = np.std(data_bkg) * scale

        if fit_m.fit_info['param_cov'] is None:
            errs_out = [None] * 8
        else:
            errs = np.sqrt(np.diag(fit_m.fit_info['param_cov']))
            amplitude_err = errs[0] * scale

            if model_fit:
                x_err = 0
                y_err = 0
                s1_err = 0
                s2_err = 0
                theta_err = 0
                alpha_err = 0
            else:
                x_err = errs[1]
                y_err = errs[2]

                if bivariate:
                    s1_err = errs[3]
                    s2_err = errs[4]
                    theta_err = errs[5]
                else:
                    s1_err = errs[3]
                    s2_err = errs[3]
                    theta_err = 0

                if m_init.alpha.fixed:
                    alpha_err = 0
                else:
                    alpha_err = errs[-1]

            errs_out = [amplitude_err, bkg_err, x_err, y_err, s1_err, s2_err,
                        theta_err, alpha_err]

        # Prepare next iteration
        fwhm = moffat_fwhm(float(np.mean([m_out.s1.value, m_out.s2.value])),
                           m_out.alpha.value)
        small_size = int(fwhm_box * fwhm)
        small_c = np.array([params_out[3], params_out[2]])

    return params_out, errs_out


def tie_univariate(model):
    """Tie the first and second width parameters of a univariate Moffat
    profile.

    Parameters:
        :param model: Fitting model in 'fit.moffat2d'
        :type model: astropy.modeling.core.moffat2d

    Returns:
        :return s2: Second width parameter of the Moffat prodile
        :rtype s2: astropy.modeling.Parameter
    """

    # Copy the narrow H_alpha line STD
    s2 = model.s1

    return s2


def resid_moffat2d(map1, fit_model):
    """Calculate the residual of a map after fitting a 2D Moffat profile.

    Parameters:
        :param map1: Map of a fitting parameter
        :type map1: numpy.ndarray [y, x] (float)
        :param fit_model: 2D Moffat model to fit and subtract (peak,
            background, central x coordinate (px), central y coordinate (px),
            first width parameter (px), second width parameter (px), position
            angle (rad), exponential parameter)
        :type fit_model: list [8] (float)

    Returns:
        :return residual: Residual map
        :rtype residual: numpy.ndarray [y, x] (float)
    """

    y, x = np.mgrid[: map1.shape[0], : map1.shape[1]]
    map_sub = moffat2d(amplitude=fit_model[0], x_0=fit_model[2],
                       y_0=fit_model[3], s1=fit_model[4],
                       s2=fit_model[5], theta=fit_model[6],
                       alpha=fit_model[7])
    residual = map1 - map_sub(x, y)

    return residual


def moffat_fwhm(s, alpha):
    """Calculate the FWHM of a Moffat profile from the width parameter.

    Parameters:
        :param s: Width parameter
        :type s: float
        :param alpha: Exponential parameter
        :type alpha: float

    Returns:
        :return fwhm: FWHM
        :rtype fwhm: float
    """

    fwhm = 2 * s * np.sqrt((2 ** (1 / alpha)) - 1)

    return fwhm


def moffat_s(fwhm, alpha):
    """Calculate the width parameter of a Moffat profile from the FWHM.

    Parameters:
        :param fwhm: FWHM
        :type fwhm: float
        :param alpha: Exponential parameter
        :type alpha: float

    Returns:
        :return s: Width parameter
        :rtype s: float
    """

    s = fwhm / (2 * np.sqrt((2 ** (1 / alpha)) - 1))

    return s


def sr(params_in, model, lambd, ps):
    """Calculate the SR of a fitted Gaussian or Moffat PSF.

    Parameters:
        :param params_in: Parameters of the fitted PSF ([height, background,
            x0, y0, sigma1, sigma2, theta] ([_, _, px, px, px, px, rad]) for
            Gaussian, [height, background, x0, y0, s1, s2, theta, alpha] ([_,
            _, px, px, px, px, rad, _]) for Moffat)
        :rtype params_in: list [7 or 8] (float)
        :param model: Model of the fitted PSF ('gauss' for Gaussian, 'moffat'
            for Moffat)
        :type model: str
        :param lambd: Wavelength (um)
        :type lambd: float
        :param ps: Spaxel scale ("/px)
        :type ps: float

    Returns:
        :return s_ratio: Strehl ratio
        :rtype s_ratio: float
    """

    # Calculate integral
    side_img = 501  # Side of the image to calculate the PSF integral (px)
    overs = 5  # Diffraction limited PSF oversampling
    image = np.zeros([side_img, side_img])
    center = np.floor(side_img / 2)
    y_mesh, x_mesh = np.indices(image.shape)

    if model == 'gauss':
        model_fun = models.Gaussian2D(amplitude=(params_in[0] / (overs ** 2)),
                                      x_mean=center, y_mean=center,
                                      x_stddev=(params_in[4] * overs),
                                      y_stddev=(params_in[5] * overs),
                                      theta=params_in[6])
        model_img = model_fun(y_mesh, x_mesh)
    else:
        model_fun = moffat2d(amplitude=(params_in[0] / (overs ** 2)),
                             x_0=center, y_0=center, s1=(params_in[4] * overs),
                             s2=(params_in[5] * overs), theta=params_in[6],
                             alpha=params_in[7])
        model_img = model_fun(y_mesh, x_mesh)

    integr = np.sum(model_img)

    # Generate the telescope aperture
    fov = 3  # Diffraction limited PSF field of view (")
    g_m = 0.206265 * lambd / (ps / overs)
    g_px = int(g_m * fov / (lambd * 0.206265))

    if (g_px % 2) == 0:
        g_px += 1

    d_out_px = g_px * params_nifs.d_out_gemini / g_m
    d_in_px = g_px * params_nifs.d_in_gemini / g_m
    image_diffr = np.zeros([g_px, g_px])
    y_diffr_mesh, x_diffr_mesh = np.indices(image_diffr.shape)
    center_diff = int(g_px / 2)
    dist_diff = np.hypot((x_diffr_mesh - center_diff),
                         (y_diffr_mesh - center_diff))
    pupil = np.where((dist_diff <= (d_out_px / 2)) &
                     (dist_diff >= (d_in_px / 2)))
    image_diffr[pupil] = 1

    # Generate the diffraction-limited PSF
    psf_diffr = \
        np.fft.fftshift(np.abs(np.fft.ifft2(np.fft.ifftshift(image_diffr))) **
                        2)
    integr_diffr = np.sum(psf_diffr)
    psf_diffr *= integr / integr_diffr

    # Calculate SR
    s_ratio = np.max(model_img) / np.max(psf_diffr)

    return s_ratio


def fit_h(wvl, spectr, fit_range, bb_0_range, sigma_range=2, fit_b=True):
    """Fit the H band spectrum to determine line parameters.
    The fit uses two narrow Gaussians with fixed peak ratio and distance for
    the [OIII] doublet. The black body offset is measured but not fitted.
    An additional broad line near the red [OIII] line can be fit.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param fit_range: Wavelength range to fit the profile (min, max) (um)
        :type fit_range: list [2] (float)
        :param bb_0_range: Wavelength range to measure the black body
            offset (min, max) (um)
        :type bb_0_range: list [2] (float)
        :param sigma_range: Fractional half-range of narrow lines' sigma
        :type sigma_range: float, default 2
        :param fit_b: Fit a broad line
        :type fit_b: bool, default True

    Returns:
        :return h_out: Fitting model
        :rtype h_out: astropy.modeling.core.h_prof
    """

    # Prepare the fitter
    wvl_fit, spectr_fit = cube_nifs.cut_spectrum(wvl, spectr, fit_range[0],
                                                 fit_range[1])
    _, spectr_bb_0 = cube_nifs.cut_spectrum(wvl, spectr, bb_0_range[0],
                                            bb_0_range[1])
    bb_0 = np.median(spectr_bb_0)
    peak_guess = np.max(spectr_fit) - bb_0
    h_init = h_prof(bb_0=bb_0, o3rn_p=peak_guess,
                    o3bn_p=(peak_guess / params_nifs.o3_ratio),
                    b_p=(peak_guess / 3))
    h_init.bb_0.fixed = True
    h_init.o3rn_c.bounds = params_nifs.o3r * v_sqrt
    h_init.o3rn_s.bounds = params_nifs.sigma_n * \
        np.array([(1 - (1 / sigma_range)), (1 + (1 / sigma_range))])
    h_init.o3bn_p.tied = tie_o3bn_p
    h_init.o3bn_c.tied = tie_o3bn_c
    h_init.o3bn_s.tied = tie_sn_h
    h_init.b_c.bounds = h_init.o3rn_c.bounds * \
        (1 + (0.005 * np.array([-1, 1])))
    h_init.b_s.bounds = np.array(h_init.o3rn_s.bounds) * 8

    if not fit_b:
        h_init.b_p = 0
        h_init.b_p.fixed = True

    fit_h_obj = fitting.LevMarLSQFitter()

    # Fit the Gaussian
    warnings.simplefilter('ignore')
    h_out = fit_h_obj(h_init, wvl_fit, spectr_fit)
    warnings.simplefilter('default')

    # Calculate continuum
    continuum(wvl, bb_0_range, spectr, h_out)

    # Calculate integrated lines
    integr_gauss_h(h_out)

    return h_out


def fit_k(wvl, spectr, wvl_range_cont, bb_slope=True, sigma_range=2,
          tie_z=False, tie_s=False, fix=False, fix_pars=None, peak_pos=False,
          only_han=False, iterate=True):
    """Fit the K band spectrum to determine line parameters.
    The fit uses an offset slope for the black body radiation, one broad and
    one narrow Gaussian with different centers for the H_a, two narrow
    Gaussians with fixed peak ratio and distance for the [NII] doublet, two
    narrow Gaussians with fixed peak ratio and distance for the [SII] doublet.
    The continuum value is also measured. The flux density in a window
    approximately on the narrow H_a line is also measured.
    There is the option to fix the position and width of the broad H_a line and
    the width of all narrow lines.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param wvl_range_cont: Wavelength range to measure the continuum
            (min, max) (um)
        :type wvl_range_cont: list [2] (float)
        :param bb_slope: Put the continuum on a slope
        :type bb_slope: bool, default True
        :param sigma_range: Fractional half-range of narrow lines' sigma
        :type sigma_range: float, default 2
        :param tie_z: Tie the redshift of narrow H_a and [NII] lines
        :type tie_z: bool, default False
        :param tie_s: Tie the redshift and width of [SII] lines
        :type tie_s: bool, default False
        :param fix: Fix the position and width of the broad H_a line and the
            width of all narrow lines
        :type fix: bool, default False
        :param fix_pars: Fitting model to fix positions and widths of lines
            selected in 'fix'
        :type fix_pars: astropy.modeling.core.k_prof, optional
        :param peak_pos: Force peaks to be positive
        :type peak_pos: bool, default False
        :param only_han: Fit only the narrow H_a line
        :type only_han: bool, default False
        :param iterate: Iterate twice
        :type iterate: bool, default True

    Returns:
        :return k_out: Fitting model
        :rtype k_out: astropy.modeling.core.k_prof
    """

    # Loop twice to adjust lines
    peak_range = [-1e10, 1e10]

    if peak_pos:
        peak_range[0] = 0

    k_out = None
    wvl_fit = None
    spectr_fit = None

    if iterate:
        i_loops = 2
    else:
        i_loops = 1

    for i_loop in range(i_loops):

        # Prepare the fitter
        if i_loop == 0:
            peak_guess = (np.max(spectr) - np.median(spectr)) / 3
            k_init = k_prof(hab_p=peak_guess, han_p=peak_guess,
                            n2r_p=peak_guess,
                            n2b_p=(peak_guess / params_nifs.n2_ratio),
                            s2b_p=peak_guess,
                            s2r_p=(peak_guess / params_nifs.s2_ratio))
            k_init.n2r_c.tied = tie_n2r_c

            if only_han:
                cont_where = np.where((wvl >= wvl_range_cont[0]) &
                                      (wvl <= wvl_range_cont[1]))[0]
                k_init.bb_0.value = np.median(spectr[cont_where])
                k_init.bb_0.fixed = True
                k_init.hab_p.value = 0
                k_init.hab_p.fixed = True
                k_init.n2r_p.value = 0
                k_init.n2r_p.fixed = True
                k_init.n2b_p.value = 0
                k_init.n2b_p.fixed = True
                k_init.s2b_p.value = 0
                k_init.s2b_p.fixed = True
                k_init.s2r_p.value = 0
                k_init.s2r_p.fixed = True
                wvl_han_where = np.where(
                    (wvl >= (k_init.han_c.value - (1.3 * k_init.han_s.value)))
                    &
                    (wvl <= (k_init.han_c.value + (1.3 * k_init.han_s.value))))
                wvl_fit = wvl[wvl_han_where]
                spectr_fit = spectr[wvl_han_where]
            else:
                wvl_fit = copy.deepcopy(wvl)
                spectr_fit = copy.deepcopy(spectr)
        else:
            k_init = copy.deepcopy(k_out)

        k_init.hab_p.bounds = peak_range
        k_init.hab_c.bounds = params_nifs.h_alpha * v_sqrt_b
        k_init.hab_s.bounds = params_nifs.sigma_b * \
            np.array([(1 - (1 / sigma_range)), (1 + (1 / sigma_range))])
        k_init.han_p.bounds = peak_range
        k_init.han_c.bounds = params_nifs.h_alpha * v_sqrt
        k_init.han_s.bounds = params_nifs.sigma_n * \
            np.array([(1 - (1 / sigma_range)), (1 + (1 / sigma_range))])
        k_init.n2r_p.bounds = peak_range
        k_init.n2r_s.tied = tie_sn_k
        k_init.n2b_p.tied = tie_n2b_p
        k_init.n2b_c.tied = tie_n2b_c
        k_init.n2b_s.tied = tie_sn_k
        k_init.s2b_p.bounds = peak_range
        k_init.s2r_p.tied = tie_s2r_p
        k_init.s2r_c.tied = tie_s2r_c
        k_init.s2r_s.tied = tie_s2r_s

        if tie_z:
            k_init.n2r_c.tied = tie_n2r_c
        else:
            k_init.n2r_c.bounds = params_nifs.n2r * v_sqrt

        if tie_s:
            k_init.s2b_c.tied = tie_s2b_c
            k_init.s2b_s.tied = tie_sn_k
            k_init.s2r_s.tied = tie_sn_k
        else:
            k_init.s2b_c.bounds = params_nifs.s2b * v_sqrt
            k_init.s2b_s.bounds = params_nifs.sigma_n * \
                np.array([(1 - (1 / sigma_range)), (1 + (1 / sigma_range))])

        if not bb_slope:
            k_init.bb_1.value = 0
            k_init.bb_1.fixed = True

        if fix:
            k_init.hab_c.value = fix_pars.hab_c.value
            k_init.hab_c.fixed = True
            k_init.hab_s.value = fix_pars.hab_s.value
            k_init.hab_s.fixed = True
            k_init.han_s.value = fix_pars.han_s.value
            k_init.han_s.fixed = True
            k_init.n2r_s.value = fix_pars.n2r_s.value
            k_init.n2r_s.fixed = True
            k_init.n2b_s.value = fix_pars.n2b_s.value
            k_init.n2b_s.fixed = True
            k_init.s2b_s.value = fix_pars.s2b_s.value
            k_init.s2b_s.fixed = True
            k_init.s2r_s.value = fix_pars.s2r_s.value
            k_init.s2r_s.fixed = True

        fit_k_obj = fitting.LevMarLSQFitter()

        # Fit the Gaussian
        warnings.simplefilter('ignore')
        k_out = fit_k_obj(k_init, wvl_fit, spectr_fit)
        warnings.simplefilter('default')

    # Calculate peak errors
    warnings.simplefilter('ignore')
    peak_err_k(wvl, spectr, k_out)
    warnings.simplefilter('default')

    # Calculate continuum
    continuum(wvl, wvl_range_cont, spectr, k_out)

    # Calculate narrow H_a line flux density
    han_flux(wvl, spectr, k_out)

    # Calculate integrated lines
    integr_gauss_k(k_out)

    return k_out


@models.custom_model
def h_prof(lambd, cont=0, bb_0=0, o3rn_p=1,
           o3rn_c=(params_nifs.o3r * (1 + params_nifs.z)),
           o3rn_s=params_nifs.sigma_n, o3rn_i=0, o3bn_p=1,
           o3bn_c=(params_nifs.o3b * (1 + params_nifs.z)),
           o3bn_s=params_nifs.sigma_n, o3bn_i=0, o3n_i=0, b_p=1,
           b_c=(params_nifs.o3r * (1 + params_nifs.z)),
           b_s=(params_nifs.sigma_n * 5), b_i=0):
    """Calculate the H band spectrum profile function.
    The model uses an offset slope for the black body radiation, Gaussians for
    the [OIII] doublet and a broad line.

    Parameters:
        :param lambd: Wavelength (um)
        :type lambd: numpy.ndarray [n] (float)
        :param cont: Continuum
        :type cont: float, default 0 (erg/s/cm^2/um)
        :param bb_0: Black body intensity offset
        :type bb_0: float, default 0 (erg/s/cm^2/um)
        :param o3rn_p: Narrow red [OIII] line peak (erg/s/cm^2/um)
        :type o3rn_p: float, default 1
        :param o3rn_c: Narrow red [OIII] line center (um)
        :type o3rn_c: float, default (o3r * (1 + z))
        :param o3rn_s: Narrow red [OIII] line STD (um)
        :type o3rn_s: float, default sigma_n
        :param o3rn_i: Narrow red [OIII] line integrated (erg/s/cm^2)
        :type o3rn_i: float, default 0
        :param o3bn_p: Narrow blue [OIII] line peak (erg/s/cm^2/um)
        :type o3bn_p: float, default 1
        :param o3bn_c: Narrow blue [OIII] line center (um)
        :type o3bn_c: float, default (o3b * (1 + z))
        :param o3bn_s: Narrow blue [OIII] line STD (um)
        :type o3bn_s: float, default sigma_n
        :param o3bn_i: Narrow blue [OIII] line integrated (erg/s/cm^2)
        :type o3bn_i: float, default 0
        :param o3n_i: Narrow [OIII] lines integrated (erg/s/cm^2)
        :type o3n_i: float, default 0
        :param b_p: Broad line peak (erg/s/cm^2/um)
        :type b_p: float, default 1
        :param b_c: Broad line center (um)
        :type b_c: float, default (o3r * (1 + z))
        :param b_s: Broad line STD (um)
        :type b_s: float, default (sigma_b / 3)
        :param b_i: Broad line integrated (erg/s/cm^2)
        :type b_i: float, default 0

    Returns:
        :return prof: H band spectrum profile
        :rtype prof: numpy.ndarray [n] (float)
    """

    # Calculate line profiles
    bb_prof = bb_0
    o3rn_prof = o3rn_p * np.exp(-0.5 * (((lambd - o3rn_c) / o3rn_s) ** 2))
    o3bn_prof = o3bn_p * np.exp(-0.5 * (((lambd - o3bn_c) / o3bn_s) ** 2))
    b_prof = b_p * np.exp(-0.5 * (((lambd - b_c) / b_s) ** 2))

    # Calculate total profile
    prof = bb_prof + o3rn_prof + o3bn_prof + b_prof
    _ = 0 * cont * o3rn_i * o3bn_i * o3n_i * b_i

    return prof


@models.custom_model
def k_prof(lambd, cont=0, bb_0=0, bb_1=0,
           hab_p=1, hab_p_err=0, hab_c=(params_nifs.h_alpha *
                                        (1 + params_nifs.z)),
           hab_s=params_nifs.sigma_b, hab_i=0,
           han_p=1, han_p_err=0, han_c=(params_nifs.h_alpha *
                                        (1 + params_nifs.z)),
           han_s=params_nifs.sigma_n, han_i=0, han_f=0,
           n2r_p=1, n2r_p_err=0, n2r_c=(params_nifs.n2r * (1 + params_nifs.z)),
           n2r_s=params_nifs.sigma_n, n2r_i=0,
           n2b_p=1, n2b_p_err=0, n2b_c=(params_nifs.n2b * (1 + params_nifs.z)),
           n2b_s=params_nifs.sigma_n, n2b_i=0, n2_i=0,
           s2b_p=1, s2b_p_err=0, s2b_c=(params_nifs.s2b * (1 + params_nifs.z)),
           s2b_s=params_nifs.sigma_n, s2b_i=0,
           s2r_p=1, s2r_p_err=0, s2r_c=(params_nifs.s2r * (1 + params_nifs.z)),
           s2r_s=params_nifs.sigma_n, s2r_i=0, s2_i=0):
    """Calculate the K band spectrum profile function.
    The model uses an offset slope for the black body radiation and Gaussians
    for the broad and narrow H_a lines, for the [NII] and [SII] doublets.

    Parameters:
        :param lambd: Wavelength (um)
        :type lambd: numpy.ndarray [n] (float)
        :param cont: Continuum (erg/s/cm^2/um)
        :type cont: float, default 0
        :param bb_0: Black body intensity offset (erg/s/cm^2/um)
        :type bb_0: float, default 0
        :param bb_1: Black body intensity slope, respect to (h_alpha * (1 + z))
            (erg/s/cm^2/um^2)
        :type bb_1: float, default 0
        :param hab_p: Broad H_a line peak (erg/s/cm^2/um)
        :type hab_p: float, default 1
        :param hab_p_err: Broad H_a line peak error (erg/s/cm^2/um)
        :type hab_p_err: float, default 0
        :param hab_c: Broad H_a line center (um)
        :type hab_c: float, default (h_alpha * (1 + z))
        :param hab_s: Broad H_a line STD (um)
        :type hab_s: float, default sigma_b
        :param hab_i: Broad H_a line integrated (erg/s/cm^2)
        :type hab_i: float, default 0
        :param han_p: Narrow H_a line peak (erg/s/cm^2/um)
        :type han_p: float, default 1
        :param han_p_err: Narrow H_a line peak error (erg/s/cm^2/um)
        :type han_p_err: float, default 0
        :param han_c: Narrow H_a line center (um)
        :type han_c: float, default (h_alpha * (1 + z))
        :param han_s: Narrow H_a line STD (um)
        :type han_s: float, default sigma_n
        :param han_i: Narrow H_a line integrated (erg/s/cm^2)
        :type han_i: float, default 0
        :param han_f: Flux density in a window approximately on the narrow
            H_a line (erg/s/cm^2/um)
        :type han_f: float, default 0
        :param n2r_p: Red [NII] line peak (erg/s/cm^2/um)
        :type n2r_p: float, default 1
        :param n2r_p_err: Red [NII] line peak error (erg/s/cm^2/um)
        :type n2r_p_err: float, default 0
        :param n2r_c: Red [NII] line center (um)
        :type n2r_c: float, default (n2r * (1 + z))
        :param n2r_s: Red [NII] line STD (um)
        :type n2r_s: float, default sigma_n
        :param n2r_i: Red [NII] line integrated (erg/s/cm^2)
        :type n2r_i: float, default 0
        :param n2b_p: Blue [NII] line peak (erg/s/cm^2/um)
        :type n2b_p: float, default 1
        :param n2b_p_err: Blue [NII] line peak error (erg/s/cm^2/um)
        :type n2b_p_err: float, default 0
        :param n2b_c: Blue [NII] line center (um)
        :type n2b_c: float, default (n2b * (1 + z))
        :param n2b_s: Blue [NII] line STD (um)
        :type n2b_s: float, default sigma_n
        :param n2b_i: Blue [NII] line integrated (erg/s/cm^2)
        :type n2b_i: float, default 0
        :param n2_i: [NII] lines integrated (erg/s/cm^2)
        :type n2_i: float, default 0
        :param s2b_p: Blue [SII] line peak (erg/s/cm^2/um)
        :type s2b_p: float, default 1
        :param s2b_p_err: Blue [SII] line peak error (erg/s/cm^2/um)
        :type s2b_p_err: float, default 0
        :param s2b_c: Blue [SII] line center (um)
        :type s2b_c: float, default (s2b * (1 + z))
        :param s2b_s: Blue [SII] line STD (um)
        :type s2b_s: float, default sigma_n
        :param s2b_i: Blue [SII] line integrated (erg/s/cm^2)
        :type s2b_i: float, default 0
        :param s2r_p: Red [SII] line peak (erg/s/cm^2/um)
        :type s2r_p: float, default 1
        :param s2r_p_err: Red [SII] line peak error (erg/s/cm^2/um)
        :type s2r_p_err: float, default 0
        :param s2r_c: Red [SII] line center (um)
        :type s2r_c: float, default (s2r * (1 + z))
        :param s2r_s: Red [SII] line STD (um)
        :type s2r_s: float, default sigma_n
        :param s2r_i: Red [SII] line integrated (erg/s/cm^2)
        :type s2r_i: float, default 0
        :param s2_i: [SII] lines integrated (erg/s/cm^2)
        :type s2_i: float, default 0

    Returns:
        :return prof: K band spectrum profile
        :rtype prof: numpy.ndarray [n] (float)
    """

    # Calculate line profiles
    bb_prof = bb_0 + (bb_1 * (lambd - (params_nifs.h_alpha *
                                       (1 + params_nifs.z))))
    hab_prof = hab_p * np.exp(-0.5 * (((lambd - hab_c) / hab_s) ** 2))
    han_prof = han_p * np.exp(-0.5 * (((lambd - han_c) / han_s) ** 2))
    n2r_prof = n2r_p * np.exp(-0.5 * (((lambd - n2r_c) / n2r_s) ** 2))
    n2b_prof = n2b_p * np.exp(-0.5 * (((lambd - n2b_c) / n2b_s) ** 2))
    s2b_prof = s2b_p * np.exp(-0.5 * (((lambd - s2b_c) / s2b_s) ** 2))
    s2r_prof = s2r_p * np.exp(-0.5 * (((lambd - s2r_c) / s2r_s) ** 2))

    # Calculate total profile
    prof = bb_prof + hab_prof + han_prof + n2r_prof + n2b_prof + s2b_prof + \
        s2r_prof
    _ = 0 * cont * hab_p_err * hab_i * han_p_err * han_i * han_f * \
        n2r_p_err * n2r_i * n2b_p_err * n2b_i * n2_i * s2b_p_err * s2b_i * \
        s2r_p_err * s2r_i * s2_i

    return prof


def tie_sn_h(model):
    """Tie a H band narrow line STD to the narrow red [OIII] line.

    Parameters:
        :param model: Fitting model in 'fit.h_prof'
        :type model: astropy.modeling.core.h_prof

    Returns:
        :return s: Parameter of the narrow line STD
        :rtype s: astropy.modeling.Parameter
    """

    # Copy the narrow red [OIII] line STD
    s = model.o3rn_s

    return s


def tie_sn_k(model):
    """Tie a K band narrow line STD to the narrow H_alpha line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return s: Parameter of the narrow line STD
        :rtype s: astropy.modeling.Parameter
    """

    # Copy the narrow H_alpha line STD
    s = model.han_s

    return s


def tie_o3bn_c(model):
    """Tie the narrow blue [OIII] line center to the narrow red [OIII] line.

    Parameters:
        :param model: Fitting model in 'fit.h_prof'
        :type model: astropy.modeling.core.h_prof

    Returns:
        :return o3bn_c: Parameter of the narrow blue [OIII] line center
        :rtype o3bn_c: astropy.modeling.Parameter
    """

    # Calculate the narrow blue [OIII] line center
    o3bn_c = model.o3rn_c * params_nifs.o3b / params_nifs.o3r

    return o3bn_c


def tie_o3bn_p(model):
    """Tie the narrow blue [OIII] line peak to the narrow red [OIII] line.

    Parameters:
        :param model: Fitting model in 'fit.h_prof'
        :type model: astropy.modeling.core.h_prof

    Returns:
        :return o3bn_p: Parameter of the narrow blue [OIII] line peak
        :rtype o3bn_p: astropy.modeling.Parameter
    """

    # Calculate the narrow blue [OIII] line peak
    o3bn_p = model.o3rn_p / params_nifs.o3_ratio

    return o3bn_p


def tie_n2r_c(model):
    """Tie the red [NII] line center to the narrow H_a line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return n2r_c: Parameter of the red [NII] line center
        :rtype n2r_c: astropy.modeling.Parameter
    """

    # Calculate the red [NII] line center
    n2r_c = model.han_c * params_nifs.n2r / params_nifs.h_alpha

    return n2r_c


def tie_n2b_p(model):
    """Tie the blue [NII] line peak to the red [NII] line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return n2b_p: Parameter of the blue [NII] line peak
        :rtype n2b_p: astropy.modeling.Parameter
    """

    # Calculate the blue [NII] line peak
    n2b_p = model.n2r_p / params_nifs.n2_ratio

    return n2b_p


def tie_n2b_c(model):
    """Tie the blue [NII] line center to the red [NII] line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return n2b_c: Parameter of the blue [NII] line center
        :rtype n2b_c: astropy.modeling.Parameter
    """

    # Calculate the blue [NII] line center
    n2b_c = model.n2r_c * params_nifs.n2b / params_nifs.n2r

    return n2b_c


def tie_s2b_c(model):
    """Tie the blue [SII] line center to the narrow H_a line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return s2b_c: Parameter of the blue [SII] line center
        :rtype s2b_c: astropy.modeling.Parameter
    """

    # Calculate the blue [SII] line center
    s2b_c = model.han_c * params_nifs.s2b / params_nifs.h_alpha

    return s2b_c


def tie_s2r_p(model):
    """Tie the red [SII] line peak to the blue [SII] line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return s2r_p: Parameter of the red [SII] line peak
        :rtype s2r_p: astropy.modeling.Parameter
    """

    # Calculate the red [SII] line peak
    s2r_p = model.s2b_p / params_nifs.s2_ratio

    return s2r_p


def tie_s2r_c(model):
    """Tie the red [SII] line center to the blue [SII] line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return s2r_c: Parameter of the red [SII] line center
        :rtype s2r_c: astropy.modeling.Parameter
    """

    # Calculate the red [SII] line center
    s2r_c = model.s2b_c * params_nifs.s2r / params_nifs.s2b

    return s2r_c


def tie_s2r_s(model):
    """Tie the red [SII] line STD to the blue [SII] line.

    Parameters:
        :param model: Fitting model in 'fit.k_prof'
        :type model: astropy.modeling.core.k_prof

    Returns:
        :return s2r_s: Parameter of the red [SII] line STD
        :rtype s2r_s: astropy.modeling.Parameter
    """

    # Copy the blue [SII] line STD
    s2r_s = model.s2b_s

    return s2r_s


def resid_h(wvl, spectr, h_fit):
    """Calculate the residual of the H band spectrum after fitting.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param h_fit: Fitting model
        :type h_fit: astropy.modeling.core.h_prof

    Returns:
        :return residual: Residual 1D spectrum
        :rtype residual: numpy.ndarray [n] (float)
    """

    prof = h_prof(bb_0=h_fit.bb_0.value, o3rn_p=h_fit.o3rn_p.value,
                  o3rn_c=h_fit.o3rn_c.value, o3rn_s=h_fit.o3rn_s.value,
                  o3bn_p=h_fit.o3bn_p.value, o3bn_c=h_fit.o3bn_c.value,
                  o3bn_s=h_fit.o3bn_s.value, b_p=h_fit.b_p.value,
                  b_c=h_fit.b_c.value, b_s=h_fit.b_s.value)
    residual = spectr - prof(wvl)

    return residual


def resid_k(wvl, spectr, k_fit):
    """Calculate the residual of the K band spectrum after fitting.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param k_fit: Fitting model
        :type k_fit: astropy.modeling.core.k_prof

    Returns:
        :return residual: Residual 1D spectrum
        :rtype residual: numpy.ndarray [n] (float)
    """

    prof = k_prof(bb_0=k_fit.bb_0.value, bb_1=k_fit.bb_1.value,
                  hab_p=k_fit.hab_p.value, hab_c=k_fit.hab_c.value,
                  hab_s=k_fit.hab_s.value, han_p=k_fit.han_p.value,
                  han_c=k_fit.han_c.value, han_s=k_fit.han_s.value,
                  n2r_p=k_fit.n2r_p.value, n2r_c=k_fit.n2r_c.value,
                  n2r_s=k_fit.n2r_s.value, n2b_p=k_fit.n2b_p.value,
                  n2b_c=k_fit.n2b_c.value, n2b_s=k_fit.n2b_s.value,
                  s2b_p=k_fit.s2b_p.value, s2b_c=k_fit.s2b_c.value,
                  s2b_s=k_fit.s2b_s.value, s2r_p=k_fit.s2r_p.value,
                  s2r_c=k_fit.s2r_c.value, s2r_s=k_fit.s2r_s.value)
    residual = spectr - prof(wvl)

    return residual


def peak_err_k(wvl, spectr, k_fit):
    """Calculate the error of the fitted peaks of the K band spectrum.
    The error is calculated as the standard error of the residual spectrum
    within the FWHM of a line. The error of each peak of a doublet is
    calculated using both lines.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param k_fit: Fitting model
        :type k_fit: astropy.modeling.core.k_prof
    """

    # Calculate the peak error
    residual = resid_k(wvl, spectr, k_fit)
    spectr_hab_where = \
        np.where((wvl >= (k_fit.hab_c.value -
                          (gauss_fwhm(k_fit.hab_s.value) / 2))) &
                 (wvl <= (k_fit.hab_c.value +
                          (gauss_fwhm(k_fit.hab_s.value) / 2))))[0]
    spectr_han_where = \
        np.where((wvl >= (k_fit.han_c.value -
                          (gauss_fwhm(k_fit.han_s.value) / 2))) &
                 (wvl <= (k_fit.han_c.value +
                          (gauss_fwhm(k_fit.han_s.value) / 2))))[0]
    spectr_n2_where = \
        np.where(((wvl >= (k_fit.n2r_c.value -
                           (gauss_fwhm(k_fit.n2r_s.value) / 2))) &
                  (wvl <= (k_fit.n2r_c.value +
                           (gauss_fwhm(k_fit.n2r_s.value) / 2)))) |
                 ((wvl >= (k_fit.n2b_c.value -
                           (gauss_fwhm(k_fit.n2b_s.value) / 2))) &
                  (wvl <= (k_fit.n2b_c.value +
                           (gauss_fwhm(k_fit.n2b_s.value) / 2)))))[0]
    spectr_s2_where = \
        np.where(((wvl >= (k_fit.s2r_c.value -
                           (gauss_fwhm(k_fit.s2r_s.value) / 2))) &
                  (wvl <= (k_fit.s2r_c.value +
                           (gauss_fwhm(k_fit.s2r_s.value) / 2)))) |
                 ((wvl >= (k_fit.s2b_c.value -
                           (gauss_fwhm(k_fit.s2b_s.value) / 2))) &
                  (wvl <= (k_fit.s2b_c.value +
                           (gauss_fwhm(k_fit.s2b_s.value) / 2)))))[0]
    k_fit.hab_p_err = np.std(residual[spectr_hab_where]) / \
        np.sqrt(len(spectr_hab_where))
    k_fit.han_p_err = np.std(residual[spectr_han_where]) / \
        np.sqrt(len(spectr_han_where))
    k_fit.n2r_p_err = np.std(residual[spectr_n2_where]) / \
        np.sqrt(len(spectr_n2_where))
    k_fit.n2b_p_err = np.std(residual[spectr_n2_where]) / \
        np.sqrt(len(spectr_n2_where))
    k_fit.s2b_p_err = np.std(residual[spectr_s2_where]) / \
        np.sqrt(len(spectr_s2_where))
    k_fit.s2r_p_err = np.std(residual[spectr_s2_where]) / \
        np.sqrt(len(spectr_s2_where))


def integr_gauss_h(spec):
    """Calculate the Gaussian integrated lines of the H spectrum.

    Parameters:
        :param spec: Fitting model
        :type spec: astropy.modeling.core.h_prof
    """

    # Calculate the Gaussian integrals
    o3rn_i = spec.o3rn_p * spec.o3rn_s * np.sqrt(2 * np.pi)
    o3bn_i = spec.o3bn_p * spec.o3bn_s * np.sqrt(2 * np.pi)
    b_i = spec.b_p * spec.b_s * np.sqrt(2 * np.pi)
    spec.o3rn_i = o3rn_i
    spec.o3bn_i = o3bn_i
    spec.o3n_i = o3rn_i + o3bn_i
    spec.b_i = b_i


def integr_gauss_k(spec):
    """Calculate the Gaussian integrated lines of the K spectrum.

    Parameters:
        :param spec: Fitting model
        :type spec: astropy.modeling.core.k_prof
    """

    # Calculate the Gaussian integrals
    hab_i = spec.hab_p * spec.hab_s * np.sqrt(2 * np.pi)
    han_i = spec.han_p * spec.han_s * np.sqrt(2 * np.pi)
    n2r_i = spec.n2r_p * spec.n2r_s * np.sqrt(2 * np.pi)
    n2b_i = spec.n2b_p * spec.n2b_s * np.sqrt(2 * np.pi)
    s2b_i = spec.s2b_p * spec.s2b_s * np.sqrt(2 * np.pi)
    s2r_i = spec.s2r_p * spec.s2r_s * np.sqrt(2 * np.pi)
    spec.hab_i = hab_i
    spec.han_i = han_i
    spec.n2r_i = n2r_i
    spec.n2b_i = n2b_i
    spec.n2_i = n2r_i + n2b_i
    spec.s2b_i = s2b_i
    spec.s2r_i = s2r_i
    spec.s2_i = s2b_i + s2r_i


def continuum(wvl, wvl_range, spectr, hk_fit):
    """Calculate the spectrum continuum value. It is the median value of the
    spectrum within a range of wavelength outside any line.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param wvl_range: Wavelength range (min, max) (um)
        :type wvl_range: list [2] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param hk_fit: Fitting model
        :type hk_fit: astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof
    """

    # Measure the continuum
    cont_where = np.where((wvl >= wvl_range[0]) & (wvl <= wvl_range[1]))[0]
    hk_fit.cont = np.median(spectr[cont_where])


def han_flux(wvl, spectr, k_fit):
    """Calculate the flux density in a window approximately on the narrow H_a
    line. It is the mean value of the spectrum within a range of wavelengths
    with a number of expected STDs as width.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)
        :param k_fit: Fitting model
        :type k_fit: astropy.modeling.core.k_prof
    """

    # Measure the continuum
    range_c = np.around((params_nifs.h_alpha * (1 + params_nifs.z)), 3)
    range_hw = params_nifs.sigma_n * params_nifs.sigma_n_flux / 2
    flux_where = np.where((wvl >= (range_c - range_hw)) &
                          (wvl <= (range_c + range_hw)))[0]
    k_fit.han_f = np.mean(spectr[flux_where])


def int_flux(wvl, wvl_range, spectr):
    """Calculate a spectrum integrated flux. The continuum + background is not
    subtracted. Therefore, the measurement is useful only if the value of
    continuum + background is spatially constant.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param wvl_range: Wavelength range (min, max) (um)
        :type wvl_range: list [2] (float)
        :param spectr: 1D Spectrum (erg/s/cm^2/um)
        :type spectr: numpy.ndarray [n] (float)

    Returns:
        :return flux: Spectrum integrated flux (erg/s/cm^2)
        :rtype flux: float
    """

    # Measure the spectrum integrated flux
    flux_where = np.where((wvl >= wvl_range[0]) & (wvl <= wvl_range[1]))[0]
    flux = np.sum(spectr[flux_where])

    return flux


def phys_lines_h(fit_in):
    """Calculate physical parameters from the fitted lines in the H band
    spectrum.

    Parameters:
        :param fit_in: Fitting model
        :type fit_in: astropy.modeling.core.h_prof

    Returns:
        :return z: Redshift
        :rtype z: float
        :return v1: LOS velocity (m/s)
        :rtype v1: float
        :return s_n: Narrow LOS velocity dispersion (m/s)
        :rtype s_n: float
        :return s_b: Broad LOS velocity dispersion (m/s)
        :rtype s_b: float
        :return fwhm_n: Narrow LOS velocity FWHM (m/s)
        :rtype fwhm_n: float
        :return fwhm_b: Broad LOS velocity FWHM (m/s)
        :rtype fwhm_b: float
    """

    # Calculate redshift
    z = (fit_in.o3rn_c.value / params_nifs.o3r) - 1

    # Calculate velocity
    v1 = params_nifs.c * ((z ** 2) + (2 * z)) / ((z ** 2) + (2 * z) + 2)

    # Calculate LOS velocity dispersion
    s_n = params_nifs.c * fit_in.o3rn_s.value / fit_in.o3rn_c.value
    s_b = params_nifs.c * fit_in.b_s.value / fit_in.b_c.value
    fwhm_n = s_n * 2 * np.sqrt(2 * np.log(2))
    fwhm_b = s_b * 2 * np.sqrt(2 * np.log(2))

    return z, v1, s_n, s_b, fwhm_n, fwhm_b


def phys_lines_k(fit_in, hab_i=None):
    """Calculate physical parameters from the fitted lines in the K band
    spectrum.

    Parameters:
        :param fit_in: Fitting model
        :type fit_in: astropy.modeling.core.k_prof
        :param hab_i: Broad H_a integrated flux (erg/s/scm). If 'None', the
            value in 'fit_in' is used
        :type hab_i: float, optional

    Returns:
        :return z: Redshift of the narrow lines
        :rtype z: float
        :return d_a: Angular scale of the narrow lines (kpc/")
        :rtype d_a: float
        :return v_hab: LOS velocity of the H_a broad line (m/s)
        :rtype v_hab: float
        :return v_n: LOS velocity of the narrow lines (m/s)
        :rtype v_n: float
        :return s_hab: LOS velocity dispersion of the H_a broad line (m/s)
        :rtype s_hab: float
        :return s_n: LOS velocity dispersion of the narrow lines (m/s)
        :rtype s_n: float
        :return fwhm_hab: LOS velocity FWHM of the H_a broad line (m/s)
        :rtype fwhm_hab: float
        :return fwhm_n: LOS velocity FWHM of the narrow lines (m/s)
        :rtype fwhm_n: float
        :return m_bh: SMBH mass (M_Sun)
        :rtype m_bh: float
    """

    # Calculate redshift
    z_hab = (fit_in.hab_c.value / params_nifs.h_alpha) - 1
    z = (fit_in.han_c.value / params_nifs.h_alpha) - 1

    # # Calculate angular scale
    d_a = cosmology.Planck18.kpc_proper_per_arcmin(z).value / 60

    # Calculate velocity
    v_hab = params_nifs.c * ((z_hab ** 2) + (2 * z_hab)) / \
        ((z_hab ** 2) + (2 * z_hab) + 2)
    v_n = params_nifs.c * ((z ** 2) + (2 * z)) / ((z ** 2) + (2 * z) + 2)

    # Calculate LOS velocity dispersion
    s_hab = params_nifs.c * fit_in.hab_s.value / fit_in.hab_c.value
    s_n = params_nifs.c * fit_in.han_s.value / fit_in.han_c.value
    fwhm_hab = s_hab * 2 * np.sqrt(2 * np.log(2))
    fwhm_n = s_n * 2 * np.sqrt(2 * np.log(2))

    # Calculate SMBH mass
    if not hab_i:
        hab_i = fit_in.hab_i.value

    d_l = cosmology.Planck18.luminosity_distance(z).to(units.cm).value
    # [ref2]
    l_hab = 4 * np.pi * hab_i * (d_l ** 2)
    warnings.simplefilter('ignore')
    m_bh = (10 ** 6.71) * ((l_hab / 1e42) ** 0.48) * ((fwhm_hab / 1e6) ** 2.12)
    # [ref1, eq. 2]
    warnings.simplefilter('default')

    return z, d_a, v_hab, v_n, s_hab, s_n, fwhm_hab, fwhm_n, m_bh


def fit_map_h(wvl, cube, fit_range, bb_0_range, sigma_range=2, n_spaces=0,
              fit_b=True):
    """Calculate the fitted parameter maps of a H band data cube, spaxel by
    spaxel.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube (erg/s/cm^2/um)
        :type cube: numpy.ndarray [n, y, x] (float)
        :param fit_range: Wavelength range to fit the profile (min, max) (um)
        :type fit_range: list [2] (float)
        :param bb_0_range: Wavelength range to measure the black body
            offset (min, max) (um)
        :type bb_0_range: list [2] (float)
        :param sigma_range: Fractional half-range of narrow lines' sigma
        :type sigma_range: float, default 2
        :param n_spaces: Number of spaces before the printed string
        :type n_spaces: int, default 0
        :param fit_b: Fit a broad line
        :type fit_b: bool, default True

    Returns:
        :return maps: Map of fitting models
        :rtype maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof)
    """

    # Prepare output
    maps = np.tile(np.array(k_prof()), [cube.shape[1], cube.shape[2]])

    # Scan the map
    maxpix = cube.shape[1] * cube.shape[2]

    for i_row in range(cube.shape[1]):
        for i_col in range(cube.shape[2]):
            npix = (i_row * cube.shape[2]) + i_col + 1
            print("\r" + (n_spaces * " ") +
                  "Processing pixel {0}/{1} ...".format(npix, maxpix),
                  end="")
            spectr = cube[:, i_row, i_col]
            h_param = fit_h(wvl, spectr, fit_range, bb_0_range,
                            sigma_range=sigma_range, fit_b=fit_b)
            maps[i_row, i_col] = h_param

    print()

    return maps


def fit_map_k(wvl, cube, wvl_range_cont, tie_z=False, tie_s=False, fix=False,
              fix_pars=None, n_spaces=0):
    """Calculate the fitted parameter maps of a K band data cube, spaxel by
    spaxel.
    There is the option to fix the position and width of the broad H_a line and
    the width of all narrow lines.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube (erg/s/cm^2/um)
        :type cube: numpy.ndarray [n, y, x] (float)
        :param wvl_range_cont: Wavelength range to measure the continuum
            (min, max) (um)
        :type wvl_range_cont: list [2] (float)
        :param tie_z: Tie the redshift of narrow H_a and [NII] lines
        :type tie_z: bool, default False
        :param tie_s: Tie the redshift and width of [SII] lines
        :type tie_s: bool, default False
        :param fix: Fix the position and width of the broad H_a line and the
            width of all narrow lines
        :type fix: bool, default False
        :param fix_pars: Fitting model to fix positions and widths of lines
            selected in 'fix'
        :type fix_pars: astropy.modeling.core.k_prof, optional
        :param n_spaces: Number of spaces before the printed string
        :type n_spaces: int, default 0

    Returns:
        :return maps: Map of fitting models
        :rtype maps: numpy.ndarray [y, x] (astropy.modeling.core.k_prof)
    """

    # Prepare output
    maps = np.tile(np.array(k_prof()), [cube.shape[1], cube.shape[2]])

    # Scan the map
    maxpix = cube.shape[1] * cube.shape[2]

    for i_row in range(cube.shape[1]):
        for i_col in range(cube.shape[2]):
            npix = (i_row * cube.shape[2]) + i_col + 1
            print(("\r" + (n_spaces * " ") +
                   "Processing pixel {0}/{1} ...").format(npix, maxpix),
                  end="")
            spectr = cube[:, i_row, i_col]
            k_param = fit_k(wvl, spectr, wvl_range_cont, tie_z=tie_z,
                            tie_s=tie_s, fix=fix, fix_pars=fix_pars)
            maps[i_row, i_col] = k_param

    print()

    return maps


def moffat_integr(model_fit):
    """Calculate the integral of a Moffat profile.

    Parameters:
        :param model_fit: 2D Moffat fitting parameters (peak, background,
            central x coordinate (px), central y coordinate (px), first width
            parameter (px), second width parameter (px), position angle (rad),
            exponential parameter)
        :type model_fit: list [8] (float)

    Returns:
        :return prof: Profile integral (in units of the peak)
        :rtype prof: float
    """

    # Create profile
    side = 101
    x_mesh, y_mesh = np.meshgrid(np.arange(side), np.arange(side))
    prof = moffat2d_prof(x_mesh, y_mesh, amplitude=model_fit[0],
                         x_0=int((side - 1) / 2), y_0=int((side - 1) / 2),
                         s1=model_fit[4], s2=model_fit[5], theta=model_fit[6],
                         alpha=model_fit[7])

    # Integrate profile
    integr = np.sum(prof)

    return integr
