"""Miscellaneous functions for NIFS.
"""


from urllib.parse import urlencode

from astropy.io import fits
import numpy as np

import cube_nifs
import params_nifs


def plot_aladin(survey, coor, fov, size, ticks, target, rad_fov=None,
                catalog=None, c_map='inferno', close=True, save=None):
    """Plot an image from a survey, using the Aladin (HiPS) service.

    Parameters:
        :param survey: Survey name
        :type survey: str
        :param coor: Central coordinates ([RA, dec]) [deg]
        :type coor: list [2] (float)
        :param fov: FOV side [arcsec]
        :type fov: float
        :param size: Image size ([width, height]) [px]
        :type size: list [2] (int)
        :param ticks: Distance of the pixel axes' ticks ["]
        :type ticks: float
        :param target: Target name
        :type target: str
        :param rad_fov: Radius of the plotted circular FOV ["]
        :type rad_fov: float, optional
        :param catalog: Catalog of plotted stars
            (['x', 'y', 'label', 'symbol', 'color']) [deg]
        :type catalog: astropy.table [n, 4] ([float, float, str, str, str]),
            optional
        :param c_map: Colormap
        :type c_map: str, default 'inferno'
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Query
    query_par = {'hips': survey, 'ra': coor[0], 'dec': coor[1],
                 'fov': (fov / params_nifs.deg_to_arcsec), 'width': size[0],
                 'height': size[1]}
    url_par = urlencode(query_par)
    url = f'http://alasky.u-strasbg.fr/hips-image-services/hips2fits?{url_par}'
    img = fits.getdata(url, ext=0)
    img = np.fliplr(img)
    header_file = fits.open(url)
    header = header_file[0].header
    header_file.close()
    ps = np.abs(header['CDELT1'] * params_nifs.deg_to_arcsec)

    if catalog:
        catalog['x'][:] -= coor[0]
        catalog['y'][:] -= coor[1]
        catalog['x'][:] /= np.abs(header['CDELT1'])
        catalog['y'][:] /= np.abs(header['CDELT2'])
        catalog['x'][:] += header['CRPIX1']
        catalog['y'][:] += header['CRPIX2']

    # Plot image
    cube_nifs.plot_img(img, ps, ticks,
                       beam_circ=np.array([[header['CRPIX1'], header['CRPIX2'],
                                            (rad_fov / ps)]]),
                       catalog=catalog, catalog_labels=True,
                       c_map=c_map, c_bar=False,
                       title="{0}\nRA: {1:.6f} deg, Dec: {2:.6f} deg".format(
                           target, coor[0], coor[1]), compass=True,
                       close=close, save=save, save_pdf=False)
