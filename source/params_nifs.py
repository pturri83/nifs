"""List of fixed parameters to analyze reduced NIFS data cubes.

References:
    [ref1] Ivison et al., 2019
    [ref2] Allen, "Astrophysical Quantities"
    [ref3] Rousselot et al., 2000
    [ref4] Proxauf et al., 2014
    [ref5] Dimitrijević et al., 2007
    [ref6] Kramida et al., 2019
    [ref7] Oliva & Origlia, 1992
    [ref8] https://www.gemini.edu/instrumentation/nifs/components
    [ref9] Ryden, 2017
"""


import numpy as np


# Fixed parameters (math)
rad_to_arcsec = 180 * 60 * 60 / np.pi  # Conversion from radians to arcseconds
deg_to_arcsec = 60 * 60  # Conversion from degrees to arcseconds

# Fixed parameters (instrument)
d_out_gemini = 8  # Gemini telescope outer diameter (m)
d_in_gemini = 1.12  # Gemini telescope inner diameter (m)
gain = 2.8  # NIFS gain (e-/count) [ref8]
low_row = 9  # Lowest row of spaxels with valid values
lambda_seeing = 0.5  # Wavelngth used to measure the seeing by Gemini (um)
lambda_h = 1.65  # Central wavelngth of the H grating (um)
lambda_k = 2.2  # Central wavelngth of the H grating (um)
sps = 0.05  # Spaxel scale ("/spx)

# Fixed parameters (physics)
c = 2.9979e8  # Speed of light (m/s) [ref2]
omega_r0 = 9e-5  # Radiation density parameter [ref9, pag. 110]
omega_m0 = 0.31  # Matter density parameter [ref9, pag. 110]
omega_l0 = 0.69  # Cosmological constant density parameter [ref9, pag. 110]
H_0 = 0.068  # Hubble constant (m/s/pc) [ref9, eq. 2.10]
h_alpha = 0.656279  # H_a wavelength at rest in vacuum (um) [ref6]
h_beta = 0.486135  # H_b wavelength at rest in vacuum (um) [ref6]
n2r = 0.6583  # Red [NII] wavelength at rest in vacuum (um) [ref2]
n2b = 0.6548  # Blue [NII] wavelength at rest in vacuum (um) [ref2]
n2_ratio = 3.07  # Ratio of red and blue [NII] lines peaks [ref5]
s2b = 0.6716  # Blue [SII] wavelength at rest in vacuum (um) [ref2]
s2r = 0.673  # Red [SII] wavelength at rest in vacuum (um) [ref2]
s2_ratio = 1.43  # Ratio of blue and red [SII] lines peaks [ref4]
o3r = 0.5007  # Red [OIII] wavelength at rest in vacuum (um) [ref5]
o3b = 0.4959  # Blue [OIII] wavelength at rest in vacuum (um) [ref5]
o3_ratio = 2.98  # Ratio of red and blue [OIII] lines peaks [ref5]
sky_c = [1.4740015, 1.4755589, 1.4772383, 1.4783736, 1.4799240, 1.4800423,
         1.4805789, 1.4833093, 1.4864397, 1.4886165, 1.4887699, 1.4908278,
         1.4909776, 1.4931883, 1.5052866, 1.5055550, 1.5068968, 1.5088276,
         1.518714, 1.52409540, 1.5287789, 1.5332402, 1.5395334, 1.5430163,
         1.5432156, 1.5500864, 1.5509769, 1.5517872, 1.5539711, 1.5540945,
         1.5546141, 1.5570159, 1.5597631, 1.5631443, 1.5654953, 1.5656178,
         1.5657749, 1.5702539, 1.5830335, 1.5833272, 1.5848061, 1.5869307,
         1.5972596, 1.6030831, 1.6079753, 1.6128608, 1.6194615, 1.6235376,
         1.6317161, 1.6341755, 1.6350650, 1.6351954, 1.6360385, 1.6388492,
         1.6414737, 1.6442155, 1.6447616, 1.6475648, 1.6477318, 1.6479061,
         1.6502365, 1.6553814, 1.6689201, 1.6692380, 1.6708852, 1.6732479,
         1.6733659, 1.6840481, 1.6903679, 1.6955078, 1.7008757, 1.7078369,
         1.7123659, 1.7210319, 1.7211664, 1.7247926, 1.7249320, 1.7282868,
         1.7303370, 1.7330869, 1.7351152, 1.7359686, 1.7382906, 1.7384705,
         1.7386695, 1.7427045, 1.7449967, 1.7505897, 1.7528254, 1.7530475,
         1.7649853, 1.7653222, 1.7671812, 1.7698442, 1.7880298, 1.7993962,
         2.0005015, 2.0008163, 2.0033211, 2.0057585, 2.0068995, 2.0193227,
         2.0275839, 2.0339497, 2.0412680, 2.0499364, 2.0562953, 2.0564143,
         2.0728170, 2.0860246, 2.0909451, 2.1115856, 2.1176557, 2.1249592,
         2.1505044, 2.1507308, 2.1537522, 2.1707826, 2.1711170, 2.1802312,
         2.1873518, 2.1955637, 2.2052366, 2.2124875, 2.2126160, 2.2247809,
         2.2311799, 2.2460264, 2.2516727, 2.26844, 2.27359]  # Central
# wavelengths of sky lines in the vacuum (um) [ref3, ref7]
sky_w = [2, 1.5]  # Width of sky lines masked (H, K) (nm)

# Guessed parameters
z = 2.405  # Approximate redshift of 084933A [ref1]
sigma_b = 0.04  # Initial guess of the STD of the H_a broad line (um)
sigma_n = 0.002  # Initial guess of the STD of narrow lines (H_a, [NII], [SII])
# (um)
sigma_n_flux = 3  # Number of times the initial guess of the STD of narrow
# lines is used to measure the flux density of narrow lines
dv = 5e5  # Maximum velocity shift of narrow lines allowed respect to the
# guessed redshift (m/s)
moffat_alpha = 2.3  # Moffat profile exponential

# Ivison et al. (2019) (main spectrum in Fig. 1) parameters [ref1]
rad_cal = 0.5  # Radius of the aperture used (")
peak_cal = 4.9  # Peak flux density of the fitted broad H_a in the central
# aperture (10^-14 erg/s/cm^2/um)
cont_cal = 2.1  # Flux density of the continuum of the fitted broad H_a in the
# central aperture (10^-14 erg/s/cm^2/um)
flux_unit = r"erg/s/cm$^{2}$"  # Flux units
flux_dens_unit = r"erg/s/cm$^{2}$/$\mu$m"  # Flux density units
flux_spat_unit = r"erg/s/cm$^{2}$/arcsec$^{2}$"  # Spatial flux density units
flux_dens_spat_unit = r"erg/s/cm$^{2}$/$\mu$m/arcsec$^{2}$"  # Spectral and
# spatial flux density units
flux_dens_unit_unc = r"counts/$\mu$m"  # Uncalibrated flux density units
flux_spat_unit_unc = r"counts/arcsec$^{2}$"  # Uncalibrated spatial flux
# density units
flux_dens_spat_unit_unc = r"counts/$\mu$m/arcsec$^{2}$"  # Uncalibrated
# spectral and spatial flux density units


# Parameters' functions
def sky_compile(sky_centers, sky_widths):
    """Calculate the ranges of wavelengths masked by telluric lines.
    Neighboring lines are merged.

    Parameters:
        :param sky_centers: Central wavelengths of sky lines masked (um)
        :type sky_centers: list [n] (list [2] (float))
        :param sky_widths: Width of sky lines masked (nm)
        :type sky_widths: float

    Returns:
        :return lines_merge: Telluric lines ranges (min, max) (um)
        :rtype lines_merge: numpy.ndarray [:, 2] (float)
    """

    # Prepare line ranges
    lines = np.zeros([len(sky_centers), 2])
    for i_line in range(len(sky_centers)):
        lines[i_line, :] = sky_centers[i_line] + ((sky_widths / 1000 / 2) *
                                                  np.array([-1, 1]))

    # Merge lines
    lines_merge = np.array([lines[0, :]])

    for i_line in range(len(sky_centers) - 1):
        if lines[(i_line + 1), 0] > lines_merge[-1, 1]:
            lines_merge = np.concatenate((lines_merge,
                                          np.array([lines[(i_line + 1), :]])))
        else:
            lines_merge[-1, 1] = lines[(i_line + 1), 1]

    return lines_merge
