"""Functions to analyze reduced NIFS data cubes.
"""


import copy
import itertools
from os import path
import warnings

from matplotlib import patches, pyplot as plt, ticker
from mpl_toolkits import axes_grid1
import numpy as np
from scipy import ndimage

from source import fit_nifs, misc_girmos, params_nifs


def collapse_cube(cube):
    """Collapse a cube using the sum.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)

    Returns:
        :return coll: Collapsed data
        :rtype coll: numpy.ndarray [y, x] (float)
    """

    # Bin spaxels
    coll = np.sum(cube, axis=0)

    return coll


def beam(cube, direction, rad):
    """Select the spaxels of a data cube along a direction and within a circle.
    The direction is measured from the center of the field.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param direction: Direction [x, y] of the beam respect to the center of
            the field (px)
        :type direction: list [2] (float)
        :param rad: Beam radius (px)
        :type rad: float

    Returns:
        :return beam_out: Coordinates [y, x] of the spaxels in the beam (px)
        :rtype beam_out: tuple [2] (numpy.ndarray [m] (int))
    """

    # Select beam
    beam_lin_x = np.linspace(0, (cube.shape[2] - 1), cube.shape[2]) - \
        direction[0]
    beam_lin_y = np.linspace(0, (cube.shape[1] - 1), cube.shape[1]) - \
        direction[1]
    beam_mesh_x, beam_mesh_y = np.meshgrid(beam_lin_x, beam_lin_y)
    beam_dist = np.hypot(beam_mesh_x, beam_mesh_y)
    beam_out = np.where(beam_dist <= rad)

    return beam_out


def beam_sq(cube, direction, side):
    """Select the spaxels of a data cube along a direction and within a square.
    The direction is measured from the center of the field.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param direction: Direction [x, y] of the beam respect to the center of
            the field (px)
        :type direction: list [2] (float)
        :param side: Beam side (px)
        :type side: float

    Returns:
        :return beam_out: Coordinates [y, x] of the spaxels in the beam (px)
        :rtype beam_out: tuple [2] (numpy.ndarray [m] (int))
    """

    # Select beam
    beam_lin_x = np.linspace(0, (cube.shape[2] - 1), cube.shape[2]) - \
        direction[0]
    beam_lin_y = np.linspace(0, (cube.shape[1] - 1), cube.shape[1]) - \
        direction[1]
    beam_mesh_x, beam_mesh_y = np.meshgrid(beam_lin_x, beam_lin_y)
    side2 = side / 2
    beam_out = np.where((beam_mesh_x <= side2) & (beam_mesh_x > -side2) &
                        (beam_mesh_y <= side2) & (beam_mesh_y > -side2))

    return beam_out


def bin_spx(cube, x, y, method='sum'):
    """Bin spaxels using the sum, mean or median.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param x: x coordinates of the spaxels (px)
        :type x: list [n] (int)
        :param y: y coordinates of the spaxels (px)
        :type y: list [n] (int)
        :param method: Method for combining the spectrum ('sum' for sum, 'mean'
            for mean, 'median' for median)
        :type method: str, default 'sum'

    Returns:
        :return binned: Binned data
        :rtype binned: numpy.ndarray [n] (float)
    """

    # Bin spaxels
    if method == 'sum':
        binned = np.sum(cube[:, y, x], axis=1)
    elif method == 'mean':
        binned = np.mean(cube[:, y, x], axis=1)
    else:
        binned = np.median(cube[:, y, x], axis=1)

    return binned


def bin_cube(cube, bin_f, method='sum'):
    """Bin a full data cube using the sum, mean or median.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y0, x0] (float)
        :param bin_f: Binning factor
        :type bin_f: int
        :param method: Method for combining the spectrum ('sum' for sum, 'mean'
            for mean, 'median' for median)
        :type method: str, default 'sum'

    Returns:
        :return binned: Binned cube
        :rtype binned: numpy.ndarray [n, y1, x1] (float)
    """

    # Bin cube
    shape_in = cube.shape
    x = int(((shape_in[2] / 2) // bin_f) * 2)
    y = int(((shape_in[1] / 2) // bin_f) * 2)
    x_0 = int((shape_in[2] / 2) - (((shape_in[2] / 2) // bin_f) * bin_f))
    y_0 = int((shape_in[1] / 2) - (((shape_in[1] / 2) // bin_f) * bin_f))
    binned = np.zeros([shape_in[0], y, x])

    for i_row in range(y):
        for i_col in range(x):
            xs = np.arange((i_col * bin_f), ((i_col + 1) * bin_f)) + x_0
            ys = np.arange((i_row * bin_f), ((i_row + 1) * bin_f)) + y_0
            binned[:, i_row, i_col] = bin_spx(cube, xs, ys, method=method)

    return binned


def bin_spectrum(wvl, spectrum, bin_f, method='mean'):
    """Bin wavelengths and a 1-D spectrum using the mean or median.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectrum: 1-D Spectrum
        :type spectrum: numpy.ndarray [n] (float)
        :param bin_f: Binning factor
        :type bin_f: int
        :param method: Method for combining the spectrum ('mean' for mean,
            'median' for median)
        :type method: str, default 'mean'

    Returns:
        :return wvl_bin: Binned wavelengths
        :rtype wvl_bin: numpy.ndarray [m] (float)
        :return spectrum_bin: Binned 1-D spectrum
        :rtype spectrum_bin: numpy.ndarray [m] (float)
    """

    # Bin wavelengths and spectrum
    len_new = len(wvl) // bin_f
    wvl_bin = np.zeros(len_new)
    spectrum_bin = np.zeros(len_new)

    for i_wvl in range(len_new):
        idxs = np.arange((i_wvl * bin_f), ((i_wvl + 1) * bin_f))
        wvl_bin[i_wvl] = np.mean(wvl[idxs])

        if method == 'mean':
            spectrum_bin[i_wvl] = np.mean(spectrum[idxs])
        else:
            spectrum_bin[i_wvl] = np.median(spectrum[idxs])

    return wvl_bin, spectrum_bin


def cut_spectrum(wvl, spectrum, wvl_min, wvl_max):
    """Cut wavelengths and a 1-D spectrum between two wavelengths values.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectrum: 1-D Spectrum
        :type spectrum: numpy.ndarray [n] (float)
        :param wvl_min: Minimum wavelength to include in the cut (um)
        :type wvl_min: float
        :param wvl_max: Maximum wavelength to include in the cut (um)
        :type wvl_max: float

    Returns:
        :return wvl_cut: Cut wavelengths
        :rtype wvl_cut: numpy.ndarray [m] (float)
        :return spectrum_cut: Cut 1-D spectrum
        :rtype spectrum_cut: numpy.ndarray [m] (float)
    """

    # Cut wavelengths and spectrum
    wvl_where = np.where((wvl >= wvl_min) & (wvl <= wvl_max))[0]
    wvl_cut = wvl[wvl_where]
    spectrum_cut = spectrum[wvl_where]

    return wvl_cut, spectrum_cut


def cut_cube(wvl, cube, wvl_min, wvl_max):
    """Cut wavelengths and a cube between two wavelengths values.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param wvl_min: Minimum wavelengths to include in the slice (um)
        :type wvl_min: float
        :param wvl_max: Minimum wavelengths to include in the slice (um)
        :type wvl_max: float

    Returns:
        :return wvl_cut: Sliced wavelengths
        :rtype wvl_cut: numpy.ndarray [m] (float)
        :return cube_cut: Sliced cube
        :rtype cube_cut: numpy.ndarray [m, y, x] (float)
    """

    # Slice cube
    wvl_where = np.where((wvl >= wvl_min) & (wvl <= wvl_max))[0]
    wvl_cut = wvl[wvl_where]
    cube_cut = cube[wvl_where, :, :]

    return wvl_cut, cube_cut


def clean_spectrum(wvl, spectrum, centers, width):
    """Cut wavelengths and a 1-D spectrum around lines. Each line is defined by
    its center and width.
    Useful to remove telluric lines.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectrum: 1-D Spectrum
        :type spectrum: numpy.ndarray [n] (float)
        :param centers: Centers of the lines (um)
        :type centers: list [p] (float)
        :param width: Width of the lines (nm)
        :type width: float

    Returns:
        :return wvl_clean: Cleaned wavelengths
        :rtype wvl_clean: numpy.ndarray [m] (float)
        :return spectrum_clean: Cleaned 1-D spectrum
        :rtype spectrum_clean: numpy.ndarray [m] (float)
    """

    # Cut wavelengths and spectrum
    wvl_clean = []
    spectrum_clean = []

    for i_line in range(len(centers)):
        wvl_min = (centers[i_line] - (width / 1000 / 2))
        wvl_max = (centers[i_line] + (width / 1000 / 2))
        wvl_where = np.where((wvl < wvl_min) | (wvl > wvl_max))[0]
        wvl_clean = wvl[wvl_where]
        spectrum_clean = spectrum[wvl_where]

    return wvl_clean, spectrum_clean


def clean_cube(wvl, cube, centers, width):
    """Cut wavelengths and a cube spectrum around lines. Each line is defined
    by its center and width.
    Useful to remove telluric lines.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param centers: Centers of the lines (um)
        :type centers: list [p] (float)
        :param width: Width of the lines (nm)
        :type width: float

    Returns:
        :return wvl_clean: Cleaned wavelengths
        :rtype wvl_clean: numpy.ndarray [m] (float)
        :return spectrum_clean: Cleaned 1-D spectrum
        :rtype spectrum_clean: numpy.ndarray [m] (float)
    """

    # Cut wavelengths and spectrum
    wvl_clean = copy.deepcopy(wvl)
    cube_clean = copy.deepcopy(cube)

    for i_line in range(len(centers)):
        wvl_min = (centers[i_line] - (width / 1000 / 2))
        wvl_max = (centers[i_line] + (width / 1000 / 2))
        wvl_where = np.where((wvl_clean < wvl_min) | (wvl_clean > wvl_max))[0]
        wvl_clean = wvl_clean[wvl_where]
        cube_clean = cube_clean[wvl_where]

    return wvl_clean, cube_clean


def plot_img(img, ps, ticks, direction=None, beam_pos=None, beam_circ=None,
             beam_squ=None, model=None, catalog=None, catalog_labels=False,
             imin=None, imax=None, ax_labels=True, c_map='inferno', c_bar=True,
             c_label=None, title="", ignore_bottom=False, perc=0,
             compass=False, legend=True, plot_1d=None, printing=False,
             close=True, save=None, save_pdf=True):
    """Plot an image.

    It can overplot beams or FWHM ellipses. It can also plot a catalog of
    targets with different symbols and labels.

    Parameters:
        :param img: Image
        :type img: numpy.ndarray [y, x] (float)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float
        :param direction: Direction [x, y] of a single beam (")
        :type direction: list [2] (float), optional
        :param beam_pos: Coordinates [x, y] of the spaxels in a single beam
            (px)
        :type beam_pos: tuple [2] (numpy.ndarray [m] (int)), optional
        :param beam_circ: Circles [x, y, radius] of beam (px)
        :type beam_circ: numpy.ndarray [n, 3] (float), optional
        :param beam_squ: Squares [x, y, side] of beam (px)
        :type beam_squ: numpy.ndarray [n, 3] (float), optional
        :param model: Parameters of the FWHM ellipse [x0, y0, first FWHM,
            second FWHM, angle, average FWHM] ([px, px, px, px, deg, "])
        :type model: list [6] (float) or list (list [6] (float)), optional
        :param catalog: Catalog of plotted stars
            (['x', 'y', 'label', 'symbol', 'color']) [px]
        :type catalog: astropy.table [n, 4] ([float, float, str, str, str]),
            optional
        :param catalog_labels: Catalog items labels
        :type catalog_labels: bool, default False
        :param imin: Minimum intensity value shown
        :type imin: float, optional
        :param imax: Maximum intensity value shown
        :type imax: float, optional
        :param ax_labels: Axis labels and ticks
        :type ax_labels: bool, default True
        :param c_map: Colormap
        :type c_map: str, default 'inferno'
        :param c_bar: Colorbar
        :type c_bar: bool, default True
        :param c_label: Colorbar label
        :type c_label: str, optional
        :param title: Plot title
        :type title: str, default ""
        :param ignore_bottom: Ignore the bottom of the FOV to choose
            automatically the maximum color value
        :type ignore_bottom: bool, default False
        :param perc: Percentile to remove from the top and bottom of the range
            of values colored (0 for none). Not used if imin or imax are used
        :type perc: float, default 0
        :param compass: Show compass
        :type compass: bool, default False
        :param legend: Show legend
        :type legend: bool, default True
        :param plot_1d: Horizontal 1-D to overplot
        :type plot_1d: numpy.ndarray [x] (float), optional
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
        :param save_pdf: Save also a PDF version
        :type save_pdf: bool, default True

    Returns:
        :return ax: Axes handle
        :rtype ax: matplotlib.axes._subplots.AxesSubplot
    """

    # Mask image
    img = np.ma.masked_invalid(img)

    # Calculate image parameters
    x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
        misc_girmos.ticks_scale(img.shape, ps, ticks)

    if imin is None:
        if perc > 0:
            imin = np.percentile(img.compressed(), perc)
        else:
            imin = np.min(img)

    if imax is None:
        if ignore_bottom:
            y_mesh, _ = np.indices(img.shape)
            imax_where = np.where(y_mesh >= params_nifs.low_row)
            img_ignore = img[imax_where]

            if perc > 0:
                imax = np.percentile(img_ignore.compressed(), (100 - perc))
            else:
                imax = np.max(img_ignore)
        else:
            if perc > 0:
                imax = np.percentile(img.compressed(), (100 - perc))
            else:
                imax = np.max(img)

    if not c_label and c_bar:
        c_label = ""

    # Plot the image
    fig = plt.figure(figsize=(6, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.6, 0.7])
    ax_pos = ax.imshow(img, cmap=c_map, aspect='equal', origin='lower',
                       vmin=imin, vmax=imax)

    if np.any(plot_1d):
        plt.plot(plot_1d, 'g-', linewidth=4)

    plt.title(title, y=1.05, fontsize=16)

    if c_bar:
        divider = axes_grid1.make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.1)
        cb = plt.colorbar(ax_pos, cax=cax)
        cb.set_label(label=c_label, size=16)
        cb.ax.tick_params(labelsize=14)
        cb.ax.yaxis.get_offset_text().set(size=14)
        cb.ax.yaxis.set_offset_position('left')

    if ax_labels:
        ax.set_xlabel("x (\")", fontsize=16, labelpad=0)
        ax.set_ylabel("y (\")", fontsize=16, labelpad=-2)
        ax.set_xticks(x_ticks_orig)
        ax.set_yticks(y_ticks_orig)
        ax.set_xticklabels(x_ticks_str, fontsize=14)
        ax.set_yticklabels(y_ticks_str, fontsize=14)
    else:
        plt.tick_params(axis='both', bottom=False, left=False,
                        labelbottom=False, labelleft=False)

    ax.invert_xaxis()

    # Show beam
    if direction is not None:
        ax.plot(direction[0], direction[1], '+k', markeredgewidth=2,
                label="Beam direction")

        if (not printing) and legend:
            ax.legend()

    if beam_pos is not None:
        for i_px in range(len(beam_pos[0])):
            sq = patches.Rectangle(((beam_pos[1][i_px] - 0.5),
                                    (beam_pos[0][i_px] - 0.5)), 1, 1,
                                   linewidth=0.8, edgecolor='g',
                                   facecolor='none')

            if i_px == 0:
                sq._label = "Beam pixels"

            ax.add_patch(sq)

        if (not printing) and legend:
            ax.legend()

    if beam_circ is not None:
        for i_beam in range(beam_circ.shape[0]):
            circ = patches.Circle((beam_circ[i_beam, 0], beam_circ[i_beam, 1]),
                                  radius=beam_circ[i_beam, 2], ls='-',
                                  linewidth=2, edgecolor='g', facecolor='none')
            ax.add_patch(circ)

    if beam_squ is not None:
        for i_beam in range(beam_squ.shape[0]):
            sq = patches.Rectangle(((beam_squ[i_beam, 0] -
                                     (beam_squ[i_beam, 2] / 2)),
                                    (beam_squ[i_beam, 1] -
                                     (beam_squ[i_beam, 2] / 2))),
                                   beam_squ[i_beam, 2], beam_squ[i_beam, 2],
                                   ls='-', linewidth=2, edgecolor='g',
                                   facecolor='none')
            ax.add_patch(sq)

    # Show FWHM ellipse
    if model is not None:
        if any(isinstance(model_one, list) for model_one in model):
            for model_one in model:
                ax.plot(model_one[0], model_one[1], '+k', markeredgewidth=2,
                        label="Center")
                ell = patches.Ellipse((model_one[0], model_one[1]),
                                      model_one[2], model_one[3],
                                      angle=model_one[4], linewidth=0.8,
                                      edgecolor='g',
                                      facecolor='none')
                ax.add_patch(ell)
        else:
            ax.plot(model[0], model[1], '+g', markersize=10,
                    markeredgewidth=1.5, label="Center")
            label_fwhm = "FWHM = {0:.3f}\"".format(model[5])
            ell = patches.Ellipse((model[0], model[1]), model[2], model[3],
                                  angle=model[4], linewidth=1.5, edgecolor='g',
                                  facecolor='none', label=label_fwhm)
            ax.add_patch(ell)

            if (not printing) and legend:
                ax.legend()

    # Show catalog
    label_offset = 10

    if catalog:
        xlims = ax.get_xlim()
        ylims = ax.get_ylim()

        ax.scatter(beam_circ[0, 0], beam_circ[0, 1], s=100, color='w',
                   marker='+')

        for i_cat in catalog:
            ax.scatter(i_cat['x'], i_cat['y'], s=100,
                       edgecolors=i_cat['color'][0], facecolors='none',
                       marker=i_cat['symbol'][0])

            if catalog_labels:
                if i_cat['symbol'][0] == 'D':
                    label_offset_sign = 1
                else:
                    label_offset_sign = -1

                ax.annotate(i_cat['label'], [i_cat['x'] - label_offset,
                                             i_cat['y'] + (label_offset *
                                                           label_offset_sign)],
                            c=i_cat['color'][0])

        plt.xlim(xlims)
        plt.ylim(ylims)

    # Plot compass
    if compass:
        ax.annotate('E', xy=(1.358, -0.095), xytext=(1.208, -0.095),
                    ha='right', va='center', xycoords='axes fraction',
                    arrowprops=dict(arrowstyle='<-'), fontsize=16)
        ax.annotate('N', xy=(1.35, -0.1), xytext=(1.35, 0.05), ha='center',
                    xycoords='axes fraction', arrowprops=dict(arrowstyle='<-'),
                    fontsize=16)

    # Save image file
    if save is not None:
        plt.savefig(save)

        if save_pdf:
            warnings.simplefilter('ignore')
            plt.savefig(path.splitext(save)[0] + '.pdf')
            warnings.simplefilter('default')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)

    return ax


# noinspection PyTypeChecker
def plot_spectrum(wvl, spectrum, sky=None, fit_mod=None, zoom=None,
                  i_range=None, i_range_res=None, show_lines='all',
                  label_lines='top', y_label=None, title="", fit_b=True,
                  close=True, save=None):
    """Plot a 1-D spectrum.
    It can overplot a fitted model. It can gray-out wavelengths that have sky
    lines. It can plot also a spectrum with the residual from the fitted model.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectrum: 1-D Spectrum
        :type spectrum: numpy.ndarray [n] (float)
        :param sky: Telluric lines ranges (min, max) (um)
        :type sky: numpy.ndarray [:, 2] (float), optional
        :param fit_mod: Spectrum fitting model
        :type fit_mod: astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof, optional
        :param zoom: Wavelength zoom limits [min, max] (um)
        :type zoom: list [2] (float), optional
        :param i_range: Spectrum intensity limits [min, max]
        :type i_range: list [2] (float), optional
        :param i_range_res: Residual spectrum intensity limits [min, max]
        :type i_range_res: list [2] (float), optional
        :param show_lines: Show lines centers and labels (None, 'all', 'single'
            for narrow H_a line or narrow red [OIII] only, 'no_broad' for all
            except broad H_a line)
        :type show_lines: str, default 'all'
        :param label_lines: Position of the lines labels ('top', 'bottom',
            'top_bottom' for all top except narrow H_a line)
        :type label_lines: str, default 'top'
        :param y_label: y axis label
        :type y_label: str, optional
        :param title: Plot title
        :type title: str, default ""
        :param fit_b: Broad line was fitted
        :type fit_b: bool, default True
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Plot the spectrum
    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    plot_spectrum_in(ax, wvl, spectrum, sky=sky, fit_mod=fit_mod, zoom=zoom,
                     i_range=i_range, show_lines=show_lines,
                     label_lines=label_lines, show_axis='xy', y_label=y_label,
                     fit_b=fit_b)
    plt.title(title)

    # Save spectrum file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the spectrum figure
    if close:
        plt.close()
    else:
        plt.show(block=False)

    # Plot also residuals
    if fit_mod:

        # Calculate the residuals
        resid = 0

        if isinstance(fit_mod, fit_nifs.h_prof):
            resid = fit_nifs.resid_h(wvl, spectrum, fit_mod)
        elif isinstance(fit_mod, fit_nifs.k_prof):
            resid = fit_nifs.resid_k(wvl, spectrum, fit_mod)

        # Plot the spectrum with residuals
        fig = plt.figure(figsize=(10, 7))
        ax1 = fig.add_subplot(111)
        ax1.set_position([0.15, 0.4, 0.7, 0.5])
        plot_spectrum_in(ax1, wvl, spectrum, sky=sky,
                         fit_mod=fit_mod, zoom=zoom, i_range=i_range,
                         show_lines=show_lines, label_lines=label_lines,
                         show_axis='xy', y_label=y_label, fit_b=fit_b)
        plt.title(title)
        ax2 = fig.add_subplot(122)
        ax2.set_position([0.15, 0.08, 0.7, 0.2])

        if y_label:
            y_label_res = y_label.replace(" (", "\n(")
        else:
            y_label_res = None

        plot_spectrum_in(ax2, wvl, resid, sky=sky, fit_mod=fit_mod, res=True,
                         zoom=zoom, i_range=i_range_res, show_lines=show_lines,
                         label_lines=None, show_axis='y', y_label=y_label_res,
                         fit_b=fit_b)
        plt.title("Residuals", y=-0.2, transform=ax2.transAxes, fontsize=16)

        # Save spectrum with residuals file
        if save is not None:
            path_split = path.splitext(save)
            save2 = path_split[0] + '_res' + path_split[1]
            plt.savefig(save2)
            plt.savefig(path.splitext(save2)[0] + '.pdf')

        # Close the spectrum with residuals figure
        if close:
            plt.close()
        else:
            plt.show(block=False)


# noinspection PyTypeChecker
def plot_spectrum_in(ax, wvl, spectrum, sky=None, fit_mod=None, res=False,
                     show_lines='all', label_lines='top', show_axis='xy',
                     y_label=None, zoom=None, i_range=None, fit_b=True,
                     under_label="", ref_lambda=None):
    """Plot the inset figure of a spectrum. It can overplot a fitted model. It
    can gray-out wavelengths that have sky lines.

    Parameters:
        :param ax: Axes handle
        :type ax: matplotlib.axes._subplots.AxesSubplot
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param spectrum: 1-D Spectrum
        :type spectrum: numpy.ndarray [n] (float)
        :param sky: Telluric lines ranges (min, max) (um)
        :type sky: numpy.ndarray [:, 2] (float), optional
        :param fit_mod: Spectrum fitting model
        :type fit_mod: astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof, optional
        :param res: Plotting the residual spectrum
        :type res: bool, default False
        :param show_lines: Show lines centers and labels (None, 'all', 'single'
            for narrow H_a line or narrow red [OIII] only, 'no_broad' for all
            except broad H_a line)
        :type show_lines: str, default 'all'
        :param label_lines: Position of the lines labels (None, 'top',
            'bottom', 'top_bottom' for all top except narrow H_a line)
        :type label_lines: str, default 'top'
        :param show_axis: Show axis labels and ticks (None, 'y' or 'xy')
        :type show_axis: str, optional
        :param y_label: y axis label
        :type y_label: str, optional
        :param zoom: Wavelength zoom limits [min, max] (um)
        :type zoom: list [2] (float), optional
        :param i_range: Spectrum intensity limits [min, max]
        :type i_range: list [2] (float), optional
        :param fit_b: Broad H line was fitted
        :type fit_b: bool, default True
        :param under_label: Label to put under the plot
        :type under_label: str, default ""
        :param ref_lambda: Reference wavelength to plot (um)
        :type ref_lambda: float, optional
    """

    # Plot the spectrum
    ax.plot(wvl, spectrum, '-k')

    if res:
        plt.plot(wvl, ([0] * len(wvl)), 'r')

    # Zoom
    if not zoom:
        zoom = [np.min(wvl), np.max(wvl)]

    ax.set_xlim(zoom)

    if i_range is None:
        spectrum_zoom = spectrum[np.where((wvl >= zoom[0]) & (wvl <= zoom[1]))]
        spectrum_zoom_min = np.min(spectrum_zoom)
        spectrum_zoom_max = np.max(spectrum_zoom)
        spectrum_zoom_range = spectrum_zoom_max - spectrum_zoom_min
        i_range = [(spectrum_zoom_min - (spectrum_zoom_range / 10)),
                   (spectrum_zoom_max + (spectrum_zoom_range / 10))]

    ax.set_ylim(i_range)

    # Show fit
    if fit_mod:
        if not res:
            prof = None

            if isinstance(fit_mod, fit_nifs.h_prof):
                prof = fit_nifs.h_prof(bb_0=fit_mod.bb_0.value,
                                       o3rn_p=fit_mod.o3rn_p.value,
                                       o3rn_c=fit_mod.o3rn_c.value,
                                       o3rn_s=fit_mod.o3rn_s.value,
                                       o3bn_p=fit_mod.o3bn_p.value,
                                       o3bn_c=fit_mod.o3bn_c.value,
                                       o3bn_s=fit_mod.o3bn_s.value,
                                       b_p=fit_mod.b_p.value,
                                       b_c=fit_mod.b_c.value,
                                       b_s=fit_mod.b_s.value)
            elif isinstance(fit_mod, fit_nifs.k_prof):
                prof = fit_nifs.k_prof(bb_0=fit_mod.bb_0.value,
                                       bb_1=fit_mod.bb_1.value,
                                       hab_p=fit_mod.hab_p.value,
                                       hab_c=fit_mod.hab_c.value,
                                       hab_s=fit_mod.hab_s.value,
                                       han_p=fit_mod.han_p.value,
                                       han_c=fit_mod.han_c.value,
                                       han_s=fit_mod.han_s.value,
                                       n2r_p=fit_mod.n2r_p.value,
                                       n2r_c=fit_mod.n2r_c.value,
                                       n2r_s=fit_mod.n2r_s.value,
                                       n2b_p=fit_mod.n2b_p.value,
                                       n2b_c=fit_mod.n2b_c.value,
                                       n2b_s=fit_mod.n2b_s.value,
                                       s2b_p=fit_mod.s2b_p.value,
                                       s2b_c=fit_mod.s2b_c.value,
                                       s2b_s=fit_mod.s2b_s.value,
                                       s2r_p=fit_mod.s2r_p.value,
                                       s2r_c=fit_mod.s2r_c.value,
                                       s2r_s=fit_mod.s2r_s.value)

            plt.plot(wvl, prof(wvl), 'r')

        if show_lines:
            if (label_lines == 'top') or (label_lines == 'top_bottom'):
                offset_level = i_range[1]
                offset_sign = -1
                offset_label = 0.1

                if label_lines == 'top_bottom':
                    offset_level_han = i_range[0]
                    offset_sign_han = 1
                    offset_label_han = 0
                    offset_frac_han = 0.2
                else:
                    offset_level_han = i_range[1]
                    offset_sign_han = -1
                    offset_label_han = 0.1
                    offset_frac_han = 0.4
            else:
                offset_level = i_range[0]
                offset_sign = 1
                offset_label = 0
                offset_level_han = i_range[0]
                offset_sign_han = 1
                offset_label_han = 0
                offset_frac_han = 0.4

            if isinstance(fit_mod, fit_nifs.h_prof):
                plt.plot(([fit_mod.o3rn_c.value] * 2), plt.ylim(), ':r')
                text_y_other = offset_level + ((0.1 + offset_label) *
                                               offset_sign * (i_range[1] -
                                                              i_range[0]))

                if label_lines:
                    plt.text((fit_mod.o3rn_c.value - 0.001), text_y_other,
                             r"[OIII]$_{r}$", rotation=90,
                             horizontalalignment='right', fontsize=16)

                if (show_lines == 'all') or (show_lines == 'no_broad'):
                    plt.plot(([fit_mod.o3bn_c.value] * 2), plt.ylim(), ':r')

                    if fit_b:
                        plt.plot(([fit_mod.b_c.value] * 2), plt.ylim(), '--r')

                    if label_lines:
                        plt.text((fit_mod.o3bn_c.value - 0.001), text_y_other,
                                 r"[OIII]$_{b}$", rotation=90,
                                 horizontalalignment='right', fontsize=16)

                        if fit_b:
                            plt.text(fit_mod.b_c.value, text_y_other, "broad",
                                     rotation=90, horizontalalignment='left',
                                     fontsize=16)
            elif isinstance(fit_mod, fit_nifs.k_prof):
                plt.plot(([fit_mod.han_c.value] * 2), plt.ylim(), ':r')

                if label_lines:
                    text_y_hn = offset_level_han + ((offset_frac_han +
                                                     offset_label_han) *
                                                    offset_sign_han *
                                                    (i_range[1] - i_range[0]))
                    plt.text(fit_mod.han_c.value, text_y_hn, r"H$_{\alpha,n}$",
                             rotation=90, horizontalalignment='right',
                             fontsize=16)

                if (show_lines == 'all') or (show_lines == 'no_broad'):
                    plt.plot(([fit_mod.n2r_c.value] * 2), plt.ylim(), '--r')
                    plt.plot(([fit_mod.n2b_c.value] * 2), plt.ylim(), '--r')
                    plt.plot(([fit_mod.s2b_c.value] * 2), plt.ylim(), '-.r')
                    plt.plot(([fit_mod.s2r_c.value] * 2), plt.ylim(), '-.r')

                    if show_lines != 'no_broad':
                        plt.plot(([fit_mod.hab_c.value] * 2), plt.ylim(), ':r')

                    if label_lines:
                        text_y_hb = offset_level + ((0.25 + offset_label) *
                                                    offset_sign *
                                                    (i_range[1] - i_range[0]))
                        text_y_other = offset_level + ((0.05 + offset_label) *
                                                       offset_sign *
                                                       (i_range[1] -
                                                        i_range[0]))
                        plt.text((fit_mod.n2r_c.value - 0.0005), text_y_other,
                                 r"[NII]$_{r}$", rotation=90,
                                 horizontalalignment='right', fontsize=16)
                        plt.text((fit_mod.n2b_c.value - 0.0005), text_y_other,
                                 r"[NII]$_{b}$", rotation=90,
                                 horizontalalignment='right', fontsize=16)
                        plt.text((fit_mod.s2b_c.value - 0.0005), text_y_other,
                                 r"[SII]$_{b}$", rotation=90,
                                 horizontalalignment='right', fontsize=16)
                        plt.text((fit_mod.s2r_c.value + 0.0015), text_y_other,
                                 r"[SII]$_{r}$", rotation=90,
                                 horizontalalignment='left', fontsize=16)

                        if show_lines != 'no_broad':
                            plt.text(fit_mod.hab_c.value, text_y_hb,
                                     r"H$_{\alpha,b}$", rotation=90,
                                     horizontalalignment='right', fontsize=16)

        if ref_lambda:
            plt.plot(([ref_lambda] * 2), plt.ylim(), '--b')

    # Show axis labels and ticks
    if y_label:
        ylabel = y_label
    else:
        ylabel = "Flux density"

    if show_axis == 'xy':
        ax.set_xlabel(r"$\lambda$ ($\mu$m)", fontsize=16)
        ax.set_ylabel(ylabel, fontsize=16)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)
    elif show_axis == 'y':
        plt.tick_params(axis='x', bottom=False, labelbottom=False)
        ax.set_ylabel(ylabel, fontsize=16)
        plt.xticks(fontsize=14)
        plt.yticks(fontsize=14)
    else:
        plt.tick_params(axis='x', bottom=False, labelbottom=False)
        plt.tick_params(axis='y', left=False, labelleft=False)
        ax.ticklabel_format(style='plain')

    if under_label != "":
        ax.text(0.5, -0.1, under_label, size=14, ha="center",
                transform=ax.transAxes)

    # Show sky lines
    if sky is not None:
        for i_sky in range(len(sky)):
            ax.axvspan(sky[i_sky, 0], sky[i_sky, 1], alpha=0.5,
                       facecolor='grey')


def plot_rad(img, ps, center, title_str, center_2=None, model_type=None,
             model_type_2=None, model_data=None, model_data_2=None,
             zoom=None, y_label="Flux", fwhm=True, legend=True, close=True,
             save=None):
    """Plot the radial profile of an image. It can overplot a Gaussian or
    Moffat fit. It can also overplot a second profile.

    Parameters:
        :param img: Image
        :type img: numpy.ndarray [y, x] (float)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param center: Center [x, y] of the profile (px)
        :type center: list [2] (float)
        :param title_str: String to add to the title
        :type title_str: str, None
        :param center_2: Center [x, y] of the second profile (px)
        :type center_2: list [2] (float), optional
        :param model_type: Type of fit, 'gaussian' or 'moffat'
        :type model_type: str, optional
        :param model_type_2: Second type of fit, 'gaussian' or 'moffat'
        :type model_type_2: str, optional
        :param model_data: Parameters of the Gaussian [height, std, bkg,
            avergae FWHM] or Moffat [height, s, alpha, bkg, avergae FWHM] fit
            ([_, px, _, "], [_, px, _, _, "])
        :type model_data: list [4], [5] (float), optional
        :param model_data_2: Parameters of the Gaussian [height, std, bkg,
            avergae FWHM] or Moffat [height, s, alpha, bkg, avergae FWHM]
            second fit ([_, px, _, "], [_, px, _, _, "])
        :type model_data_2: list [4], [5] (float), optional
        :param zoom: Radius maximum limit (")
        :type zoom: float, optional
        :param y_label: y axis label
        :type y_label: str, default "Flux"
        :param fwhm: Show FWHM
        :type fwhm: bool, default True
        :param legend: Show legend
        :type legend: bool, default True
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Calculate profile
    dist_flat_2 = []
    map_flat_2 = []
    y_mesh_orig, x_mesh_orig = np.indices(img.shape)
    x_mesh = x_mesh_orig - center[0]
    y_mesh = y_mesh_orig - center[1]
    dist = np.hypot(x_mesh, y_mesh)
    dist_flat = dist.flatten() * ps
    map_flat_orig = img.flatten()

    if center_2:
        x_mesh_2 = x_mesh_orig - center_2[0]
        y_mesh_2 = y_mesh_orig - center_2[1]
        dist_2 = np.hypot(x_mesh_2, y_mesh_2)
        dist_flat_2 = dist_2.flatten() * ps

    # Zoom
    if zoom is not None:
        whr = np.where(dist_flat <= zoom)
        dist_flat = dist_flat[whr]
        map_flat = map_flat_orig[whr]
    else:
        map_flat = copy.deepcopy(map_flat_orig)

    if center_2:
        if zoom is not None:
            whr_2 = np.where(dist_flat_2 <= zoom)
            dist_flat_2 = dist_flat_2[whr_2]
            map_flat_2 = map_flat_orig[whr_2]
        else:
            map_flat_2 = copy.deepcopy(map_flat_orig)

    y_max = np.max(map_flat)
    y_min = np.min(map_flat)

    if center_2:
        y_max_2 = np.max(map_flat_2)
        y_min_2 = np.min(map_flat_2)
        y_max = np.max([y_max, y_max_2])
        y_min = np.min([y_min, y_min_2])

    # Plot the profile
    fig = plt.figure(figsize=(6, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.2, 0.15, 0.65, 0.7])
    ax.scatter(dist_flat, map_flat, c='b', s=1)

    if center_2:
        ax.scatter(dist_flat_2, map_flat_2, c='r', s=1)

    ax.set_xlabel("r (\")", fontsize=16)
    ax.set_ylabel(y_label, fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.yaxis.get_offset_text().set(size=14)

    if title_str:
        plt.title(("Radial profile of " + title_str), y=1.1)

    # Show fit
    if model_type is not None:
        r = np.arange(0, (np.max(dist_flat) / ps), 0.1)

        if model_type == 'gauss':
            prof = fit_nifs.gauss1d_prof(r, model_data[0], model_data[1],
                                         model_data[2])
            plt.plot((r * ps), prof, 'b-', linewidth=1, label="Gaussian fit")

            if fwhm:
                plt.plot(([model_data[3] / 2] * 2),
                         [model_data[2],
                          fit_nifs.gauss1d_prof((model_data[3] / (2 * ps)),
                                                model_data[0], model_data[1],
                                                model_data[2])], 'b--',
                         linewidth=1,
                         label="FWHM = {0:.3f}\"".format(model_data[3]))
        else:
            prof = fit_nifs.moffat1d_prof(r, model_data[0], model_data[1],
                                          model_data[2], model_data[3])
            plt.plot((r * ps), prof, 'b-', linewidth=1,
                     label="Moffat fit".format(model_data[4]))

            if fwhm:
                plt.plot(([model_data[4] / 2] * 2),
                         [model_data[3],
                          fit_nifs.moffat1d_prof((model_data[4] / (2 * ps)),
                                                 model_data[0], model_data[1],
                                                 model_data[2],
                                                 model_data[3])], 'b--',
                         linewidth=1,
                         label="FWHM = {0:.3f}\"".format(model_data[4]))

        prof_max = np.max(prof)
        prof_min = np.min(prof)
        y_max = np.max([y_max, prof_max])
        y_min = np.min([y_min, prof_min])

        if legend:
            ax.legend()

    if model_type_2 is not None:
        r_2 = np.arange(0, (np.max(dist_flat_2) / ps), 0.1)

        if model_type_2 == 'gauss':
            prof_2 = fit_nifs.gauss1d_prof(r_2, model_data_2[0],
                                           model_data_2[1], model_data_2[2])
            plt.plot((r_2 * ps), prof_2, 'r-', linewidth=1,
                     label="Gaussian fit (star)")

            if fwhm:
                plt.plot(([model_data_2[3] / 2] * 2),
                         [model_data_2[2],
                          fit_nifs.gauss1d_prof((model_data_2[3] / (2 * ps)),
                                                model_data_2[0],
                                                model_data_2[1],
                                                model_data_2[2])], 'r--',
                         linewidth=1,
                         label="FWHM (star) = {0:.3f}\"".format(
                             model_data_2[3]))
        else:
            prof_2 = fit_nifs.moffat1d_prof(r_2, model_data_2[0],
                                            model_data_2[1], model_data_2[2],
                                            model_data_2[3])
            plt.plot((r_2 * ps), prof_2, 'r-', linewidth=1,
                     label="Moffat fit (star)".format(model_data_2[4]))

            if fwhm:
                plt.plot(([model_data_2[4] / 2] * 2),
                         [model_data_2[3],
                          fit_nifs.moffat1d_prof((model_data_2[4] / (2 * ps)),
                                                 model_data_2[0],
                                                 model_data_2[1],
                                                 model_data_2[2],
                                                 model_data_2[3])], 'r--',
                         linewidth=1,
                         label="FWHM (star) = {0:.3f}\"".format(
                             model_data_2[4]))

        prof_max_2 = np.max(prof_2)
        prof_min_2 = np.min(prof_2)
        y_max = np.max([y_max, prof_max_2])
        y_min = np.min([y_min, prof_min_2])

        if legend:
            ax.legend()

    # Scale
    if zoom is not None:
        ax.set_xlim([0, zoom])

    y_range = y_max - y_min
    y_max += y_range / 10
    y_min -= y_range / 10
    ax.set_ylim([y_min, y_max])

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


def plot_rad_quad(img, ps, center, steps, title_str, method='mean',
                  y_label="Flux", offset=0.05, printing=False, close=True,
                  save=None):
    """Plot the average radial profile of the quadrants of an image. The
    standard error of each datapoint is used as errorbar.

    Parameters:
        :param img: Image
        :type img: numpy.ndarray [y, x] (float)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param center: Center [x, y] of the profile (px)
        :type center: list [2] (float)
        :param steps: Distance bins boundaries (")
        :type steps: numpy.ndarray [n] (float)
        :param title_str: String to add to the title
        :type title_str: str, None
        :param method: Averaging method ('mean' or 'median')
        :type method: str, default 'mean'
        :param y_label: y axis label
        :type y_label: str, default "Flux"
        :param offset: Fraction of the smallest step thet the points of the
            different quadrants are shifted along the x axis
        :type offset: float, default 0.05
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Calculate profile
    y_mesh, x_mesh = np.indices(img.shape)
    x_mesh = x_mesh - center[0]
    y_mesh = y_mesh - center[1]
    dist = np.hypot(x_mesh, y_mesh)
    x_mesh_flat = x_mesh.flatten()
    y_mesh_flat = y_mesh.flatten()
    dist_flat = dist.flatten() * ps
    map_flat = img.flatten()

    # Divide profile
    where_sw = np.where((x_mesh_flat < 0) & (y_mesh_flat < 0))
    where_se = np.where((x_mesh_flat >= 0) & (y_mesh_flat < 0))
    where_nw = np.where((x_mesh_flat < 0) & (y_mesh_flat >= 0))
    where_ne = np.where((x_mesh_flat >= 0) & (y_mesh_flat >= 0))
    dist_flat_sw = dist_flat[where_sw]
    dist_flat_se = dist_flat[where_se]
    dist_flat_nw = dist_flat[where_nw]
    dist_flat_ne = dist_flat[where_ne]
    map_flat_sw = map_flat[where_sw]
    map_flat_se = map_flat[where_se]
    map_flat_nw = map_flat[where_nw]
    map_flat_ne = map_flat[where_ne]

    # Calculate averages
    dist_avg = (steps[: -1] + steps[1:]) / 2
    method_str = ""
    map_avg_sw = np.zeros(len(dist_avg))
    map_avg_se = np.zeros(len(dist_avg))
    map_avg_nw = np.zeros(len(dist_avg))
    map_avg_ne = np.zeros(len(dist_avg))
    map_err_sw = np.zeros(len(dist_avg))
    map_err_se = np.zeros(len(dist_avg))
    map_err_nw = np.zeros(len(dist_avg))
    map_err_ne = np.zeros(len(dist_avg))

    for i_rad in range(len(dist_avg)):
        where_avg_sw = np.where((dist_flat_sw >= steps[i_rad]) &
                                (dist_flat_sw < steps[i_rad + 1]))[0]
        where_avg_se = np.where((dist_flat_se >= steps[i_rad]) &
                                (dist_flat_se < steps[i_rad + 1]))[0]
        where_avg_nw = np.where((dist_flat_nw >= steps[i_rad]) &
                                (dist_flat_nw < steps[i_rad + 1]))[0]
        where_avg_ne = np.where((dist_flat_ne >= steps[i_rad]) &
                                (dist_flat_ne < steps[i_rad + 1]))[0]

        if method == 'mean':
            method_str = "mean"
            map_avg_sw[i_rad] = np.mean(map_flat_sw[where_avg_sw])
            map_avg_se[i_rad] = np.mean(map_flat_se[where_avg_se])
            map_avg_nw[i_rad] = np.mean(map_flat_nw[where_avg_nw])
            map_avg_ne[i_rad] = np.mean(map_flat_ne[where_avg_ne])
        else:
            method_str = "median"
            map_avg_sw[i_rad] = np.median(map_flat_sw[where_avg_sw])
            map_avg_se[i_rad] = np.median(map_flat_se[where_avg_se])
            map_avg_nw[i_rad] = np.median(map_flat_nw[where_avg_nw])
            map_avg_ne[i_rad] = np.median(map_flat_ne[where_avg_ne])

        map_err_sw[i_rad] = np.std(map_flat_sw[where_avg_sw]) / \
            np.sqrt(len(where_avg_sw))
        map_err_se[i_rad] = np.std(map_flat_se[where_avg_se]) / \
            np.sqrt(len(where_avg_se))
        map_err_nw[i_rad] = np.std(map_flat_nw[where_avg_nw]) / \
            np.sqrt(len(where_avg_nw))
        map_err_ne[i_rad] = np.std(map_flat_ne[where_avg_ne]) / \
            np.sqrt(len(where_avg_ne))

    # Plot the profile
    offset_r = offset * (dist_avg[1] - dist_avg[0])
    fig = plt.figure(figsize=(6, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.errorbar(dist_avg, map_avg_ne, yerr=map_err_ne, fmt='o-', c='b',
                label="NE")
    ax.errorbar((dist_avg + offset_r), map_avg_nw, yerr=map_err_nw, fmt='s--',
                c='g', label="NW")
    ax.errorbar((dist_avg + (2 * offset_r)), map_avg_se, yerr=map_err_se,
                fmt='^:', c='r', label="SE")
    ax.errorbar((dist_avg + (3 * offset_r)), map_avg_sw, yerr=map_err_sw,
                fmt='v-.', c='m', label="SW")
    ax.set_xlabel("r (\")", fontsize=16)
    ax.set_ylabel(y_label, fontsize=16)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.yaxis.get_offset_text().set(size=14)

    if title_str and not printing:
        plt.title(("Radial profile of " + method_str + " " + title_str), y=1.1)

    ax.legend(loc='upper right')

    # Zoom
    ylim = list(ax.get_ylim())

    if ylim[0] > 0:
        ylim[0] = 0

    ax.set_xlim([steps[0], steps[-1]])
    ax.set_ylim(ylim)

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


# noinspection PyTypeChecker
def plot_rad_map(maps, ps, center, param, model_type=None, model_data=None,
                 zoom=None, y_label="Flux", fwhm=True, legend=True, close=True,
                 save=None):
    """Plot the radial profile of a map. It can overplot a Gaussian or
    Moffat fit.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param center: Center [x, y] of the profile (px)
        :type center: list [2] (float)
        :param param: Fitted parameter to map
        :type param: str
        :param model_type: Type of fit, 'gaussian' or 'moffat'
        :type model_type: str, optional
        :param model_data: Parameters of the Gaussian [height, std, bkg,
            avergae FWHM] or Moffat [height, s, alpha, bkg, avergae FWHM] fit
            ([_, px, _, "], [_, px, _, _, "])
        :type model_data: list [4], [5] (float), optional
        :param zoom: Radius maximum limit (")
        :type zoom: float, optional
        :param y_label: y axis label
        :type y_label: str, default "Flux"
        :param fwhm: Show FWHM
        :type fwhm: bool, default True
        :param legend: Show legend
        :type legend: bool, default True
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select map
    map1 = None
    title_str = None

    if isinstance(maps[0, 0], fit_nifs.h_prof):
        map1, title_str = grab_map_h(maps, param)
    elif isinstance(maps[0, 0], fit_nifs.k_prof):
        map1, title_str = grab_map_k(maps, param)

    if (param[-2:] == '_i') or (param[-2:] == '_p') or (param[-2:] == '_f') \
            or (param == 'cont') or (param == 'bb_0'):
        map1 /= ps ** 2

    # Plot the profile
    plot_rad(map1, ps, center, title_str, model_type=model_type,
             model_data=model_data, zoom=zoom, y_label=y_label, fwhm=fwhm,
             legend=legend, close=close, save=save)


# noinspection PyTypeChecker
def plot_rad_quad_map(maps, ps, center, param, steps, method='mean',
                      y_label="Flux", offset=0.05, printing=False, close=True,
                      save=None):
    """Plot the average radial profiles of the quadrants of a map.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param center: Center [x, y] of the profile (px)
        :type center: list [2] (float)
        :param param: Fitted parameter to map
        :type param: str
        :param steps: Distance bins boundaries (")
        :type steps: numpy.ndarray [n] (float)
        :param method: Averaging method ('mean' or 'median')
        :type method: str, default 'mean'
        :param y_label: y axis label
        :type y_label: str, default "Flux"
        :param offset: Fraction of the smallest step thet the points of the
            different quadrants are shifted along the x axis
        :type offset: float, default 0.05
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select map
    map1 = None
    title_str = None

    if isinstance(maps[0, 0], fit_nifs.h_prof):
        map1, title_str = grab_map_h(maps, param)
    elif isinstance(maps[0, 0], fit_nifs.k_prof):
        map1, title_str = grab_map_k(maps, param)

    if (param[-2:] == '_i') or (param[-2:] == '_p') or (param[-2:] == '_f') \
            or (param == 'cont') or (param == 'bb_0'):
        map1 /= ps ** 2

    # Plot the profile
    plot_rad_quad(map1, ps, center, steps, title_str, method=method,
                  y_label=y_label, offset=offset, printing=printing,
                  close=close, save=save)


# noinspection PyTypeChecker
def plot_az(maps, ps, center, param, n_slices=10, dist_range=(0, 1e10),
            errorbars=True, rlabel_pos=180, range_flux=None,
            y_label="Average flux", printing=False, close=True, save=None):
    """Plot the azimuthal profile of an image, using the average values in
    slices. The image is from a cube of maps.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param center: Center [x, y] of the profile (px)
        :type center: list [2] (float)
        :param param: Fitted parameter to map
        :type param: str
        :param n_slices: Number of slices
        :type n_slices: int, default 10
        :param dist_range: Range of distances from the center used for the
            average ([min, max]) (")
        :type dist_range: list [2] (int), default (0, 1e10)
        :param errorbars: Plot errorbars
        :type errorbars: bool, default True
        :param rlabel_pos: Angle of the radial axis labels (deg)
        :type rlabel_pos: float, default 180
        :param range_flux: Flux range
        :type range_flux: float, optional
        :param y_label: y axis label
        :type y_label: str, default "Flux"
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select map
    map1 = None
    title_str = ""

    if isinstance(maps[0, 0], fit_nifs.h_prof):
        map1, title_str = grab_map_h(maps, param)
    elif isinstance(maps[0, 0], fit_nifs.k_prof):
        map1, title_str = grab_map_k(maps, param)

    plot_az_map(map1, ps, center, title_str, n_slices=n_slices,
                dist_range=dist_range, errorbars=errorbars,
                rlabel_pos=rlabel_pos, range_flux=range_flux, y_label=y_label,
                printing=printing, close=close, save=save)


def plot_az_map(map1, ps, center, title_str, n_slices=10, dist_range=(0, 1e10),
                errorbars=True, rlabel_pos=180, range_flux=None,
                y_label="Average flux", printing=False, close=True, save=None):
    """Plot the azimuthal profile of an image, using the average values in
    slices.

    Parameters:
        :param map1: Map
        :type map1: numpy.ndarray [y, x] (float)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param center: Center [x, y] of the profile (px)
        :type center: list [2] (float)
        :param title_str: Part of the title
        :type title_str: str
        :param n_slices: Number of slices
        :type n_slices: int, default 10
        :param dist_range: Range of distances from the center used for the
            average ([min, max]) (")
        :type dist_range: list [2] (int), default (0, 1e10)
        :param errorbars: Plot errorbars
        :type errorbars: bool, default True
        :param rlabel_pos: Angle of the radial axis labels (deg)
        :type rlabel_pos: float, default 180
        :param range_flux: Flux range
        :type range_flux: float, optional
        :param y_label: y axis label
        :type y_label: str, default "Flux"
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Calculate profile
    map1 /= ps ** 2
    y_mesh, x_mesh = np.indices(map1.shape)
    x_mesh = x_mesh - center[0]
    y_mesh = y_mesh - center[1]
    complex_mesh = x_mesh + (1j * y_mesh)
    theta = np.angle(complex_mesh)
    dist = np.hypot(x_mesh, y_mesh) * ps
    slices = np.linspace(-np.pi, np.pi, num=(n_slices + 1))
    theta_slice = np.zeros(n_slices)
    centroid_slice = np.zeros(n_slices)
    centroid_err_slice = np.zeros(n_slices)

    for i_slice in range(n_slices):
        where_slice = np.where((theta >= slices[i_slice]) &
                               (theta < slices[i_slice + 1]) &
                               (dist >= dist_range[0]) &
                               (dist <= dist_range[1]))

        if where_slice[0].size != 0:
            theta_slice[i_slice] = (np.pi / 2) - \
                np.mean(slices[i_slice: (i_slice + 2)])
            centroid_slice[i_slice] = np.mean(map1[where_slice])
            centroid_err_slice[i_slice] = np.std(map1[where_slice]) / \
                np.sqrt(len(map1[where_slice]))
        else:
            theta_slice[i_slice] = np.nan
            centroid_slice[i_slice] = np.nan
            centroid_err_slice[i_slice] = np.nan

    nan_where = np.where(np.isnan(centroid_slice))[0]
    theta_slice = np.delete(theta_slice, nan_where)
    centroid_slice = np.delete(centroid_slice, nan_where)
    centroid_err_slice = np.delete(centroid_err_slice, nan_where)

    # Plot the profile
    fig = plt.figure(figsize=(6, 5))
    ax = fig.add_subplot(111, polar=True)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.plot(theta_slice, centroid_slice, 'o', c='k', markersize=3)
    ax.plot(np.linspace(0, (2 * np.pi), 1000), np.zeros(1000), color='k',
            linestyle=':')

    if errorbars:
        ax.errorbar(theta_slice, centroid_slice, yerr=centroid_err_slice,
                    c='k', fmt='none')

    ax.set_theta_zero_location('N')

    if range_flux:
        ax.set_rmin(range_flux[0])
        ax.set_rmax(range_flux[1])

    ax.set_rlabel_position(rlabel_pos)
    r_max = ax.get_rmax()
    xpos_label = 0.7 * r_max
    ypos_label = 1.7 * r_max
    r_label = np.hypot(xpos_label, ypos_label)
    theta_label = np.arctan(xpos_label / ypos_label) + (np.pi / 2)
    ax.text(theta_label, r_label, y_label, size=10, rotation=90,
            horizontalalignment='center', verticalalignment='center')

    if not printing:
        plt.title(("Azimuthal profile of " + title_str), pad=15)

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


def print_fit(profiles_h, profiles_k, exclude_h=(), exclude_k=(), save=None):
    """Print the fitted profiles of maps in H and K band.

    Parameters:
        :param profiles_h: H band profiles
        :type profiles_h: dict
        :param profiles_k: K band profiles
        :type profiles_k: dict
        :param exclude_h: List of profile indeces to exclude for H band
        :type exclude_h: tuple, list [2] (int), default ()
        :param exclude_k: List of profile indeces to exclude for K band
        :type exclude_k: tuple, list [2] (int), default ()
        :param save: Path and filename root of the saved text
        :type save: str, optional
    """

    # Select profiles
    for i_band in ['h', 'k']:
        if i_band == 'h':
            profiles = profiles_h
        else:
            profiles = profiles_k

        sps = profiles['sps']

        for i_prof in range(len(profiles['tag'])):
            if ((i_band == 'h') and (i_prof not in exclude_h)) or \
                    ((i_band == 'k') and (i_prof not in exclude_k)):

                # Calculate
                s = np.sqrt(profiles['profile'][i_prof][2] *
                            profiles['profile'][i_prof][3])
                alpha = profiles['profile'][i_prof][5]
                fwhm = fit_nifs.moffat_fwhm(s, alpha) * sps
                ss = profiles['profile'][i_prof][2: 4]
                s_max = np.max(ss)
                s_min = np.min(ss)
                ell = (s_max - s_min) / s_max
                pa = profiles['profile'][i_prof][4] * 180 / np.pi

                # Print
                line0 = "Profile of {0} ('{1}'), {2} grating".format(
                    profiles['name'][i_prof], profiles['tag'][i_prof],
                    profiles['band'])
                line1 = "Geometric average FWHM: {0:.2f}\"".format(fwhm)
                line2 = "Ellipticity: {0:.2f}".format(ell)
                line3 = "Alpha: {0:.2f}".format(alpha)
                line4 = "Position angle: {0:d} deg".format(int(pa))
                print(line0)
                print(line1)
                print(line2)
                print(line3)
                print(line4)
                print()

                # Save
                file_txt = open((save + '_' + profiles['band'].lower() + '_' +
                                 profiles['tag'][i_prof] + '_' + '.txt'), 'w')
                file_txt.write(line0 + "\n")
                file_txt.write(line1 + "\n")
                file_txt.write(line2 + "\n")
                file_txt.write(line3 + "\n")
                file_txt.write(line4 + "\n")
                file_txt.close()


def plot_fwhm(profiles_h, profiles_k, exclude_h=(), exclude_k=(),
              printing=False, close=True, save=None):
    """Plot the FWHM of maps in H and K band.

    Parameters:
        :param profiles_h: H band profiles
        :type profiles_h: dict
        :param profiles_k: K band profiles
        :type profiles_k: dict
        :param exclude_h: List of profile indeces to exclude for H band
        :type exclude_h: tuple, list [2] (int), default ()
        :param exclude_k: List of profile indeces to exclude for K band
        :type exclude_k: tuple, list [2] (int), default ()
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select profiles
    fwhms_h = []
    fwhms_k = []
    labels_h = []
    labels_k = []
    n = 0

    for i_band in ['h', 'k']:
        if i_band == 'h':
            profiles = profiles_h
            fwhms = fwhms_h
            labels = labels_h
            extra_str = ""
        else:
            profiles = profiles_k
            fwhms = fwhms_k
            labels = labels_k
            extra_str = "\u200b"

        sps = profiles['sps']

        for i_prof in range(len(profiles['tag'])):
            if ((i_band == 'h') and (i_prof not in exclude_h)) or \
                    ((i_band == 'k') and (i_prof not in exclude_k)):
                s = np.sqrt(profiles['profile'][i_prof][2] *
                            profiles['profile'][i_prof][3])
                alpha = profiles['profile'][i_prof][5]
                fwhm = fit_nifs.moffat_fwhm(s, alpha)
                fwhms.append(fwhm * sps)
                labels.append(profiles['name'][i_prof] + extra_str)
                n += 1

    # Plot the profiles
    fig = plt.figure(figsize=((n * 1.5), 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.2, 0.7, 0.65])
    ax.bar(labels_h, fwhms_h, color='b', hatch='/')
    ax.bar(labels_k, fwhms_k, color='r', hatch='\\')
    leg = ax.legend(["H", "K"], ncol=2, loc='upper center',
                    handlelength=4, frameon=False, fontsize=16)
    ax.set_ylim([0, (np.max(fwhms_h + fwhms_k) * 1.4)])
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    for patch in leg.get_patches():
        patch.set_height(15)
        patch.set_y(-4)

    ax.set_ylabel("FWHM (\")", fontsize=16)

    if not printing:
        plt.title("FWHMa of maps", fontsize=16)

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


def plot_alpha(profiles_h, profiles_k, exclude_h=(), exclude_k=(),
               printing=False, close=True, save=None):
    """Plot the alpha of maps in H and K band.

    Parameters:
        :param profiles_h: H band profiles
        :type profiles_h: dict
        :param profiles_k: K band profiles
        :type profiles_k: dict
        :param exclude_h: List of profile indeces to exclude for H band
        :type exclude_h: tuple, list [2] (int), default ()
        :param exclude_k: List of profile indeces to exclude for K band
        :type exclude_k: tuple, list [2] (int), default ()
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select profiles
    alphas_h = []
    alphas_k = []
    labels_h = []
    labels_k = []
    n = 0

    for i_band in ['h', 'k']:
        if i_band == 'h':
            profiles = profiles_h
            alphas = alphas_h
            labels = labels_h
            extra_str = ""
        else:
            profiles = profiles_k
            alphas = alphas_k
            labels = labels_k
            extra_str = "\u200b"

        for i_prof in range(len(profiles['tag'])):
            if ((i_band == 'h') and (i_prof not in exclude_h)) or \
                    ((i_band == 'k') and (i_prof not in exclude_k)):
                alpha = profiles['profile'][i_prof][5]
                alphas.append(alpha)
                labels.append(profiles['name'][i_prof] + extra_str)
                n += 1

    # Plot the profiles
    fig = plt.figure(figsize=((n * 1.5), 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.2, 0.7, 0.65])
    ax.bar(labels_h, alphas_h, color='b', hatch='/')
    ax.bar(labels_k, alphas_k, color='r', hatch='\\')
    leg = ax.legend(["H", "K"], ncol=2, loc='upper center',
                    handlelength=4, frameon=False, fontsize=16)
    ax.set_ylim([0, (np.max(alphas_h + alphas_k) * 1.4)])
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    for patch in leg.get_patches():
        patch.set_height(15)
        patch.set_y(-4)

    ax.set_ylabel(r"$\alpha$", fontsize=16)

    if not printing:
        plt.title("Moffat exponentials of maps", fontsize=16)

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


def plot_pos(profiles_h, profiles_k, exclude_h=(), exclude_k=(),
             reverse_band=True, printing=False, close=True, save=None):
    """Plot the center position of maps in H and K band.

    Parameters:
        :param profiles_h: H band profiles
        :type profiles_h: dict
        :param profiles_k: K band profiles
        :type profiles_k: dict
        :param exclude_h: List of profile indeces to exclude for H band
        :type exclude_h: tuple, list [2] (list), default ()
        :param exclude_k: List of profile indeces to exclude for K band
        :type exclude_k: tuple, list [2] (list), default ()
        :param reverse_band: List first the K and then the H band
        :type reverse_band: bool, default True
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select profiles
    col_h = 'b'
    col_k = 'r'
    xs_h = []
    ys_h = []
    xs_k = []
    ys_k = []
    labels_h = []
    labels_k = []
    symbs_h = []
    symbs_k = []
    symbs_list = itertools.cycle(('o', 's', '^', 'v', 'd', '*'))

    if reverse_band:
        bands = ['k', 'h']
    else:
        bands = ['h', 'k']

    for i_band in bands:
        if i_band == 'h':
            profiles = profiles_h
            xs = xs_h
            ys = ys_h
            labels = labels_h
            symbs = symbs_h
            extra_str = ""
        else:
            profiles = profiles_k
            xs = xs_k
            ys = ys_k
            labels = labels_k
            symbs = symbs_k
            extra_str = "\u200b"

        sps = profiles['sps']
        ref_idx = \
            [i for i, s in enumerate(profiles['name']) if "Continuum" in s][0]
        x_ref = profiles['profile'][ref_idx, 0]
        y_ref = profiles['profile'][ref_idx, 1]

        for i_prof in range(len(profiles['tag'])):
            if ((i_band == 'h') and (i_prof not in exclude_h)) or \
                    ((i_band == 'k') and (i_prof not in exclude_k)):
                if profiles['name'][i_prof] != "Continuum":
                    x = (profiles['profile'][i_prof, 0] - x_ref) * sps
                    y = (profiles['profile'][i_prof, 1] - y_ref) * sps
                    xs.append(x)
                    ys.append(y)
                    labels.append(profiles['name'][i_prof] + extra_str)
                    symbs.append(next(symbs_list))

                    if printing:
                        dist_as = np.hypot(x, y)
                        dist_px = dist_as / sps
                        line_print = ("Distance of '{0}': {1:.1f} mas, " +
                                      "{2:.2f} spx").format(
                            profiles['tag'][i_prof], (dist_as * 1000), dist_px)
                        print(line_print)

    # Plot the positions
    if reverse_band:
        xs_first = xs_k
        ys_first = ys_k
        labels_first = labels_k
        col_first = col_k
        symbs_first = symbs_k
        xs_second = xs_h
        ys_second = ys_h
        labels_second = labels_h
        col_second = col_h
        symbs_second = symbs_h
    else:
        xs_first = xs_h
        ys_first = ys_h
        labels_first = labels_h
        col_first = col_h
        symbs_first = symbs_h
        xs_second = xs_k
        ys_second = ys_k
        labels_second = labels_k
        col_second = col_k
        symbs_second = symbs_k

    fig = plt.figure(figsize=(7, 4.5))
    ax = fig.add_subplot(111)
    ax.set_position([0.05, 0.15, 0.7, 0.8])
    plt.plot(0, 0, color='k', marker='x', label='Continuum', linestyle='',
             markersize=10)

    for i_dot in range(len(labels_first)):
        plt.plot(xs_first[i_dot], ys_first[i_dot], color=col_first,
                 marker=symbs_first[i_dot], label=labels_first[i_dot],
                 linestyle='', markersize=10)

    for i_dot in range(len(labels_second)):
        plt.plot(xs_second[i_dot], ys_second[i_dot], color=col_second,
                 marker=symbs_second[i_dot], label=labels_second[i_dot],
                 linestyle='', markersize=10)

    xs = xs_h + xs_k + [0]
    ys = ys_h + ys_k + [0]
    xs_min = np.min(xs)
    xs_max = np.max(xs)
    xs_range = xs_max - xs_min
    xs_mean = np.mean([xs_min, xs_max])
    ys_min = np.min(ys)
    ys_max = np.max(ys)
    ys_range = ys_max - ys_min
    ys_mean = np.mean([ys_min, ys_max])
    xy_range = np.max([xs_range, ys_range])
    xy_halfrange = 1.2 * xy_range / 2
    xs_min_ax = xs_mean - xy_halfrange
    xs_max_ax = xs_mean + xy_halfrange
    ys_min_ax = ys_mean - xy_halfrange
    ys_max_ax = ys_mean + xy_halfrange
    xs_range = [xs_min_ax, xs_max_ax]
    ys_range = [ys_min_ax, ys_max_ax]
    ax.set_xlim(xs_range)
    ax.set_ylim(ys_range)
    ax.invert_xaxis()
    ax.set_aspect('equal')
    ax.set_xlabel(r"$\Delta$x" + " (\")", fontsize=16)
    ax.set_ylabel(r"$\Delta$y" + " (\")", fontsize=16)
    tick_loc = ticker.MultipleLocator(base=params_nifs.sps)
    ax.xaxis.set_major_locator(tick_loc)
    ax.yaxis.set_major_locator(tick_loc)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    ax.grid(linestyle='--')
    ax.legend(fontsize=14, loc='upper left', bbox_to_anchor=(1.05, 1.03))

    if not printing:
        plt.title("Centers of maps", fontsize=16)

    ax.annotate('E', xy=(1.158, -0.143), xytext=(0.9, -0.143), ha='right',
                va='center', xycoords='axes fraction',
                arrowprops=dict(arrowstyle='<-'), fontsize=16)
    ax.annotate('N', xy=(1.15, -0.15), xytext=(1.15, 0.1), ha='center',
                xycoords='axes fraction', arrowprops=dict(arrowstyle='<-'),
                fontsize=16)

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


# noinspection PyTypeChecker
def plot_map(maps, ps, ticks, param, snr=False, mask_map=None, mask_thr=0,
             mask_lim='low', vel=False, beam_circ=None, model=None, imin=None,
             imax=None, ignore_bottom=False, perc=0, printing=False,
             calib=True, close=True, save=None):
    """Plot the map of a fitted parameter. It can plot instead the SNR map of a
    peak, defined as the absolute of the ratio of the peak and its error.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float
        :param param: Fitted parameter to map
        :type param: str
        :param snr: Plot the map ot the SNR
        :type snr: bool, default False
        :param mask_map: Fitted parameter to use as mask
        :type mask_map: str, optional
        :param mask_thr: Threshold value for masking
        :type mask_thr: float, default 0
        :param mask_lim: Limit type for masking ('up' for upper, 'low ' for
            lower)
        :type mask_lim: str, default 'low'
        :param vel: Transform the map to velocities
        :type vel: bool, default False
        :param beam_circ: Circles [x, y, radius] of beam (px)
        :type beam_circ: numpy.ndarray [n, 3] (float), optional
        :param model: Parameters of the FWHM ellipse [x0, y0, first FWHM,
            second FWHM, angle, average FWHM] ([px, px, px, px, deg, "])
        :type model: list [6] (float), optional
        :param imin: Minimum intensity value shown
        :type imin: float, optional
        :param imax: Maximum intensity value shown
        :type imax: float, optional
        :param ignore_bottom: Ignore the bottom of the FOV to choose
            automatically the maximum color value
        :type ignore_bottom: bool, default False
        :param perc: Percentile to remove from the top and bottom of the range
            of values colored (0 for none). Not used if imin or imax are used
        :type perc: float, default 0
        :param printing: Printed version
        :type printing: bool, default False
        :param calib: The data flux is calibrated
        :type calib: bool, default True
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select map
    if snr:
        map1, title_str = grab_map_snr(maps, param)
        c_label = "SNR"
    else:
        map1 = None
        title_str = ""

        if isinstance(maps[0, 0], fit_nifs.h_prof):
            map1, title_str = grab_map_h(maps, param)
        elif isinstance(maps[0, 0], fit_nifs.k_prof):
            map1, title_str = grab_map_k(maps, param)

        lambd_ref = 0
        c_label = ""

        if vel:
            if param == 'hab_c':
                lambd_ref = params_nifs.h_alpha
            elif param == 'han_c':
                lambd_ref = params_nifs.h_alpha
            elif param == 'n2r_c':
                lambd_ref = params_nifs.n2r
            elif param == 'n2b_c':
                lambd_ref = params_nifs.n2b
            elif param == 's2b_c':
                lambd_ref = params_nifs.s2b
            elif param == 's2r_c':
                lambd_ref = params_nifs.s2r

            map1 = params_nifs.c * (((map1 / lambd_ref) ** 2) - 1) / \
                (((map1 / lambd_ref) ** 2) + 1)
            map1 -= np.median(map1)
            map1 /= 1e3
            title_str = title_str.replace("center", "LOS velocity")
            c_label = "Velocity (km/s)"

        if (param[-2:] == '_p') or (param[-2:] == '_f') or (param == 'cont') \
                or (param == 'bb_0'):

            if calib:
                c_label = "Flux density\n(" + params_nifs.flux_dens_spat_unit \
                    + ")"
            else:
                c_label = "Flux density\n(" +\
                    params_nifs.flux_dens_spat_unit_unc + ")"
        elif param[-2:] == '_i':

            if calib:
                c_label = "Flux density\n(" + params_nifs.flux_spat_unit + ")"
            else:
                c_label = "Flux density\n(" + params_nifs.flux_spat_unit_unc \
                    + ")"

        if (param[-2:] == '_i') or (param[-2:] == '_p') or \
                (param[-2:] == '_f') or (param == 'cont') or (param == 'bb_0'):
            map1 /= ps ** 2

    # Mask map
    if mask_map:
        mapm = None

        if isinstance(maps[0, 0], fit_nifs.h_prof):
            mapm, _ = grab_map_h(maps, mask_map)
        elif isinstance(maps[0, 0], fit_nifs.k_prof):
            mapm, _ = grab_map_k(maps, mask_map)

        if mask_lim == 'low':
            mapm_bool = mapm < mask_thr
        else:
            mapm_bool = mapm > mask_thr

        map2 = np.ma.masked_where(mapm_bool, map1)
    else:
        map2 = np.ma.masked_array(map1)

    # Show map
    if printing:
        compass = True
        title = ""
    else:
        compass = False
        title = "Map of " + title_str

    plot_img(map2, ps, ticks, beam_circ=beam_circ, model=model, imin=imin,
             imax=imax, c_label=c_label, title=title,
             ignore_bottom=ignore_bottom, perc=perc, compass=compass,
             close=close, save=save)

    if vel:
        plot_img(map1, ps, ticks, beam_circ=beam_circ, model=model, imin=imin,
                 imax=imax, c_label=c_label, title=title,
                 ignore_bottom=ignore_bottom, perc=perc, compass=compass,
                 close=close, save=save)


def plot_map_sub(map1, title_str, c_label, ps, ticks, beam_circ=None,
                 model=None, imin=None, imax=None, ignore_bottom=False,
                 printing=False, close=True, save=None):
    """Plot the map of a fitted parameter, after subtracting a 2D Moffat model,
    fitting only the peak and background values.

    Parameters:
        Returns:
        :param map1: Map of a fitting parameter
        :type map1: numpy.ndarray [y, x] (float)
        :param title_str: Map title
        :type title_str: str
        :param c_label: Color label title
        :type c_label: str
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float
        :param beam_circ: Circles [x, y, radius] of beam (px)
        :type beam_circ: numpy.ndarray [n, 3] (float), optional
        :param model: Parameters of the FWHM ellipse [x0, y0, first FWHM,
            second FWHM, angle, average FWHM] ([px, px, px, px, deg, "])
        :type model: list [6] (float), optional
        :param imin: Minimum intensity value shown
        :type imin: float, optional
        :param imax: Maximum intensity value shown
        :type imax: float, optional
        :param ignore_bottom: Ignore the bottom of the FOV to choose
            automatically the maximum color value
        :type ignore_bottom: bool, default False
        :param printing: Printed version
        :type printing: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Show map
    if printing:
        compass = True
        title = ""
    else:
        compass = False
        title = "Map of " + title_str + ", subtracted"

    plot_img(map1, ps, ticks, beam_circ=beam_circ, model=model, imin=imin,
             imax=imax, c_label=c_label, title=title,
             ignore_bottom=ignore_bottom, compass=compass, printing=printing,
             close=close, save=save)


# noinspection PyTypeChecker
def plot_ratio_map(maps, ps, ticks, param1, param2, model=None, imin=None,
                   imax=None, close=True, save=None):
    """Plot the map of the ratio of two fitted parameters.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float
        :param param1: Fitted parameter in the numerator of the ratio to map
        :type param1: str
        :param param2: Fitted parameter in the denumerator of the ratio to map
        :type param2: str
        :param model: Parameters of the FWHM ellipse [x0, y0, first FWHM,
            second FWHM, angle, average FWHM] ([px, px, px, px, deg, "])
        :type model: list [6] (float), optional
        :param imin: Minimum intensity value shown
        :type imin: float, optional
        :param imax: Maximum intensity value shown
        :type imax: float, optional
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select maps
    map1 = None
    map2 = None
    title_str1 = ""
    title_str2 = ""

    if isinstance(maps[0, 0], fit_nifs.h_prof):
        map1, title_str1 = grab_map_h(maps, param1)
        map2, title_str2 = grab_map_h(maps, param2)
    elif isinstance(maps[0, 0], fit_nifs.k_prof):
        map1, title_str1 = grab_map_k(maps, param1)
        map2, title_str2 = grab_map_k(maps, param2)

    warnings.simplefilter('ignore')
    map12 = map1 / map2
    warnings.simplefilter('default')
    map12[np.where(map12 == np.inf)] = np.nan
    map12 = np.ma.masked_where(np.isnan(map12), map12)

    # Show map
    plot_img(map12, ps, ticks, model=model, imin=imin, imax=imax,
             title=("Map of the ratio of\n" + title_str1 + " and " +
                    title_str2), close=close, save=save)


# noinspection PyTypeChecker
def convolve_maps(maps, rad):
    """Convolve maps using a circular top-hat kernel of a given diameter.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param rad: Radius of the circular top-hat kernel (px)
        :type rad: float

    Returns:
        :return maps_conv: Convolved maps of fitted parameters
        :rtype maps_conv: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
    """

    # Prepare kernel
    print("Convolving maps ...")
    side = int(np.floor(rad) * 2) + 1
    center = (side - 1) / 2
    kern = np.zeros([side, side])
    kern_x, kern_y = np.meshgrid(np.arange(side), np.arange(side))
    kern_dist = np.hypot((kern_x - center), (kern_y - center))
    kern[np.where(kern_dist <= rad)] = 1
    kern /= np.sum(kern)

    # Convolve maps
    parameters = maps[0, 0].param_names
    maps_conv = copy.deepcopy(maps)

    for i_param in range(len(parameters)):
        map1 = None

        if isinstance(maps[0, 0], fit_nifs.h_prof):
            map1, _ = grab_map_h(maps, parameters[i_param])
        elif isinstance(maps[0, 0], fit_nifs.k_prof):
            map1, _ = grab_map_k(maps, parameters[i_param])

        map1_conv = ndimage.convolve(map1, kern, mode='mirror')
        assign_map(map1_conv, maps_conv, i_param)

    return maps_conv


def convolve_map(map1, rad):
    """Convolve a map using a circular top-hat kernel of a given diameter.

    Parameters:
        :param map1: Map of a fitted parameter
        :type map1: numpy.ndarray [y, x] (float)
        :param rad: Radius of the circular top-hat kernel (px)
        :type rad: float

    Returns:
        :return map_conv: Convolved map of fitted parameters
        :rtype map_conv: numpy.ndarray [y, x] (float)
    """

    # Prepare kernel
    side = int(np.floor(rad) * 2) + 1
    center = (side - 1) / 2
    kern = np.zeros([side, side])
    kern_x, kern_y = np.meshgrid(np.arange(side), np.arange(side))
    kern_dist = np.hypot((kern_x - center), (kern_y - center))
    kern[np.where(kern_dist <= rad)] = 1
    n_kern = np.sum(kern)

    # Convolve maps
    map_conv = ndimage.convolve(map1, kern, mode='mirror')
    map_conv /= n_kern

    return map_conv


def convolve_cube(cube, rad):
    """Convolve/bin a data cube using a circular top-hat kernel of a given
    diameter.

    Parameters:
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param rad: Radius of the circular top-hat kernel (px)
        :type rad: float

    Returns:
        :return cube_conv: Convolved/binned data cube
        :rtype cube_conv: numpy.ndarray [n, y, x] (float)
    """

    # Convolve/bin cube
    cube_conv = copy.deepcopy(cube)

    for i_row in range(cube.shape[1]):
        for i_col in range(cube.shape[2]):
            beam_dir = beam(cube, [i_col, i_row], rad)
            cube_conv_raw = bin_spx(cube, beam_dir[1], beam_dir[0])
            cube_conv[:, i_row, i_col] = cube_conv_raw / len(beam_dir[0])

    return cube_conv


def grab_map_h(maps, param):
    """Select the H band map of a fitted parameter

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof)
        :param param: Fitted parameter to map
        :type param: str

    Returns:
        :return map1: Map of a fitting parameter
        :rtype map1: numpy.ndarray [y, x] (float)
        :return title_str: Map title
        :rtype title_str: str
    """

    # Select map
    map1 = np.zeros(maps.shape)
    title_str = ""

    if param == 'cont':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].cont.value

        title_str = "Continuum"
    elif param == 'bb_0':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].bb_0.value

        title_str = "BB offset"
    elif param == 'o3rn_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3rn_p.value

        title_str = r"[OIII]$_{r,n}$ peak"
    elif param == 'o3rn_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3rn_c.value

        title_str = r"[OIII]$_{r,n}$ center"
    elif param == 'o3rn_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3rn_s.value

        title_str = r"[OIII]$_{r,n}$ $\sigma$"
    elif param == 'o3rn_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3rn_i.value

        title_str = r"[OIII]$_{r,n}$ integrated"
    elif param == 'o3bn_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3bn_p.value

        title_str = r"[OIII]$_{b,n}$ peak"
    elif param == 'o3bn_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3bn_c.value

        title_str = r"[OIII]$_{b,n}$ center"
    elif param == 'o3bn_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3bn_s.value

        title_str = r"[OIII]$_{b,n}$ $\sigma$"
    elif param == 'o3bn_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3bn_i.value

        title_str = r"[OIII]$_{b,n}$ integrated"
    elif param == 'o3n_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].o3n_i.value

        title_str = r"[OIII]$_{n}$ integrated"
    elif param == 'b_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].b_p.value

        title_str = "Broad line peak"
    elif param == 'b_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].b_c.value

        title_str = "Broad line center"
    elif param == 'b_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].b_s.value

        title_str = r"Broad line $\sigma$"
    elif param == 'b_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].b_i.value

        title_str = "Broad line integrated"

    return map1, title_str


def grab_map_k(maps, param):
    """Select the K band map of a fitted parameter

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.k_prof)
        :param param: Fitted parameter to map
        :type param: str

    Returns:
        :return map1: Map of a fitting parameter
        :rtype map1: numpy.ndarray [y, x] (float)
        :return title_str: Map title
        :rtype title_str: str
    """

    # Select map
    map1 = np.zeros(maps.shape)
    title_str = ""

    if param == 'cont':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].cont.value

        title_str = "Continuum"
    elif param == 'bb_0':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].bb_0.value

        title_str = "BB offset"
    elif param == 'bb_1':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].bb_1.value

        title_str = "BB slope"
    elif param == 'hab_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].hab_p.value

        title_str = r"H$_{\alpha,b}$ peak"
    elif param == 'hab_p_err':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].hab_p_err.value

        title_str = r"H$_{\alpha,b}$ peak error"
    elif param == 'hab_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].hab_c.value

        title_str = r"H$_{\alpha,b}$ center"
    elif param == 'hab_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].hab_s.value

        title_str = r"H$_{\alpha,b}$ $\sigma$"
    elif param == 'hab_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].hab_i.value

        title_str = r"H$_{\alpha,b}$ integrated"
    elif param == 'han_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].han_p.value

        title_str = r"H$_{\alpha,n}$ peak"
    elif param == 'han_p_err':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].han_p_err.value

        title_str = r"H$_{\alpha,n}$ peak error"
    elif param == 'han_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].han_c.value

        title_str = r"H$_{\alpha,n}$ center"
    elif param == 'han_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].han_s.value

        title_str = r"H$_{\alpha,n}$ $\sigma$"
    elif param == 'han_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].han_i.value

        title_str = r"H$_{\alpha,n}$ integrated"
    elif param == 'han_f':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].han_f.value

        title_str = r"H$_{\alpha,n}$ flux density"
    elif param == 'n2r_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2r_p.value

        title_str = r"[NII]$_{r}$ peak"
    elif param == 'n2r_p_err':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2r_p_err.value

        title_str = r"[NII]$_{r}$ peak error"
    elif param == 'n2r_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2r_c.value

        title_str = r"[NII]$_{r}$ center"
    elif param == 'n2r_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2r_s.value

        title_str = r"[NII]$_{r}$ $\sigma$"
    elif param == 'n2r_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2r_i.value

        title_str = r"[NII]$_{r}$ integrated"
    elif param == 'n2b_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2b_p.value

        title_str = r"[NII]$_{b}$ peak"
    elif param == 'n2b_p_err':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2b_p_err.value

        title_str = r"[NII]$_{b}$ peak error"
    elif param == 'n2b_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2b_c.value

        title_str = r"[NII]$_{b}$ center"
    elif param == 'n2b_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2b_s.value

        title_str = r"[NII]$_{b}$ $\sigma$"
    elif param == 'n2b_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2b_i.value

        title_str = r"[NII]$_{b}$ integrated"
    elif param == 'n2_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].n2_i.value

        title_str = r"[NII] integrated"
    elif param == 's2b_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2b_p.value

        title_str = r"[SII]$_{b}$ peak"
    elif param == 's2b_p_err':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2b_p_err.value

        title_str = r"[SII]$_{b}$ peak error"
    elif param == 's2b_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2b_c.value

        title_str = r"[SII]$_{b}$ center"
    elif param == 's2b_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2b_s.value

        title_str = r"[SII]$_{b}$ $\sigma$"
    elif param == 's2b_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2b_i.value

        title_str = r"[SII]$_{b}$ integrated"
    elif param == 's2r_p':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2r_p.value

        title_str = r"[SII]$_{r}$ peak"
    elif param == 's2r_p_err':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2r_p_err.value

        title_str = r"[SII]$_{r}$ peak error"
    elif param == 's2r_c':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2r_c.value

        title_str = r"[SII]$_{r}$ center"
    elif param == 's2r_s':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2r_s.value

        title_str = r"[SII]$_{r}$ $\sigma$"
    elif param == 's2r_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2r_i.value

        title_str = r"[SII]$_{r}$ integrated"
    elif param == 's2_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].s2_i.value

        title_str = r"[SII] integrated"
    elif param == 'ha_i':
        for i_row in range(map1.shape[0]):
            for i_col in range(map1.shape[1]):
                map1[i_row, i_col] = maps[i_row, i_col].ha_i.value

        title_str = r"H$_{\alpha}$ integrated"
    else:
        warnings.warn("Map parameter '{0}' not accepted".format(param))

    return map1, title_str


def bin_map(maps, param, beam_coo, method='sum'):
    """Bin spaxels of a fitted parameter using the sum, mean or median. The
    sum standard deviation or the standard error is also calculated.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.k_prof)
        :param param: Fitted parameter to map
        :type param: str
        :param beam_coo: Coordinates [y, x] of the spaxels in the beam (px)
        :type beam_coo: tuple [2] (numpy.ndarray [n] (int))
        :param method: Method for combining the parameter ('sum' for sum,
            'mean' for mean, 'median' for median)
        :type method: str, default 'sum'

    Returns:
        :return binned: Binned parameter (sum, mean or median)
        :rtype binned: float
        :return err: Sum standard deviation or standard error of the fitted
            parameter
        :rtype err: float
    """

    # Bin spaxels
    map_grab, _ = grab_map_k(maps, param)
    map_beam = map_grab[beam_coo[0], beam_coo[1]]

    if method == 'sum':
        binned = np.sum(map_beam)
        err = np.std(map_beam) * np.sqrt(len(map_beam))
    elif method == 'mean':
        binned = np.mean(map_beam)
        err = np.std(map_beam) / np.sqrt(len(map_beam))
    else:
        binned = np.median(map_beam)
        err = np.std(map_beam) / np.sqrt(len(map_beam))

    return binned, err


def grab_map_snr(maps, param):
    """Select the SNR map of a fitted peak parameter. This works only for the K
    band spectrum.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.k_prof)
        :param param: Fitted parameter to map
        :type param: str

    Returns:
        :return map1: SNR map of a fitting parameter
        :rtype map1: numpy.ndarray [y, x] (float)
        :return title_str: Map title
        :rtype title_str: str
    """

    # Select map
    map_s, title_str = grab_map_k(maps, param)
    map_n, _ = grab_map_k(maps, (param + '_err'))
    map1 = np.abs(map_s / map_n)
    title_str += "'s SNR"

    return map1, title_str


def grid_spectrum(wvl, cube, sky=None, fit_mod=None, exclude_fit=None,
                  print_han=None, show_lines=None, ref_lambda=None, zoom=None,
                  i_range=None, title="", compass=False, close=True,
                  save=None):
    """Plot a grid of spectra.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param sky: Telluric lines ranges (min, max) (um)
        :type sky: numpy.ndarray [:, 2] (float), optional
        :param fit_mod: Map of spectrum fitting model
        :type fit_mod: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof), optional
        :param exclude_fit: Spectra not fitted with model ([[row, column]])
        :type exclude_fit: list [n] (list [2] (int)), optional
        :param print_han: Velocity reference used to print the narrow H_a
            fitted velocity under each fitted spectrum (m/s)
        :type print_han: float, optional
        :param show_lines: Show lines centers and labels (None, 'all', 'single'
            for narrow H_a line or narrow red [OIII] only, 'no_broad' for all
            except broad H_a line)
        :type show_lines: str, default None
        :param ref_lambda: Reference wavelength to plot (um)
        :type ref_lambda: float, optional
        :param zoom: Wavelength zoom limits [min, max] (um)
        :type zoom: list [2] (float), optional
        :param i_range: Spectrum intensity limits [central subplot, other
            subplots] [min, max]
        :type i_range: list [2] (list [2] (float)), optional
        :param title: Plot title
        :type title: str, default ""
        :param compass: Show compass
        :type compass: bool, default False
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Plot the figure
    sides = cube.shape[1:]
    fig = plt.figure(figsize=(8, 9))
    ax = None

    for i_row in range(sides[0]):
        for i_col in range(sides[1]):
            if all(isinstance(el, list) for el in i_range):
                if (i_row == int((sides[0] - 1) / 2)) & \
                        (i_col == int((sides[1] - 1) / 2)):
                    i_range_i = i_range[0]
                else:
                    i_range_i = i_range[1]
            else:
                i_range_i = i_range

            print_han_i = False

            if fit_mod is None:
                fit_mod_i = None
            else:
                if exclude_fit is None:
                    fit_mod_i = fit_mod[i_row, i_col]

                    if print_han:
                        print_han_i = True
                else:
                    if [i_row, i_col] in exclude_fit:
                        fit_mod_i = None
                    else:
                        fit_mod_i = fit_mod[i_row, i_col]

                        if print_han:
                            print_han_i = True

            if print_han_i:
                _, _, _, v_n, _, _, _, _, _ = fit_nifs.phys_lines_k(fit_mod_i)
                v_n -= print_han
                v_n *= 1e-3
                print_han_str = "v = {0:.0f} km/s".format(v_n)
            else:
                print_han_str = ""

            ax = plt.subplot(sides[0], sides[1],
                             (((sides[0] - i_row - 1) * sides[0]) +
                              (sides[1] - i_col)))
            plot_spectrum_in(ax, wvl, cube[:, i_row, i_col], sky=sky,
                             fit_mod=fit_mod_i,
                             zoom=zoom, i_range=i_range_i,
                             show_lines=show_lines, label_lines=None,
                             show_axis=None, under_label=print_han_str,
                             ref_lambda=ref_lambda)

    fig.suptitle(title)

    # Plot compass
    if compass:
        ax.annotate('E', xy=(0.9735, 0.03), xytext=(0.9, 0.03),
                    ha='right', va='center', xycoords='figure fraction',
                    arrowprops=dict(arrowstyle='<-'), fontsize=16)
        ax.annotate('N', xy=(0.97, 0.0266), xytext=(0.97, 0.1), ha='center',
                    xycoords='figure fraction',
                    arrowprops=dict(arrowstyle='<-'), fontsize=16)

    # Save image file
    if save is not None:
        plt.savefig(save)
        plt.savefig(path.splitext(save)[0] + '.pdf')

    # Close the image
    if close:
        plt.close()
    else:
        plt.show(block=False)


def grid_spectrum_one(wvl, cube, x, y, ps, sky=None, fit_mod=None, zoom=None,
                      i_range=None, show_lines='all', label_lines='top',
                      y_label=None, title="", fit_b=True, close=True,
                      save=None):
    """Plot a spectrum selected from a data cube, based on the closest
    position.

    Parameters:
        :param wvl: Wavelengths of the spectrum (um)
        :type wvl: numpy.ndarray [n] (float)
        :param cube: Data cube
        :type cube: numpy.ndarray [n, y, x] (float)
        :param x: x position (")
        :type x: float
        :param y: x position (")
        :type y: float
        :param ps: Spaxel scale ("/px)
        :type ps: float
        :param sky: Telluric lines ranges (min, max) (um)
        :type sky: numpy.ndarray [:, 2] (float), optional
        :param fit_mod: Map of spectrum fitting model
        :type fit_mod: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof), optional
        :param zoom: Wavelength zoom limits [min, max] (um)
        :type zoom: list [2] (float), optional
        :param i_range: Spectrum intensity limits [min, max]
        :type i_range: list [2] (float), optional
        :param show_lines: Show lines centers and labels (None, 'all' or
            'single' for narrow H_a line or narrow red [OIII] only)
        :type show_lines: str, default 'all'
        :param label_lines: Position of the lines labels ('top' o 'bottom')
        :type label_lines: str, default 'top'
        :param y_label: y axis label
        :type y_label: str, optional
        :param title: Plot title
        :type title: str, default ""
        :param fit_b: Broad line was fitted
        :type fit_b: bool, default True
        :param close: Close the figure and don't show it
        :type close: bool, default True
        :param save: Path and filename of the saved image
        :type save: str, optional
    """

    # Select the direction
    grid_x, grid_y = np.meshgrid(((np.arange(0, cube.shape[2]) -
                                   ((cube.shape[2] - 1) / 2)) * ps),
                                 ((np.arange(0, cube.shape[1]) -
                                   ((cube.shape[1] - 1) / 2)) * ps))
    dist = np.hypot(grid_x - x, grid_y - y)
    selects = np.where(dist == np.min(dist))
    select = [selects[0][0], selects[1][0]]

    spectrum = cube[:, select[0], select[1]]
    fit_mod_single = fit_mod[select[0], select[1]]

    # Plot the spectrum
    plot_spectrum(wvl, spectrum, sky=sky, fit_mod=fit_mod_single, zoom=zoom,
                  i_range=i_range, show_lines=show_lines,
                  label_lines=label_lines, y_label=y_label, title=title,
                  fit_b=fit_b, close=close, save=save)


# noinspection PyTypeChecker
def map_sub(maps, param, model_sub, mask_map=None, mask_thr=0, mask_lim='low'):
    """Subtract the map of a fitted parameter with a 2D Moffat model, fitting
    only the peak and background values.

    Parameters:
        :param maps: Maps of fitted parameters
        :type maps: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param param: Fitted parameter to map
        :type param: str
        :param model_sub: 2D Moffat model to fit and subtract (peak,
            background, central x coordinate (px), central y coordinate (px),
            first width parameter (px), second width parameter (px), position
            angle (rad), exponential parameter)
        :type model_sub: list [8] (float)
        :param mask_map: Fitted parameter to use as mask
        :type mask_map: str, optional
        :param mask_thr: Threshold value for masking
        :type mask_thr: float, default 0
        :param mask_lim: Limit type for masking ('up' for upper, 'low ' for
            lower)
        :type mask_lim: str, default 'low'

    Returns:
        :return map2: Map of a fitting parameter
        :rtype map2: numpy.ndarray [y, x] (float)
        :return title_str: Map title
        :rtype title_str: str
    """

    # Select map
    map1 = None
    title_str = ""

    if isinstance(maps[0, 0], fit_nifs.h_prof):
        map1, title_str = grab_map_h(maps, param)
    elif isinstance(maps[0, 0], fit_nifs.k_prof):
        map1, title_str = grab_map_k(maps, param)

    # Subtract the 2D Moffat model
    fit_sub, _ = fit_nifs.moffat2d_fit(map1, model_fit=model_sub)
    map2 = fit_nifs.resid_moffat2d(map1, fit_sub)

    # Mask map
    if mask_map:
        mapm = None

        if isinstance(maps[0, 0], fit_nifs.h_prof):
            mapm, _ = grab_map_h(maps, mask_map)
        elif isinstance(maps[0, 0], fit_nifs.k_prof):
            mapm, _ = grab_map_k(maps, mask_map)

        if mask_lim == 'low':
            mapm_bool = mapm < mask_thr
        else:
            mapm_bool = mapm > mask_thr

        map2 = np.ma.masked_where(mapm_bool, map1)

    return map2, title_str


def snrs(img, signal_pos, bkg_pos):
    """Calculate the SNR of an object on an image as a function of the aperture
    radius.

    Parameters:
        :param img: Image
        :type img: numpy.ndarray [u, v] (float)
        :param signal_pos: Position of the object (px) ([y, x])
        :type signal_pos: list [2]
        :param bkg_pos: Position of the background patch (px) ([y, x, radius])
        :type bkg_pos: list [3]

    Returns:
        :return rs: Radii (px)
        :rtype rs: numpy.ndarray [n]
        :return snrs_out: SNRs
        :rtype snrs_out: numpy.ndarray [n]
    """

    # Measure noise
    bkg_mesh = np.indices(np.shape(img))
    bkg_dist = np.hypot((bkg_mesh[0] - bkg_pos[1]), (bkg_mesh[1] - bkg_pos[0]))
    bkg_where = np.where(bkg_dist <= bkg_pos[2])
    bkgs = img[bkg_where]
    bkg = np.median(bkgs)
    bkg_noise = np.std(bkgs)

    # Measure signal
    signal_mesh = np.indices(np.shape(img))
    rs = np.around(np.arange(1, int(np.min(np.shape(img)) / 2), 0.1),
                   decimals=10)
    signals = np.zeros(len(rs))
    areas = np.zeros(len(rs))

    for i_r in range(len(rs)):
        signal_dist = np.hypot((signal_mesh[0] - signal_pos[1]),
                               (signal_mesh[1] - signal_pos[0]))
        signal_where = np.where(signal_dist <= rs[i_r])
        signal = img[signal_where] - bkg
        signals[i_r] = np.sum(signal)
        areas[i_r] = len(signal_where[0])

    # Calculate SNR
    snrs_out = signals * np.sqrt(params_nifs.gain) / \
        np.sqrt(signals + ((bkg_noise ** 2) * areas))

    return rs, snrs_out


# noinspection PyTypeChecker
def clean_maps(maps_in, center, north=2, south=10, east=0, west=9, rad=8,
               params=('cont', 'o3n'), ps=0, ticks=0, save=None):
    """Clean fitted maps by removing the background vertical stripes. Only
    selected maps are cleaned.
    An auxiliary plot can be saved.

    Parameters:
        :param maps_in: Maps of fitted parameters
        :type maps_in: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param center: Center of the parget ([x, y]) (px)
        :type center: list [2] (float)
        :param north: Number of northern pixel rows not used
        :type north: int, default 2
        :param north: Number of northern pixel rows not used
        :type north: int, default 2
        :param south: Number of southern pixel rows not used
        :type south: int, default 2
        :param east: Number of eastern pixel columns not used
        :type east: int, default 0
        :param west: Number of western pixel columns not used
        :type west: int, default 9
        :param rad: Radius of pixel not used around the target (px)
        :type rad: float, default 8
        :param params: Fitted parameters that are cleaned (using only continuum
            or integrated flux)
        :type params: list, tuple [a] (str), default ('cont', 'o3n')
        :param ps: Spaxel scale ("/px)
        :type ps: float, default 0
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float, default 0
        :param save: Path of the saved plot
        :type save: str, optional

    Returns:
        :return maps_filt: Filtered maps of fitted parameters
        :rtype maps_filt: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
    """

    # Select maps
    parameters = maps_in[0, 0].param_names
    maps_filt = copy.deepcopy(maps_in)

    for i_param in range(len(parameters)):

        # Clean map
        map1 = None
        param = parameters[i_param]
        param_pre = param.split('_')[0]

        if (not ((param[-2:] == '_i') or
                 (param == 'cont'))) or (param_pre not in params):
            continue

        if isinstance(maps_in[0, 0], fit_nifs.h_prof):
            map1, _ = grab_map_h(maps_in, parameters[i_param])
        elif isinstance(maps_in[0, 0], fit_nifs.k_prof):
            map1, _ = grab_map_k(maps_in, parameters[i_param])

        if save:
            save_tmp = path.join(save, 'map_' + param + '_cleaning.png')
        else:
            save_tmp = None

        map1_filt = clean_map(map1, center, north=north, south=south,
                              east=east, west=west, rad=rad, ps=ps,
                              ticks=ticks, save=save_tmp)
        assign_map(map1_filt, maps_filt, i_param)

    return maps_filt


def clean_map(map_in, center, north=2, south=10, east=0, west=9, rad=8, ps=0,
              ticks=0, scale=1, save=None):
    """Clean fitted map by removing the background vertical stripes. The bottom
    part of the image is not cleaned.
    An auxiliary plot can be saved.

    Parameters:
        :param map_in: Map of fitted parameter
        :type map_in: numpy.ndarray [y, x] (float)
        :param center: Center of the parget ([x, y]) (px)
        :type center: list [2] (float)
        :param north: Number of northern pixel rows not used
        :type north: int, default 2
        :param north: Number of northern pixel rows not used
        :type north: int, default 2
        :param south: Number of southern pixel rows not used
        :type south: int, default 2
        :param east: Number of eastern pixel columns not used
        :type east: int, default 0
        :param west: Number of western pixel columns not used
        :type west: int, default 9
        :param rad: Radius of pixel not used around the target (px)
        :type rad: float, default 8
        :param ps: Spaxel scale ("/px)
        :type ps: float, default 0
        :param ticks: Distance of the spaxel axes' ticks (")
        :type ticks: float, default 0
        :param scale: Intensity scale factor for the plot
        :type scale: float, default 1
        :param save: Path and filename of the saved image
        :type save: str, optional

    Returns:
        :return map_filt: Filtered map of fitted parameters
        :rtype map_filt: numpy.ndarray [y, x] (float)
    """

    # Clean map
    y, x = np.mgrid[: map_in.shape[0], : map_in.shape[1]]
    where_xy = (x >= west) & (x < (map_in.shape[1] - east)) & (y >= south) & \
        (y < (map_in.shape[0] - north)) & \
        (np.hypot((x - center[0]), (y - center[1])) > rad)
    map2 = copy.deepcopy(map_in)
    map2[~where_xy] = np.nan
    warnings.simplefilter('ignore')
    filter_1d = np.nanmedian(map2, axis=0)
    warnings.simplefilter('default')
    filter_1d[: (west + 0)] = filter_1d[west + 0]
    filter_1d -= np.mean(filter_1d)
    filter_2d = np.repeat(filter_1d[np.newaxis, :], map_in.shape[0], axis=0)
    filter_2d[: 8, :] = 0
    map1_filt = map_in - filter_2d

    # Plot
    if save:
        plot_1d = (filter_1d * map_in.shape[0] * 0.3 /
                   np.max(np.abs(filter_1d))) + (map_in.shape[0] / 2)
        plot_img((map2 * scale), ps, ticks, c_label=None, title="",
                 compass=True, legend=False, plot_1d=plot_1d, close=True,
                 save=save)

    return map1_filt


# noinspection PyTypeChecker
def assign_map(map_in, maps_in, i_param):
    """Assign a single fitted map to fitted maps.

    Parameters:
        :param map_in: Map of a fitted parameter
        :type map_in: numpy.ndarray [y, x] (float)
        :param maps_in: Maps of fitted parameters
        :type maps_in: numpy.ndarray [y, x] (astropy.modeling.core.h_prof or
            astropy.modeling.core.k_prof)
        :param i_param: Index of the parameter
        :type i_param: int
    """

    # Assign map
    for i_row in range(map_in.shape[0]):
        for i_col in range(map_in.shape[1]):
            if isinstance(maps_in[0, 0], fit_nifs.h_prof):
                if i_param == 0:
                    maps_in[i_row, i_col].cont.value = map_in[i_row, i_col]
                elif i_param == 1:
                    maps_in[i_row, i_col].bb_0.value = map_in[i_row, i_col]
                elif i_param == 2:
                    maps_in[i_row, i_col].o3rn_p.value = map_in[i_row, i_col]
                elif i_param == 3:
                    maps_in[i_row, i_col].o3rn_c.value = map_in[i_row, i_col]
                elif i_param == 4:
                    maps_in[i_row, i_col].o3rn_s.value = map_in[i_row, i_col]
                elif i_param == 5:
                    maps_in[i_row, i_col].o3rn_i.value = map_in[i_row, i_col]
                elif i_param == 6:
                    maps_in[i_row, i_col].o3bn_p.value = map_in[i_row, i_col]
                elif i_param == 7:
                    maps_in[i_row, i_col].o3bn_c.value = map_in[i_row, i_col]
                elif i_param == 8:
                    maps_in[i_row, i_col].o3bn_s.value = map_in[i_row, i_col]
                elif i_param == 9:
                    maps_in[i_row, i_col].o3bn_i.value = map_in[i_row, i_col]
                elif i_param == 10:
                    maps_in[i_row, i_col].o3n_i.value = map_in[i_row, i_col]
                elif i_param == 11:
                    maps_in[i_row, i_col].b_p.value = map_in[i_row, i_col]
                elif i_param == 12:
                    maps_in[i_row, i_col].b_c.value = map_in[i_row, i_col]
                elif i_param == 13:
                    maps_in[i_row, i_col].b_s.value = map_in[i_row, i_col]
                elif i_param == 14:
                    maps_in[i_row, i_col].b_i.value = map_in[i_row, i_col]
            elif isinstance(maps_in[0, 0], fit_nifs.k_prof):
                if i_param == 0:
                    maps_in[i_row, i_col].cont.value = map_in[i_row, i_col]
                elif i_param == 1:
                    maps_in[i_row, i_col].bb_0.value = map_in[i_row, i_col]
                elif i_param == 2:
                    maps_in[i_row, i_col].bb_1.value = map_in[i_row, i_col]
                elif i_param == 3:
                    maps_in[i_row, i_col].hab_p.value = map_in[i_row, i_col]
                elif i_param == 4:
                    maps_in[i_row, i_col].hab_p_err.value = \
                        map_in[i_row, i_col]
                elif i_param == 5:
                    maps_in[i_row, i_col].hab_c.value = map_in[i_row, i_col]
                elif i_param == 6:
                    maps_in[i_row, i_col].hab_s.value = map_in[i_row, i_col]
                elif i_param == 7:
                    maps_in[i_row, i_col].hab_i.value = map_in[i_row, i_col]
                elif i_param == 8:
                    maps_in[i_row, i_col].han_p.value = map_in[i_row, i_col]
                elif i_param == 9:
                    maps_in[i_row, i_col].han_p_err.value = \
                        map_in[i_row, i_col]
                elif i_param == 10:
                    maps_in[i_row, i_col].han_c.value = map_in[i_row, i_col]
                elif i_param == 11:
                    maps_in[i_row, i_col].han_s.value = map_in[i_row, i_col]
                elif i_param == 12:
                    maps_in[i_row, i_col].han_i.value = map_in[i_row, i_col]
                elif i_param == 13:
                    maps_in[i_row, i_col].han_f.value = map_in[i_row, i_col]
                elif i_param == 13:
                    maps_in[i_row, i_col].n2r_p.value = map_in[i_row, i_col]
                elif i_param == 14:
                    maps_in[i_row, i_col].n2r_p_err.value = \
                        map_in[i_row, i_col]
                elif i_param == 15:
                    maps_in[i_row, i_col].n2r_c.value = map_in[i_row, i_col]
                elif i_param == 16:
                    maps_in[i_row, i_col].n2r_s.value = map_in[i_row, i_col]
                elif i_param == 17:
                    maps_in[i_row, i_col].n2r_i.value = map_in[i_row, i_col]
                elif i_param == 18:
                    maps_in[i_row, i_col].n2b_p.value = map_in[i_row, i_col]
                elif i_param == 19:
                    maps_in[i_row, i_col].n2b_p_err.value = \
                        map_in[i_row, i_col]
                elif i_param == 20:
                    maps_in[i_row, i_col].n2b_c.value = map_in[i_row, i_col]
                elif i_param == 21:
                    maps_in[i_row, i_col].n2b_s.value = map_in[i_row, i_col]
                elif i_param == 22:
                    maps_in[i_row, i_col].n2b_i.value = map_in[i_row, i_col]
                elif i_param == 23:
                    maps_in[i_row, i_col].n2_i.value = map_in[i_row, i_col]
                elif i_param == 24:
                    maps_in[i_row, i_col].s2b_p.value = map_in[i_row, i_col]
                elif i_param == 25:
                    maps_in[i_row, i_col].s2b_p_err.value = \
                        map_in[i_row, i_col]
                elif i_param == 26:
                    maps_in[i_row, i_col].s2b_c.value = map_in[i_row, i_col]
                elif i_param == 27:
                    maps_in[i_row, i_col].s2b_s.value = map_in[i_row, i_col]
                elif i_param == 28:
                    maps_in[i_row, i_col].s2b_i.value = map_in[i_row, i_col]
                elif i_param == 29:
                    maps_in[i_row, i_col].s2r_p.value = map_in[i_row, i_col]
                elif i_param == 30:
                    maps_in[i_row, i_col].s2r_p_err.value = \
                        map_in[i_row, i_col]
                elif i_param == 31:
                    maps_in[i_row, i_col].s2r_c.value = map_in[i_row, i_col]
                elif i_param == 32:
                    maps_in[i_row, i_col].s2r_s.value = map_in[i_row, i_col]
                elif i_param == 33:
                    maps_in[i_row, i_col].s2r_i.value = map_in[i_row, i_col]
                elif i_param == 34:
                    maps_in[i_row, i_col].s2_i.value = map_in[i_row, i_col]
