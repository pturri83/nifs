"""Analyze the NIFS data cubes of the single exposures of the telluric stars.

References:
    [ref1] Greisen et Calabretta, 2002
"""


import datetime
import glob
import re
from os import makedirs, pardir, path

from astropy.io import fits
from matplotlib import dates, pyplot as plt
import numpy as np

from source import cube_nifs, fit_nifs, params_nifs


# Data parameters
root_folder = '/Volumes/Astro/Data/NIFS/'  # Root data folder
target_folder = '084933A'  # Target subfolder
data_combine = [[2017, 'H', '084933a_17a', '20170407', 'obs59', 1, 'center'],
                [2017, 'K', '084933a_17a', '20170407', 'obs47', 1, 'max'],
                [2017, 'K', '084933a_17a', '20170407', 'obs51', 2, 'max'],
                [2017, 'K', '084933a_17a', '20170410', 'obs69', 3, 'max'],
                [2017, 'K', '084933a_17a', '20170410', 'obs73', 4, 'max'],
                [2019, 'K', '084933a_19b', '20200103', 'obs64', 1, 'max'],
                [2019, 'K', '084933a_19b', '20200103', 'obs68', 2, 'max'],
                [2021, 'K', '084933a_21b', '20220224', 'obs84', 1, 'center'],
                [2021, 'K', '084933a_21b', '20220224', 'obs139', 2, 'center'],
                [2021, 'K', '084933a_21b', '20220226', 'obs96', 3, 'center'],
                [2021, 'K', '084933a_21b', '20220226', 'obs141', 4, 'center']]
# Data subfolder ([year, band, semester, date, observation, observation number,
# peak guess])

# Analysis parameters
analyze_maps = False  # Analyze maps
rad = 0.5  # Radius of the analyzed beam (")
stack_range_list = [[1.55, 1.75], [2.05, 2.4]]  # Range of wavelengths to
# stack [H, K] (um)
n_slices = 24  # Number of slices in azimuthal profiles
dist_range = [0.5, 1]  # Range of distances in radial and azimuthal profiles
# ([min, max]) (")
wvl_range_cont = [2.1, 2.15]  # Range of K band wavelengths to measure the
# continuum (min, max) (um)
avg_step = 0.1  # Smallest distance step size to calculate the average in the
# radial plots of the quadrants (")

# Graphic parameters
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (")
max_fwhm = 0.5  # Maximum FWHM plotted (")
max_sr = 0.2  # Maximum SR plotted

# Analyze all data
print("\nProgram started")

for i_data in range(len(data_combine)):

    # Find data
    year = data_combine[i_data][0]
    band = data_combine[i_data][1]
    folder = data_combine[i_data][2]
    date = data_combine[i_data][3]
    obs = data_combine[i_data][4]
    cube_n = data_combine[i_data][5]
    pos_guess = data_combine[i_data][6]
    print("Year {0}, {1} band, telluric observation #{2}".format(year, band,
                                                                 cube_n))
    complete_folder = path.join(root_folder, folder, target_folder, date, band,
                                'Tellurics', obs)
    find_files = glob.glob(path.join(complete_folder, 'ctfbrsn*.fits'))
    save_folder = path.join(path.abspath(path.join(complete_folder, pardir,
                                                   pardir, pardir, pardir,
                                                   pardir)), 'plots',
                            'telluric')

    if not path.exists(save_folder):
        makedirs(save_folder)

    if band == 'H':
        stack_range = stack_range_list[0]
        lambda_c = params_nifs.lambda_h
    else:
        stack_range = stack_range_list[1]
        lambda_c = params_nifs.lambda_k

    # Analyze exposures
    n_files = len(find_files)
    fwhm_m = np.zeros(n_files)
    ell_m = np.zeros(n_files)
    pa_m = np.zeros(n_files)
    alpha_m = np.zeros(n_files)
    sr_m = np.zeros(n_files)
    times = np.zeros(n_files)
    fwhm_header = np.zeros(n_files)
    fwhm_header_lambda = np.zeros(n_files)
    object_id = ''
    lambda_e = 0

    for i_file in range(n_files):

        # Load file
        print("\r   Analyzing exposure # {0}/{1} ...".format((i_file + 1),
                                                             n_files), end="")
        folder_file = find_files[i_file]
        hdul = fits.open(folder_file)
        header_0 = hdul[0].header
        header_1 = hdul[1].header
        data = hdul[1].data
        hdul.close()
        data_shape = data.shape

        # Read header
        sps = header_1['CD1_1']
        date_str = header_0['DATE-OBS']
        time_str = header_0['UT']
        object_id = header_0['OBJECT']

        if year != 2017:
            fwhm_header[i_file] = header_0['AOSEEING']
            fwhm_header_lambda[i_file] = fwhm_header[i_file] * lambda_c / \
                params_nifs.lambda_seeing

        # Calculate time
        date_list = re.split('-', date_str)
        date_num = [int(i) for i in date_list]
        time_list = re.split('[:.]', time_str)
        time_num = [int(i) for i in time_list]
        date_obj = datetime.datetime(date_num[0], date_num[1], date_num[2],
                                     time_num[0], time_num[1], time_num[2])
        times[i_file] = dates.date2num(date_obj)

        # Calculate wavelengths
        slice_idx = np.arange(data_shape[0])
        wvl = ((slice_idx + 1 - header_1['CRPIX3']) * header_1['CD3_3']) + \
            header_1['CRVAL3']  # [ref1]
        wvl /= 10000
        wvl_ch = np.mean(wvl[1:] - wvl[: -1])
        sky = params_nifs.sky_compile(params_nifs.sky_c, params_nifs.sky_w[0])

        # Clean data
        wvl_slice, data_slice = cube_nifs.cut_cube(wvl, data, stack_range[0],
                                                   stack_range[1])
        wvl_clean, data_clean = cube_nifs.clean_cube(wvl_slice, data_slice,
                                                     params_nifs.sky_c,
                                                     params_nifs.sky_w[1])

        # Collapse data
        data_coll = cube_nifs.collapse_cube(data_clean)

        # Select beam
        direction_tmp = np.unravel_index(data_coll.argmax(), data_coll.shape)
        direction = [direction_tmp[1], direction_tmp[0]]
        beam = cube_nifs.beam(data, direction, (rad / sps))
        beam_bin = cube_nifs.bin_spx(data, beam[1], beam[0])

        # Show selected beam
        data_coll_plot = data_coll / (sps ** 2)
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, direction=direction,
                           beam_pos=beam,
                           c_label=("Intensity\n(" +
                                    params_nifs.flux_spat_unit_unc + ")"),
                           title="Collapsed cube with beam",
                           save=path.join(save_folder,
                                          ('coll_beam_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show beam spectrum
        beam_bin_plot = beam_bin / wvl_ch
        cube_nifs.plot_spectrum(wvl, beam_bin_plot, sky=sky,
                                y_label=("Intensity (" +
                                         params_nifs.flux_dens_unit_unc + ")"),
                                title="Beam spectrum",
                                save=path.join(save_folder,
                                               ('spectr_beam_' + band + '_' +
                                                str(cube_n) + '_' +
                                                str(i_file + 1) + '.png')))

        # Calculate equivalent wavelength
        beam_clean = cube_nifs.beam(data_clean, direction, (rad / sps))
        beam_bin_clean = cube_nifs.bin_spx(data_clean, beam_clean[1],
                                           beam_clean[0])
        lambda_e = np.sum(wvl_clean * beam_bin_clean) / np.sum(beam_bin_clean)

        # Measure FWHM
        fit_psf_m, _ = fit_nifs.moffat2d_fit(data_coll, pos_guess=pos_guess)
        s_m = np.sqrt(fit_psf_m[4] * fit_psf_m[5])
        fwhms_m = 2 * np.array(fit_psf_m[4: 6]) * \
            np.sqrt((2 ** (1 / fit_psf_m[7])) - 1) * sps
        fwhm_m[i_file] = np.sqrt(fwhms_m[0] * fwhms_m[1])
        fwhm_m_max = np.max(fwhms_m)
        fwhm_m_min = np.min(fwhms_m)
        ell_m[i_file] = (fwhm_m_max - fwhm_m_min) / fwhm_m_max
        pa_m[i_file] = fit_psf_m[6]
        alpha_m[i_file] = fit_psf_m[7]
        sr_m[i_file] = fit_nifs.sr(fit_psf_m, 'moffat', lambda_e, sps)

        # Show collapsed cube for printing
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           c_label=("Intensity\n(" +
                                    params_nifs.flux_spat_unit_unc + ")"),
                           compass=True, legend=False,
                           save=path.join(save_folder,
                                          ('print_coll_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show collapsed cube with fit
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           model=[fit_psf_m[2], fit_psf_m[3],
                                  (fwhms_m[0] / sps), (fwhms_m[1] / sps),
                                  np.rad2deg(pa_m[i_file]), fwhm_m[i_file]],
                           c_label=("Intensity\n(" +
                                    params_nifs.flux_spat_unit_unc + ")"),
                           title=("Collapsed cube\n (" + str(year) + ", " +
                                  band + " band, obs #" + str(cube_n) +
                                  ", exp #" + str(i_file + 1) + ", star " +
                                  object_id + ")"), compass=True,
                           save=path.join(save_folder,
                                          ('coll_fwhm_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show collapsed cube with fit for printing
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           model=[fit_psf_m[2], fit_psf_m[3],
                                  (fwhms_m[0] / sps), (fwhms_m[1] / sps),
                                  np.rad2deg(pa_m[i_file]), fwhm_m[i_file]],
                           c_label=("Intensity\n(" +
                                    params_nifs.flux_spat_unit_unc + ")"),
                           compass=True, legend=False,
                           save=path.join(save_folder,
                                          ('print_coll_fwhm_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show radial profile with fit
        peak_plot = fit_psf_m[0] / (sps ** 2)
        bkg_plot = fit_psf_m[1] / (sps ** 2)
        cube_nifs.plot_rad(data_coll_plot, sps, fit_psf_m[2: 4],
                           ("collapsed cube (" + band + " band, obs #" +
                            str(cube_n) + ", exp #" + str(i_file + 1) +
                            ", star " + object_id + ")"),
                           model_type='moffat',
                           model_data=[peak_plot, s_m, fit_psf_m[7], bkg_plot,
                                       fwhm_m[i_file]], zoom=0.5,
                           y_label=("Intensity\n(" +
                                    params_nifs.flux_spat_unit_unc + ")"),
                           save=path.join(save_folder,
                                          ('rad_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show radial profile with fit for printing
        cube_nifs.plot_rad(data_coll_plot, sps, fit_psf_m[2: 4], None,
                           model_type='moffat',
                           model_data=[peak_plot, s_m, fit_psf_m[7],
                                       bkg_plot, fwhm_m[i_file]], zoom=0.5,
                           y_label=("Intensity\n(" +
                                    params_nifs.flux_spat_unit_unc + ")"),
                           fwhm=False, legend=False,
                           save=path.join(save_folder,
                                          ('print_rad_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))
        print()

        # Analyze profile direction
        if analyze_maps and (band == 'K'):

            # Map lines
            print("      Analyzing maps ...")
            maps = fit_nifs.fit_map_k(wvl_clean, data_clean, wvl_range_cont,
                                      n_spaces=6)
            map_han_f, _ = cube_nifs.grab_map_k(maps, 'han_f')
            map_han_f_center_where = np.where(map_han_f == np.max(map_han_f))
            map_han_f_center = [map_han_f_center_where[1][0],
                                map_han_f_center_where[0][0]]

            # Show quadrant radial profile
            steps_quad1 = np.arange(5) * avg_step
            steps_quad2 = np.arange(steps_quad1[-1], dist_range[1],
                                    (2 * avg_step))
            steps_quad = np.concatenate([steps_quad1, steps_quad2[1:],
                                         [dist_range[1]]])
            tmp = params_nifs.flux_dens_spat_unit_unc
            cube_nifs.plot_rad_quad_map(maps, sps, map_han_f_center, 'han_f',
                                        steps_quad,
                                        y_label=("Flux density (" + tmp + ")"),
                                        save=path.join(save_folder,
                                                       ('rad_quad_han_f_' +
                                                        band + '_' +
                                                        str(cube_n) + '_' +
                                                        str(i_file + 1) +
                                                        '.png')))

            # Show quadrant radial profile for printing
            cube_nifs.plot_rad_quad_map(maps, sps, map_han_f_center, 'han_f',
                                        steps_quad,
                                        y_label=("Flux density (" + tmp + ")"),
                                        printing=False,
                                        save=path.join(save_folder,
                                                       ('print_rad_quad_han_f_'
                                                        + band + '_' +
                                                        str(cube_n) + '_' +
                                                        str(i_file + 1) +
                                                        '.png')))

            # Show azimuthal profile
            beam_az_han_f = np.array([[map_han_f_center[0],
                                       map_han_f_center[1],
                                       (dist_range[0] / sps)],
                                      [map_han_f_center[0],
                                       map_han_f_center[1],
                                       (dist_range[1] / sps)]])
            cube_nifs.plot_map(maps, sps, sp_ticks, 'han_f',
                               beam_circ=beam_az_han_f, calib=False,
                               save=path.join(save_folder,
                                              ('map_az_han_f_' + band + '_' +
                                               str(cube_n) + '_' +
                                               str(i_file + 1) + '.png')))
            cube_nifs.plot_az(maps, sps, map_han_f_center, 'han_f',
                              n_slices=n_slices, dist_range=dist_range,
                              y_label=("Average flux density\n(" +
                                       params_nifs.flux_dens_spat_unit_unc +
                                       ")"),
                              save=path.join(save_folder,
                                             ('az_han_f_' + band + '_' +
                                              str(cube_n) + '_' +
                                              str(i_file + 1) + '.png')))

    # Calculate FWHM
    fwhm_diffr = 1.22 * 0.206265 * lambda_e / params_nifs.d_out_gemini
    print("   Diffraction-limited FWHM: {0:.3f}\"".format(fwhm_diffr))
    fwhm_m_min = np.min(fwhm_m)
    fwhm_m_max = np.max(fwhm_m)
    fwhm_m_mean = np.mean(fwhm_m)
    print("   Minimum FWHM: {0:.3f}\"".format(fwhm_m_min))
    print("   Maximum FWHM: {0:.3f}\"".format(fwhm_m_max))
    print("   Mean FWHM: {0:.3f}\"".format(fwhm_m_mean))
    fwhm_header_mean = 0

    if year != 2017:
        fwhm_header_mean = np.mean(fwhm_header)
        print("   Mean seeing ({0:.1f} um): {1:.3f}\"".format(
            params_nifs.lambda_seeing, fwhm_header_mean))

    # Calculate SR
    sr_m_min = np.min(sr_m)
    sr_m_max = np.max(sr_m)
    sr_m_mean = np.mean(sr_m)
    print("   Minimum SR: {0:.3f}".format(sr_m_min))
    print("   Maximum SR: {0:.3f}".format(sr_m_max))
    print("   Mean SR: {0:.3f}".format(sr_m_mean))

    # Calculate position angles
    for i_angle in range(n_files - 1):
        if (pa_m[i_angle + 1] - pa_m[i_angle]) > (np.pi / 2):
            pa_m[i_angle + 1] -= np.pi
        elif (pa_m[i_angle + 1] - pa_m[i_angle]) < (-np.pi / 2):
            pa_m[i_angle + 1] += np.pi

    # Save profiles
    save_m = np.stack((fwhm_m, ell_m, pa_m, alpha_m, sr_m), axis=-1)
    np.savetxt(path.join(save_folder,
                         ('profile_' + band + '_' + str(cube_n) + '.txt')),
               save_m, fmt='%.4f',
               header=("Moffat: Geometric FWHM (px), ellipticity, " +
                       "PA (rad), alpha, SR"))

    # Show FWHM
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.plot_date(times, fwhm_m, '-ok',
                 label=(r"AO ($\overline{FWHM}$" +
                        "={0:.3f}\")".format(fwhm_m_mean)))

    if year == 2019:
        ax.plot_date(times, fwhm_header, '--ok',
                     label=(r"Seeing ({0:.1f} $\mu$m) ".format(
                         params_nifs.lambda_seeing) + r"($\overline{FWHM}$" +
                            "={0:.3f}\")".format(fwhm_header_mean)))

    ax.plot_date([times[0], times[-1]], ([fwhm_diffr] * 2), ':k',
                 label=(r"Diffraction-limited ($FWHM$" +
                        "={0:.3f}\")".format(fwhm_diffr)))
    ax.set_ylim(0, max_fwhm)
    formatter = dates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(rotation=30)
    ax.set_xlabel("UT")
    ax.set_ylabel("FWHM (\")")
    plt.title("PSF FWHM\n (" + str(year) + ", " + band + " band, obs #" +
              str(cube_n) + ", star " + object_id + ")")
    ax.legend()
    plt.savefig(path.join(save_folder,
                          ('fwhm_m_' + band + '_' + str(cube_n) + '.png')))
    plt.savefig(path.join(save_folder,
                          ('fwhm_m_' + band + '_' + str(cube_n) + '.pdf')))
    plt.close()

    # Show elongation
    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(111)
    ax1.set_position([0.15, 0.15, 0.7, 0.7])
    ax1.plot_date(times, ell_m, '-ob')
    ylim = ax1.get_ylim()
    plt.ylim(0, ylim[1])
    ax1.xaxis.set_major_formatter(formatter)
    ax1.xaxis.set_tick_params(rotation=30)
    ax1.tick_params(axis='y', labelcolor='b')
    ax1.set_xlabel("UT")
    ax1.set_ylabel("Ellipticity", c='b')
    ax2 = ax1.twinx()
    ax2.set_position([0.15, 0.15, 0.7, 0.7])
    ax2.plot_date(times, pa_m, '-or')
    ax2.tick_params(axis='y', labelcolor='r')
    ax2.set_ylabel("Angle (rad)", c='r')
    plt.title("PSF Elongation\n (" + str(year) + ", " + band + " band, obs #" +
              str(cube_n) + ", star " + object_id + ")")
    plt.savefig(path.join(save_folder,
                          ('elong_m_' + band + '_' + str(cube_n) + '.png')))
    plt.savefig(path.join(save_folder,
                          ('elong_m_' + band + '_' + str(cube_n) + '.pdf')))
    plt.close()

    # Show SR
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.plot_date(times, sr_m, '-ob', label=(r"Strehl ratio ($\overline{SR}$" +
                                            "={0:.3f})".format(sr_m_mean)))
    ax.set_ylim(0, max_sr)
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(rotation=30)
    ax.set_xlabel("UT")
    ax.set_ylabel("SR")
    plt.title("PSF SR\n (" + str(year) + ", " + band + " band, obs #" +
              str(cube_n) + ", star " + object_id + ")")
    ax.legend()
    plt.savefig(path.join(save_folder,
                          ('sr_m_' + band + '_' + str(cube_n) + '.png')))
    plt.savefig(path.join(save_folder,
                          ('sr_m_' + band + '_' + str(cube_n) + '.pdf')))
    plt.close()

print("Program terminated\n")
