"""Analyze a NIFS data cube of a stacked observation of 084933A.

References:
    [ref1] Greisen et Calabretta, 2002
"""


import copy
import glob
from os import makedirs, pardir, path
import re

from astropy.io import fits
import numpy as np

from source import cube_nifs, fit_nifs, misc_girmos, params_nifs


# Data parameters
root_folder = '/Volumes/Astro/Data/NIFS/'  # Root data folder
years = [2017, 2019, 2021]  # Observing years
bands = ['H', 'K']  # Observing bands

# Analysis parameters
pos_guesses = [None, None, [[36, 23], [36, 34]]]  # Position guesses (x, y) for
# fitting, based on year and stack number [px]
rad = 0.5  # Radius of the analyzed beam (")
stack_range_list = [[1.55, 1.75], [2.05, 2.35]]  # Range of wavelengths to
# stack [H, K] (um)
bkg_pos = [-0.9, 0.9, 0.4]  # Position of the patch used to measure the
# background noise (") ([x, y, radius])
snr_d = 0.4  # Fixed diameter of the aperture to calculate the SNR (")
star_psf = [[2021, 'K', 2, [53, 50]]]  # Stacks that have also a PSF star to
# analyze, with its (x, y) position [px]

# Graphic parameters
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (")
wvl_zooms = [[1.65, 1.75], [2.2, 2.3]]  # Wavelength zoom limits [H, K] (um)
i_ranges = [[-5e-14, 1.5e-13], [-4e-14, 1e-13]]  # Spectrum intensity limits
# [H, K] (erg/s/cm^2/um)

# Load data
print("\nProgram started")
my_stacks = [True, False]
sky = params_nifs.sky_compile(params_nifs.sky_c, params_nifs.sky_w[0])
pos_guess_mine_h = []
pos_guess_mine_k = []
star_psf_tmp = [i[0: 3] for i in star_psf]

for my_stack in my_stacks:
    if my_stack is True:
        str_my = "My stacks"
        save_my = 'mine'
    else:
        str_my = "Nifty stacks"
        save_my = 'nifty'

    print("- {0}".format(str_my))
    years_tmp = copy.deepcopy(years)

    if my_stack is True:
        years_tmp.insert(0, 0)

    for band in bands:
        print("   - {0} band".format(band))

        for year in years_tmp:
            if year == 0:
                str_year = "All epochs"
            else:
                str_year = str(year)

            title_str = "\n(" + str_year + ", " + band + " band)"
            file_list = []
            folder_in = ''
            folder = ''

            if year == 2017:
                folder = '084933a_17a/084933A/'

                if band == 'H':
                    file_list = ['20170407_obs57']
                else:
                    file_list = ['20170407_obs49', '20170410_obs71']
            elif year == 2019:
                folder = '084933a_19b/084933A/'

                if band == 'H':
                    file_list = []
                else:
                    file_list = ['20200103_obs66']

            elif year == 2021:
                folder = '084933a_21b/084933A/'

                if band == 'H':
                    file_list = []
                else:
                    file_list = ['20220224_obs86', '20220226_obs145']

            if (file_list == []) and (year != 0):
                continue

            print("      - {0}".format(str_year))

            if my_stack:
                if year == 0:
                    folder_in = path.join(root_folder, 'stack')
                    folder_files = [path.join(folder_in, ('stack_' + band +
                                                          '.fits'))]
                    folder_files_unc = [path.join(folder_in,
                                                  ('stack_' + band +
                                                   '_uncalib.fits'))]
                else:
                    folder_in = path.abspath(path.join(root_folder, folder,
                                                       pardir, 'stack'))
                    folder_files = glob.glob(path.join(folder_in,
                                                       ('stack_' + band +
                                                        '_?.fits')))
                    folder_files_unc = \
                        glob.glob(path.join(folder_in, ('stack_' + band +
                                                        '_?_uncalib.fits')))

            else:
                folder_files = [path.join(root_folder, folder,
                                          'Merged_telCorAndFluxCalibrated',
                                          (i + '_merged.fits')) for i in
                                file_list]
                folder_files_unc = [path.join(root_folder, folder,
                                              'Merged_telluricCorrected',
                                              (i + '_merged.fits')) for i in
                                    file_list]

            if band == 'H':
                wvl_zoom = wvl_zooms[0]
                i_range = i_ranges[0]
                stack_range = stack_range_list[0]
                sky_w = params_nifs.sky_w[0]
                slices = []
                slices_name = []

                if my_stack:
                    pos_guess = 'center'
                else:
                    pos_guess = pos_guess_mine_h
            else:
                wvl_zoom = wvl_zooms[1]
                i_range = i_ranges[1]
                stack_range = stack_range_list[1]
                sky_w = params_nifs.sky_w[1]
                slices = [[2.233, 2.237], [2.239, 2.245]]
                slices_name = [r"H$_{\alpha,n}$", r"[N II]$_{r}$"]

                if my_stack:
                    pos_guess = 'max'
                else:
                    pos_guess = pos_guess_mine_k

            try:
                year_index = years.index(year)
                pos_guesses_tmp = pos_guesses[year_index]
            except ValueError:
                pos_guesses_tmp = None

            for cube_n in range(len(folder_files)):

                # Choose position guess
                if pos_guesses_tmp:
                    pos_guess_tmp = pos_guesses_tmp[cube_n]
                    pos_guess = pos_guess_tmp[:: -1]

                # Create save folder
                spaces = ""

                if my_stack:
                    if year == 0:
                        save_folder = path.join(folder_in, 'plots', band)
                    else:
                        save_folder = path.join(
                            path.abspath(path.join(folder_in,
                                                   pardir)),
                            'plots', 'stack', band, save_my)
                else:
                    save_folder = path.abspath(path.join(root_folder, folder,
                                                         pardir, 'plots',
                                                         'stack',
                                                         band, save_my,
                                                         ('cube_' +
                                                          str(cube_n + 1))))
                    print("         - Observation #{0}".format(cube_n + 1))
                    spaces = "   "

                if not path.exists(save_folder):
                    makedirs(save_folder)

                # Load cube
                folder_file = folder_files[cube_n]
                file_split = re.split('_|.fits', folder_file)

                try:
                    file_split_int = int(file_split[-2])
                    file_suff = "_" + str(file_split_int)
                except ValueError:
                    file_split_int = np.nan
                    file_suff = ""

                folder_file_unc = folder_files_unc[cube_n]
                hdul = fits.open(folder_file)
                header0 = hdul[0].header
                header = hdul[1].header
                data = hdul[1].data
                hdul.close()
                hdul = fits.open(folder_file_unc)
                data_unc = hdul[1].data
                hdul.close()
                data_shape = data.shape
                sps = header['CD1_1']

                # Calculate wavelengths
                slice_idx = np.arange(data_shape[0])
                wvl = ((slice_idx + 1 - header['CRPIX3']) * header['CD3_3']) \
                    + header['CRVAL3']  # [ref1]
                wvl /= 10000
                wvl_ch = np.mean(wvl[1:] - wvl[: -1])

                # Collapse data
                _, data_slice = cube_nifs.cut_cube(wvl, data, stack_range[0],
                                                   stack_range[1])
                _, data_slice_unc = cube_nifs.cut_cube(wvl, data_unc,
                                                       stack_range[0],
                                                       stack_range[1])
                data_coll = cube_nifs.collapse_cube(data_slice)
                data_coll_unc = cube_nifs.collapse_cube(data_slice_unc)

                # Clean data
                fit_psf_m_tmp, _ = fit_nifs.moffat2d_fit(data_coll,
                                                         pos_guess=pos_guess)
                data_coll = cube_nifs.clean_map(data_coll, fit_psf_m_tmp[2: 4],
                                                ps=sps, ticks=sp_ticks,
                                                scale=(sps ** -2),
                                                save=path.join(
                                                    save_folder,
                                                    ('coll_filter' + file_suff
                                                     + '.png')))
                data_coll_unc = cube_nifs.clean_map(data_coll_unc,
                                                    fit_psf_m_tmp[2: 4])

                # Save collapsed image
                hdu0 = fits.PrimaryHDU(header=header0)
                hdu1 = fits.ImageHDU(data_coll)
                hdu1.header = header
                hdul = fits.HDUList([hdu0, hdu1])
                hdul.writeto((folder_file[:-5] + '_coll' + '.fits'),
                             overwrite=True)

                # Show collapsed image
                data_coll_plot = data_coll / (sps ** 2)
                cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                   c_label=("Flux (" +
                                            params_nifs.flux_spat_unit + ")"),
                                   perc=0.5, compass=True,
                                   title=("Collapsed cube" + title_str),
                                   save=path.join(save_folder,
                                                  ('coll' + file_suff +
                                                   '.png')))

                # Show collapsed image for printing
                cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                   c_label=("Flux (" +
                                            params_nifs.flux_spat_unit + ")"),
                                   perc=0.5, compass=True,
                                   save=path.join(save_folder,
                                                  ('print_coll' + file_suff +
                                                   '.png')))

                # Measure FWHM
                fit_psf_m, _ = fit_nifs.moffat2d_fit(data_coll,
                                                     pos_guess=pos_guess)
                target_x_m = fit_psf_m[2]
                target_y_m = fit_psf_m[3]

                if my_stack and (year == 0):
                    if band == 'H':
                        pos_guess_mine_h = [target_x_m, target_y_m]
                    else:
                        pos_guess_mine_k = [target_x_m, target_y_m]

                s_m = np.sqrt(fit_psf_m[4] * fit_psf_m[5])
                fwhms_m = 2 * np.array(fit_psf_m[4: 6]) * \
                    np.sqrt((2 ** (1 / fit_psf_m[7])) - 1) * sps
                fwhm_m = np.sqrt(fwhms_m[0] * fwhms_m[1])
                pa_m = fit_psf_m[6]

                # Show collapsed cube with fit
                cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                   model=[fit_psf_m[2], fit_psf_m[3],
                                          (fwhms_m[0] / sps),
                                          (fwhms_m[1] / sps),
                                          np.rad2deg(pa_m), fwhm_m],
                                   c_label=("Flux (" +
                                            params_nifs.flux_spat_unit + ")"),
                                   perc=0.5, compass=True,
                                   title=("Collapsed cube with FWHM" +
                                          title_str),
                                   save=path.join(save_folder,
                                                  ('coll_fwhm_m' + file_suff +
                                                   '.png')))

                # Show collapsed cube with fit for printing
                cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                   model=[fit_psf_m[2], fit_psf_m[3],
                                          (fwhms_m[0] / sps),
                                          (fwhms_m[1] / sps),
                                          np.rad2deg(pa_m), fwhm_m],
                                   c_label=("Flux (" +
                                            params_nifs.flux_spat_unit + ")"),
                                   perc=0.5, compass=True, legend=False,
                                   save=path.join(save_folder,
                                                  ('print_coll_fwhm_m' +
                                                   file_suff + '.png')))

                # Show radial profile with fit
                peak_plot = fit_psf_m[0] / (sps ** 2)
                bkg_plot = fit_psf_m[1] / (sps ** 2)
                cube_nifs.plot_rad(data_coll_plot, sps, fit_psf_m[2: 4],
                                   ("collapsed cube " + title_str),
                                   model_type='moffat',
                                   model_data=[peak_plot, s_m, fit_psf_m[7],
                                               bkg_plot, fwhm_m], zoom=0.5,
                                   y_label=("Flux (" +
                                            params_nifs.flux_spat_unit + ")"),
                                   save=path.join(save_folder,
                                                  ('rad_coll_m' + file_suff +
                                                   '.png')))

                # Measure the nearby star
                if my_stack and ([year, band, file_split_int] in star_psf_tmp):

                    # Find nearby star
                    star_psf_idx = star_psf_tmp.index([year, band,
                                                       file_split_int])
                    star_psf_xy = star_psf[star_psf_idx][3]

                    # Measure FWHM
                    fit_psf_star, _ = \
                        fit_nifs.moffat2d_fit(data_coll,
                                              pos_guess=[star_psf_xy[1],
                                                         star_psf_xy[0]])
                    s_star = np.sqrt(fit_psf_star[4] * fit_psf_star[5])
                    fwhms_star = 2 * np.array(fit_psf_star[4: 6]) * \
                        np.sqrt((2 ** (1 / fit_psf_star[7])) - 1) * sps
                    fwhm_star = np.sqrt(fwhms_star[0] * fwhms_star[1])
                    pa_star = fit_psf_star[6]

                    # Show collapsed cube with fit
                    cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                       model=[fit_psf_star[2], fit_psf_star[3],
                                              (fwhms_star[0] / sps),
                                              (fwhms_star[1] / sps),
                                              np.rad2deg(pa_star), fwhm_star],
                                       c_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       perc=0.5, compass=True,
                                       title=("Collapsed cube with star FWHM" +
                                              title_str),
                                       save=path.join(save_folder,
                                                      ('coll_fwhm_star' +
                                                       file_suff + '.png')))

                    # Show collapsed cube with fit for printing
                    cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                       model=[fit_psf_star[2], fit_psf_star[3],
                                              (fwhms_star[0] / sps),
                                              (fwhms_star[1] / sps),
                                              np.rad2deg(pa_star), fwhm_star],
                                       c_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       perc=0.5, compass=True, legend=False,
                                       save=path.join(save_folder,
                                                      ('print_coll_fwhm_star' +
                                                       file_suff + '.png')))

                    # Show collapsed cube with fit of both objects
                    cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                       model=[[fit_psf_m[2], fit_psf_m[3],
                                               (fwhms_m[0] / sps),
                                               (fwhms_m[1] / sps),
                                               np.rad2deg(pa_m), fwhm_m],
                                              [fit_psf_star[2],
                                               fit_psf_star[3],
                                               (fwhms_star[0] / sps),
                                               (fwhms_star[1] / sps),
                                               np.rad2deg(pa_star),
                                               fwhm_star]],
                                       c_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       perc=0.5, compass=True,
                                       title=(("Collapsed cube with target " +
                                               "and star FWHM") + title_str),
                                       save=path.join(save_folder,
                                                      ('coll_fwhm_m_star' +
                                                       file_suff + '.png')))

                    # Show collapsed cube with fit of both objects for printing
                    cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                       model=[[fit_psf_m[2], fit_psf_m[3],
                                               (fwhms_m[0] / sps),
                                               (fwhms_m[1] / sps),
                                               np.rad2deg(pa_m), fwhm_m],
                                              [fit_psf_star[2],
                                               fit_psf_star[3],
                                               (fwhms_star[0] / sps),
                                               (fwhms_star[1] / sps),
                                               np.rad2deg(pa_star),
                                               fwhm_star]],
                                       c_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       perc=0.5, compass=True, legend=False,
                                       save=path.join(save_folder,
                                                      ('print_coll_fwhm_m_star'
                                                       + file_suff + '.png')))

                    # Show radial profile with fit
                    peak_plot_star = fit_psf_star[0] / (sps ** 2)
                    bkg_plot_star = fit_psf_star[1] / (sps ** 2)
                    cube_nifs.plot_rad(data_coll_plot, sps, fit_psf_m[2: 4],
                                       ("collapsed cube " + title_str),
                                       center_2=fit_psf_star[2: 4],
                                       model_type='moffat',
                                       model_type_2='moffat',
                                       model_data=[peak_plot, s_m,
                                                   fit_psf_m[7], bkg_plot,
                                                   fwhm_m],
                                       model_data_2=[peak_plot_star, s_star,
                                                     fit_psf_star[7],
                                                     bkg_plot_star, fwhm_star],
                                       zoom=0.5,
                                       y_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       save=path.join(save_folder,
                                                      ('rad_coll_star' +
                                                       file_suff + '.png')))

                # Measure stacked SNR
                x_ticks_orig, y_ticks_orig, x_ticks_str, y_ticks_str = \
                    misc_girmos.ticks_scale(data_coll_unc.shape, sps, sp_ticks)
                x_0 = x_ticks_orig[np.where(np.array(x_ticks_str) ==
                                            '0.0')[0][0]]
                y_0 = y_ticks_orig[np.where(np.array(y_ticks_str) ==
                                            '0.0')[0][0]]
                bkg_pos_px = [(i / sps) for i in bkg_pos]
                bkg_pos_px[0] += x_0
                bkg_pos_px[1] += y_0
                r_snrs, snrs = \
                    cube_nifs.snrs(data_coll_unc, fit_psf_m[2: 4], bkg_pos_px)
                snrs_max = np.max(snrs)
                r_snrs_max = r_snrs[np.where(snrs == snrs_max)[0][0]] * sps
                r_snrs_diff = np.abs((r_snrs * sps) - (snr_d / 2))
                r_snrs_diff_idx = np.where(r_snrs_diff ==
                                           np.min(r_snrs_diff))[0][0]
                snrs_fix = snrs[r_snrs_diff_idx]
                print((spaces + "        Maximum collapsed SNR (d = " +
                       "{0:.2f}\"): {1:.1f}").format((r_snrs_max * 2),
                                                     snrs_max))
                print((spaces + "        Collapsed SNR (d = {0:.2f}\"): " +
                       "{1:.1f}").format(snr_d, snrs_fix))

                # Select beam
                direction = np.round(fit_psf_m[2: 4])
                beam = cube_nifs.beam(data, direction, (rad / sps))
                beam_bin = cube_nifs.bin_spx(data, beam[1], beam[0])

                # Show selected beam
                cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                                   direction=direction, beam_pos=beam,
                                   c_label=("Flux (" +
                                            params_nifs.flux_spat_unit + ")"),
                                   title=("Collapsed cube with beam" +
                                          title_str),
                                   perc=0.5, compass=True,
                                   save=path.join(save_folder,
                                                  ('coll_beam' + file_suff +
                                                   '.png')))

                # Show beam spectrum
                beam_bin_plot = beam_bin / wvl_ch
                cube_nifs.plot_spectrum(wvl, beam_bin_plot, sky=sky,
                                        i_range=i_range,
                                        y_label=("Flux density (" +
                                                 params_nifs.flux_dens_unit +
                                                 ")"),
                                        title=("Beam spectrum" + title_str),
                                        save=path.join(save_folder,
                                                       ('beam_spectr' +
                                                        file_suff + '.png')))

                # Show beam spectrum zoomed
                cube_nifs.plot_spectrum(wvl, beam_bin_plot, sky=sky,
                                        zoom=wvl_zoom, i_range=i_range,
                                        y_label=("Flux density (" +
                                                 params_nifs.flux_dens_unit +
                                                 ")"),
                                        title=("Beam spectrum zoomed" +
                                               title_str),
                                        save=path.join(save_folder,
                                                       ('beam_spectr_zoom' +
                                                        file_suff + '.png')))

                # Slice the cube
                slc = [[]] * len(slices)

                for i_slice in range(len(slices)):
                    _, slc[i_slice] = cube_nifs.cut_cube(wvl, data,
                                                         slices[i_slice][0],
                                                         slices[i_slice][1])

                # Collapse the slices
                for i_slice in range(len(slices)):
                    coll_slc = cube_nifs.collapse_cube(slc[i_slice])
                    title_slice_str = title_str[:-1] + ", " + \
                        slices_name[i_slice] + " line)"

                    # Show collapsed slice
                    coll_slc_plot = coll_slc / (sps ** 2)
                    cube_nifs.plot_img(coll_slc_plot, sps, sp_ticks,
                                       c_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       title=("Collapsed cube" +
                                              title_slice_str), perc=0.5,
                                       compass=True,
                                       save=path.join(save_folder,
                                                      ('coll_line' +
                                                       str(i_slice + 1) +
                                                       file_suff + '.png')))

                    # Measure FWHM of collapsed slice
                    if year == 2021 and not my_stack:
                        iterate = False
                    else:
                        iterate = True

                    fit_psf_m_slice, _ = \
                        fit_nifs.moffat2d_fit(coll_slc, pos_guess=pos_guess,
                                              iterate=iterate)
                    target_x_m_slice = fit_psf_m_slice[2]
                    target_y_m_slice = fit_psf_m_slice[3]
                    s_m_slice = np.sqrt(fit_psf_m_slice[4] *
                                        fit_psf_m_slice[5])
                    fwhms_m_slice = 2 * np.array(fit_psf_m_slice[4: 6]) * \
                        np.sqrt((2 ** (1 / fit_psf_m_slice[7])) - 1) * sps
                    fwhm_m_slice = np.sqrt(fwhms_m_slice[0] * fwhms_m_slice[1])
                    pa_m_slice = fit_psf_m_slice[6]

                    # Show collapsed slice cube with fit
                    cube_nifs.plot_img(coll_slc_plot, sps, sp_ticks,
                                       model=[fit_psf_m_slice[2],
                                              fit_psf_m_slice[3],
                                              (fwhms_m_slice[0] / sps),
                                              (fwhms_m_slice[1] / sps),
                                              np.rad2deg(pa_m_slice),
                                              fwhm_m_slice],
                                       c_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       title=("Collapsed cube with FWHM" +
                                              title_slice_str), perc=0.5,
                                       compass=True,
                                       save=path.join(save_folder,
                                                      ('coll_fwhm_m_line' +
                                                       str(i_slice + 1) +
                                                       file_suff + '.png')))

                    # Show radial profile of collapsed slice with fit
                    peak_slice_plot = fit_psf_m_slice[0] / (sps ** 2)
                    bkg_slice_plot = fit_psf_m_slice[1] / (sps ** 2)
                    cube_nifs.plot_rad(coll_slc_plot, sps,
                                       fit_psf_m_slice[2: 4],
                                       ("collapsed cube " + title_slice_str),
                                       model_type='moffat',
                                       model_data=[peak_slice_plot, s_m_slice,
                                                   fit_psf_m_slice[7],
                                                   bkg_slice_plot,
                                                   fwhm_m_slice],
                                       y_label=("Flux (" +
                                                params_nifs.flux_spat_unit +
                                                ")"),
                                       zoom=0.5,
                                       save=path.join(save_folder,
                                                      ('rad_coll_m_line' +
                                                       str(i_slice + 1) +
                                                       file_suff + '.png')))

print("Program terminated\n")
