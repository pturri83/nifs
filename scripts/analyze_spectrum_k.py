"""Analyze a NIFS K band spectrum of 084933A.

References:
    [ref1] Greisen et Calabretta, 2002
    [ref2] Ivison et al., 2019
"""


import copy
from os import makedirs, path
import pickle

from astropy.io import fits
import itertools
import numpy as np

from source import cube_nifs, fit_nifs, params_nifs


# Data parameters
data_f = '/Volumes/Astro/Data/NIFS/stack/stack_K.fits'  # Data folder and file
plot_p = '/Volumes/Astro/Data/NIFS/stack/plots/K'  # Plots parent folder

# Analysis parameters
rad_m = 0.5  # Radius of the main beam (arcsec)
rad_s = 0.5  # Radius of a secondary beam (arcsec)
rad_phys = 0.45  # Radius of the beam to calculate the physical properties
# (arcsec)
side_sq = 0.6  # Side of a square beam (arcsec)
k_range = [2.1, 2.35]  # Range of wavelengths to fit and stack and plot (um)
bin_wvl = 3  # Wavelength binning factor
bin_spxs = [2, 3, 4]  # Spaxel binning factors
conv_spx = 2  # Radius of the convolution kernel (px)
n_slices = 24  # Number of slices in azimuthal profiles
dist_range = [0.5, 1]  # Range of distances in radial and azimuthal profiles
# ([min, max]) (")
dist_range_sub = [0.2, 1]  # Range of distances in subtracted azimuthal
# profiles ([min, max]) (arcsec)
wvl_range_cont = [2.1, 2.15]  # Range of wavelengths to measure the continuum
# (min, max) (um)
avg_step = 0.1  # Smallest distance step size to calculate the average in the
# radial plots of the quadrants (arcsec)

# Graphic parameters
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (arcsec)
i_range = [-5e-15, 7.5e-14]  # Spectrum intensity limits of main and secondary
# beams (erg/s/cm^2/um)
i_range_sec = [-1e-14, 2e-14]  # Spectrum intensity limits of secondary only
# beams (erg/s/cm^2/um)
i_range_res = [-1e-14, 1.5e-14]  # Residual spectrum intensity limits of main
# and secondary beams (erg/s/cm^2/um)
k_range_plot = [2.1, 2.35]  # Range of wavelengths to print the main beam (um)
k_range_grid = [2.21, 2.27]  # Range of wavelengths for the grid of spectra
# (um)
k_range_grid_sq = [2.22, 2.26]  # Range of wavelengths for the grid of spectra
# of square beams (um)
spectrum_grid_side = 7  # Number of spectra to show on the side of the grid
mask_vel = [4e-19, 5e-19]  # Minimum narrow H_a line flux to plot its velocity
# (not binned, binned) (erg/s/cm^2/um)
flux_dens_range_az = [0, 7e-15]  # Range of flux densities in azimuthal plots
# (min, max) (erg/s/cm^2/um/"^2)
flux_range_az_b = [-1e-15, 1.5e-15]  # Range of broad fluxes in azimuthal plots
# (min, max) (erg/s/cm^2/"^2)
flux_range_az_n = [-1e-16, 2e-16]  # Range of narrow fluxes in azimuthal plots
# (min, max) (erg/s/cm^2/"^2)
flux_dens_range_az_sub = [-1e-15, 1e-15]  # Range of flux densities in
# subtracted azimuthal plots (min, max) (erg/s/cm^2/um/"^2)
flux_range_az_b_sub = [-1e-16, 1.5e-16]  # Range of broad fluxes in subtracted
# azimuthal plots (erg/s/cm^2/"^2)
flux_range_az_n_sub = [-1e-17, 2e-17]  # Range of narrow fluxes in subtracted
# azimuthal plots (min, max) (erg/s/cm^2/"^2)
flux_dens_range_az_bin = [-3e-15, 6e-15]  # Range of flux densities in binned
# azimuthal plots (min, max) (erg/s/cm^2/um/"^2)
flux_range_az_bin_b = [-1e-15, 2e-15]  # Range of broad fluxes in binned
# azimuthal plots (min, max) (erg/s/cm^2/"^2)
flux_range_az_bin_n = [-4e-17, 1e-16]  # Range of narrow fluxes in binned
# azimuthal plots (min, max) (erg/s/cm^2/"^2)
bin_grids = 8  # Side of grid of contiguous spectra
i_range_grid = [-5e-16, 3.5e-15]  # Spectrum flux density limits for the grid
# of spectra (erg/s/cm^2/um)
i_range_grid_sq = [[1e-14, 5.5e-14], [-5e-15, 2e-14]]  # Spectrum flux density
# limits for the grid of spectra of square beams ([central, others])
# (erg/s/cm^2/um)
k_range_s = [2.2, 2.32]  # Range of wavelengths to print secondary beams (um)
exclude_fit_grid_sq = [[0, 0], [0, 1], [0, 2], [1, 0]]  # Index of the plots in
# the grid of spectra of square beams that are not plotted with the fit
# ([row, column])
max_r = 1  # Maximum distance plotted in radial profiles (")

# Manage files
print("\nProgram started")
plot_f = path.join(plot_p, 'spectrum')
fits_f = path.join(plot_p, 'spectrum', 'fits')

if not path.exists(plot_f):
    makedirs(plot_f)

if not path.exists(fits_f):
    makedirs(fits_f)

# Load data
print("Preparing data ...")
hdul = fits.open(data_f)
header = hdul[1].header
data = hdul[1].data
hdul.close()
data_shape = data.shape
sps = header['CD1_1']

# Calculate wavelengths
slice_idx = np.arange(data_shape[0])
wvl = ((slice_idx + 1 - header['CRPIX3']) * header['CD3_3']) + header['CRVAL3']
# [ref1]
wvl /= 10000
sky = params_nifs.sky_compile(params_nifs.sky_c, params_nifs.sky_w[0])

# Integrate flux density
wvl_steps = wvl[1:] - wvl[:-1]
wvl_step = np.mean(wvl_steps)
data /= wvl_step

# Clean data
wvl_clean, data_clean = cube_nifs.cut_cube(wvl, data, k_range[0], k_range[1])
wvl_clean, data_clean = cube_nifs.clean_cube(wvl_clean, data_clean,
                                             params_nifs.sky_c,
                                             params_nifs.sky_w[1])

# Select beams
data_coll = cube_nifs.collapse_cube(data_clean)
data_coll *= np.max(wvl_clean) - np.min(wvl_clean)
fit_psf_m_tmp, _ = fit_nifs.moffat2d_fit(data_coll, pos_guess='max')
data_coll = cube_nifs.clean_map(data_coll, fit_psf_m_tmp[2: 4])
fit_psf_m, _ = fit_nifs.moffat2d_fit(data_coll, pos_guess='max')
direction_tmp = fit_psf_m[2: 4]
direction_m = [direction_tmp[0], direction_tmp[1], (rad_m / sps)]
beam_m = cube_nifs.beam(data_clean, direction_m[0: -1], direction_m[2])
beam_bin_m = cube_nifs.bin_spx(data_clean, beam_m[1], beam_m[0])
beam_bin_m_plot = cube_nifs.bin_spx(data, beam_m[1], beam_m[0])
direction_s = np.zeros([4, 3])
direction_offset = [[-1, -1], [-1, 1], [1, -1], [1, 1]]
direction_label = ["Center", "SW", "NW", "SE", "NE"]
direction_suffix = ['c', 'sw', 'nw', 'se', 'ne']
beam_s = [np.array([])] * 4
beam_bin_s = [np.array([])] * 4
beam_bin_s_plot = [np.array([])] * 4
dist_s = (rad_m + rad_s) / np.sqrt(2)

for i_dir in range(len(direction_offset)):
    direction_s[i_dir, 0] = (direction_tmp[0] +
                             (direction_offset[i_dir][0] * dist_s / sps))
    direction_s[i_dir, 1] = (direction_tmp[1] +
                             (direction_offset[i_dir][1] * dist_s / sps))
    direction_s[i_dir, 2] = rad_s / sps
    beam_s[i_dir] = cube_nifs.beam(data_clean, direction_s[i_dir, :-1],
                                   direction_s[i_dir, 2])
    beam_bin_s[i_dir] = cube_nifs.bin_spx(data_clean, beam_s[i_dir][0],
                                          beam_s[i_dir][1])
    beam_bin_s_plot[i_dir] = cube_nifs.bin_spx(data, beam_s[i_dir][0],
                                               beam_s[i_dir][1])

beam = copy.copy(beam_s)
beam.insert(0, beam_m)
beam_circ = np.vstack([direction_m, direction_s])
beam_bin = np.vstack([beam_bin_m, beam_bin_s])
beam_bin_plot = np.vstack([beam_bin_m_plot, beam_bin_s_plot])

# Show beams
data_coll_plot = data_coll / (sps ** 2)
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, beam_circ=beam_circ,
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   title="Collapsed cube with beams",
                   save=path.join(plot_f, 'coll_beams.png'))

# Show beams for printing
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, beam_circ=beam_circ,
                   ax_labels=False, c_bar=False, perc=0.5,
                   save=path.join(plot_f, 'print_coll_beams.png'))

# Fit the main beam
print("\nAnalyzing primary beam ...\n")
k_fit_m = fit_nifs.fit_k(wvl_clean, beam_bin[0], wvl_range_cont)

# Show the main beam spectrum zoomed, with fitted spectrum
cube_nifs.plot_spectrum(wvl, beam_bin_plot[0], sky=sky,
                        fit_mod=k_fit_m, zoom=k_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        title="Beam spectrum fitted ({0})".format(
                            direction_label[0]),
                        save=path.join(plot_f,
                                       ('beam_spectr_fit_' +
                                        direction_suffix[0] + '.png')))

# Show the main beam spectrum zoomed, with fitted spectrum, for printing
cube_nifs.plot_spectrum(wvl, beam_bin_plot[0], sky=sky,
                        fit_mod=k_fit_m, zoom=k_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       ('print_beam_spectr_fit_' +
                                        direction_suffix[0] + '.png')))

# Bin the main beam spectrum by wavelength
wvl_bin2_m_plot, beam_bin2_m_plot = cube_nifs.bin_spectrum(wvl,
                                                           beam_bin_plot[0],
                                                           bin_wvl)

# Show the main beam spectrum zoomed, binned by wavelength, with fitted
# spectrum
cube_nifs.plot_spectrum(wvl_bin2_m_plot, beam_bin2_m_plot, sky=sky,
                        fit_mod=k_fit_m, zoom=k_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        title="Binned beam spectrum fitted ({0})".format(
                            direction_label[0]),
                        save=path.join(plot_f,
                                       ('beam_spectr_fit_' +
                                        direction_suffix[0] + '_bin.png')))

# Analyze secondary beams
for i_dir in range(1, beam_circ.shape[0]):

    # Prepare secondary beam spectrum for fitting
    print("\rAnalyzing secondary beams ({0}/{1}) ...".format(
        i_dir, (beam_circ.shape[0] - 1)), end="")

    # Fit the secondary beam
    k_fit_s = fit_nifs.fit_k(wvl_clean, beam_bin[i_dir], wvl_range_cont)

    # Show the secondary beam spectrum zoomed, with fitted spectrum
    cube_nifs.plot_spectrum(wvl, beam_bin_plot[i_dir], sky=sky,
                            fit_mod=k_fit_s, zoom=k_range_plot,
                            i_range=i_range, i_range_res=i_range_res,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            title="Beam spectrum fitted ({0})".format(
                                direction_label[i_dir]),
                            save=path.join(plot_f,
                                           ('beam_spectr_fit_' +
                                            direction_suffix[i_dir] + '.png')))

    # Bin the secondary beam spectrum by wavelength
    wvl_bin2_s_plot, beam_bin2_s_plot = \
        cube_nifs.bin_spectrum(wvl, beam_bin_plot[i_dir], bin_wvl)

    # Show the secondary beam spectrum zoomed, binned by wavelength, with
    # fitted spectrum
    cube_nifs.plot_spectrum(wvl_bin2_s_plot, beam_bin2_s_plot, sky=sky,
                            fit_mod=k_fit_s, zoom=k_range_plot,
                            i_range=i_range, i_range_res=i_range_res,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            title="Binned beam spectrum fitted ({0})".format(
                                direction_label[i_dir]),
                            save=path.join(plot_f,
                                           ('beam_spectr_fit_' +
                                            direction_suffix[i_dir] +
                                            '_bin.png')))

    # Show the secondary beam spectrum zoomed, binned by wavelength, with
    # fitted spectrum, for printing
    cube_nifs.plot_spectrum(wvl_bin2_s_plot, beam_bin2_s_plot, sky=sky,
                            fit_mod=k_fit_s, zoom=k_range_s,
                            i_range=i_range_sec, i_range_res=i_range_res,
                            show_lines=None,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            save=path.join(plot_f,
                                           ('print_beam_spectr_fit_' +
                                            direction_suffix[i_dir] +
                                            '_bin.png')))

print("\n")

# Prepare square beams
grid_sq_idx = np.mgrid[-1: 2, -1: 2]
direction_sq = np.zeros([9, 3])
direction_sq_label = ["SW", "S", "SE", "W", "Center", "E", "NW", "N", "NE"]
direction_sq_suffix = ['sw', 's', 'se', 'w', 'c', 'e', 'nw', 'n', 'ne']
vel_arr = np.zeros([3, 3])
vel_arr_han = np.zeros([3, 3])
grid_sq = np.zeros([len(wvl), 3, 3])
maps_grid_sq = np.tile(np.array(fit_nifs.k_prof()), [3, 3])
maps_grid_sq_han = np.tile(np.array(fit_nifs.k_prof()), [3, 3])

# Analyze square beams
for i_dir in range(0, 9):

    # Prepare square beam spectrum for fitting
    print("\rAnalyzing square beams ({0}/9) ...".format(i_dir + 1), end="")
    grid_x = grid_sq_idx[1].flatten()[i_dir]
    grid_y = grid_sq_idx[0].flatten()[i_dir]
    direction_sq[i_dir, 0] = (direction_tmp[0] + (grid_x * side_sq / sps))
    direction_sq[i_dir, 1] = (direction_tmp[1] + (grid_y * side_sq / sps))
    direction_sq[i_dir, 2] = side_sq / sps
    beam_sq = cube_nifs.beam_sq(data_clean, direction_sq[i_dir, :2],
                                direction_sq[i_dir, 2])
    beam_bin_sq = cube_nifs.bin_spx(data_clean, beam_sq[0], beam_sq[1])
    beam_bin_sq_plot = cube_nifs.bin_spx(data, beam_sq[0], beam_sq[1])
    grid_sq[:, (grid_y + 1), (grid_x + 1)] = beam_bin_sq_plot

    # Save square beam spectrum
    wvl_col = fits.Column(name='wvl', array=wvl_clean, format='F', unit='um')
    spectr_col = fits.Column(name='spectrum', array=beam_bin_sq, format='F',
                             unit='erg/s/cm^2/um')
    spectr_cols = fits.ColDefs([wvl_col, spectr_col])
    hdu_spectr = fits.BinTableHDU.from_columns(spectr_cols)
    hdu_spectr.writeto(
        path.join(fits_f,
                  'spectr_sq_{0}.fits'.format(direction_sq_suffix[i_dir])),
        overwrite=True)

    # Fit the square beam
    if [(grid_y + 1), (grid_x + 1)] in exclude_fit_grid_sq:
        k_fit_sq = None
    else:
        k_fit_sq = fit_nifs.fit_k(wvl_clean, beam_bin_sq, wvl_range_cont)

    k_fit_sq_han = fit_nifs.fit_k(wvl_clean, beam_bin_sq, wvl_range_cont,
                                  bb_slope=False, only_han=True, iterate=False)
    maps_grid_sq[(grid_y + 1), (grid_x + 1)] = k_fit_sq
    maps_grid_sq_han[(grid_y + 1), (grid_x + 1)] = k_fit_sq_han

    # Show the square beam spectrum zoomed, with fitted spectrum
    cube_nifs.plot_spectrum(wvl, beam_bin_sq_plot, sky=sky, fit_mod=k_fit_sq,
                            zoom=k_range_plot, i_range=i_range,
                            i_range_res=i_range_res,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            title=("Squared beam spectrum fitted " +
                                   "({0})").format(direction_sq_label[i_dir]),
                            save=path.join(plot_f,
                                           ('beam_sq_spectr_fit_' +
                                            direction_sq_suffix[i_dir] +
                                            '.png')))

    # Measure the square beam velocity
    if [(grid_y + 1), (grid_x + 1)] not in exclude_fit_grid_sq:
        _, _, _, v_n, _, _, _, _, _ = fit_nifs.phys_lines_k(k_fit_sq)
        vel_arr[(grid_y + 1), (grid_x + 1)] = v_n

    _, _, _, v_n_han, _, _, _, _, _ = fit_nifs.phys_lines_k(k_fit_sq_han)
    vel_arr_han[(grid_y + 1), (grid_x + 1)] = v_n_han

print()

# Print and save beam velocities
vel_ref = vel_arr[1, 1]
vel_ref_han = vel_arr_han[1, 1]
vel_arr -= vel_ref
vel_arr_han -= vel_ref_han
line_sq_0 = "Velocity of the square beams:"
print(line_sq_0)
file_txt = open(path.join(plot_f, 'vel_sq.txt'), 'w')
file_txt_han = open(path.join(plot_f, 'vel_sq_han.txt'), 'w')
file_txt.write(line_sq_0 + "\n")
file_txt_han.write(line_sq_0 + "\n")

for i_dir in range(0, 9):
    grid_x = grid_sq_idx[1].flatten()[i_dir] + 1
    grid_y = grid_sq_idx[0].flatten()[i_dir] + 1

    if [grid_y, grid_x] not in exclude_fit_grid_sq:
        line_sq = "   {0:7}: {1:6.1f} km/s".format(direction_sq_label[i_dir],
                                                   (vel_arr[grid_y, grid_x] *
                                                    1e-3))
        file_txt.write(line_sq + "\n")

    line_sq_han = "   {0:7}: {1:6.1f} km/s".format(direction_sq_label[i_dir],
                                                   (vel_arr_han[grid_y, grid_x]
                                                    * 1e-3))
    file_txt_han.write(line_sq_han + "\n")

file_txt.close()
file_txt_han.close()

# Show square beams
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, beam_squ=direction_sq,
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   title="Collapsed cube with square beams",
                   save=path.join(plot_f, 'coll_beams_sq.png'))

# Show square beams for printing
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, beam_squ=direction_sq,
                   c_bar=False, perc=0.5, compass=True,
                   save=path.join(plot_f, 'print_coll_beams_sq.png'))

# Plot square beam velocities
cube_nifs.plot_img((vel_arr * 1e-3), dist_s, sp_ticks,
                   c_label="Velocity (km/s)", compass=True,
                   save=path.join(plot_f, 'beams_los.png'))
cube_nifs.plot_img((vel_arr_han * 1e-3), dist_s, sp_ticks,
                   c_label="Velocity (km/s)", compass=True,
                   save=path.join(plot_f, 'beams_los_han.png'))

# Show grid of square beam spectra
cube_nifs.grid_spectrum(wvl, grid_sq, sky=sky, fit_mod=maps_grid_sq,
                        exclude_fit=exclude_fit_grid_sq, print_han=vel_ref,
                        show_lines='single', zoom=k_range_grid_sq,
                        i_range=i_range_grid_sq,
                        title="Grid of fitted spectra of square beams",
                        compass=True,
                        save=path.join(plot_f, 'grid_spectr_beams_sq.png'))
cube_nifs.grid_spectrum(wvl, grid_sq, sky=sky, fit_mod=maps_grid_sq_han,
                        print_han=vel_ref_han, show_lines='single',
                        zoom=k_range_grid_sq, i_range=i_range_grid_sq,
                        title="Grid of fitted spectra of square beams",
                        compass=True,
                        save=path.join(plot_f, 'grid_spectr_beams_sq_han.png'))

# Show grid of square beam spectra for printing
cube_nifs.grid_spectrum(wvl, grid_sq, sky=sky, fit_mod=maps_grid_sq,
                        exclude_fit=exclude_fit_grid_sq, print_han=vel_ref,
                        show_lines='single', zoom=k_range_grid_sq,
                        i_range=i_range_grid_sq, compass=True,
                        save=path.join(plot_f,
                                       'print_grid_spectr_beams_sq.png'))
cube_nifs.grid_spectrum(wvl, grid_sq, sky=sky, fit_mod=maps_grid_sq_han,
                        print_han=vel_ref_han, show_lines='single',
                        zoom=k_range_grid_sq, i_range=i_range_grid_sq,
                        compass=True,
                        save=path.join(plot_f,
                                       'print_grid_spectr_beams_sq_han.png'))

# Show physical beam
print("\nAnalyzing physical beam ...")
direction_phys = np.array(direction_m)
direction_phys[2] = rad_phys / sps
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                   beam_circ=np.expand_dims(direction_phys, 0),
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   title="Collapsed cube with beam",
                   save=path.join(plot_f, 'coll_beams_phys.png'))

# Show physical beam for printing
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                   beam_circ=np.expand_dims(direction_phys, 0),
                   ax_labels=False, c_bar=False, perc=0.5,
                   save=path.join(plot_f, 'print_coll_beams_phys.png'))

# Show the physical beam spectrum zoomed, with fitted spectrum
beam_phys = cube_nifs.beam(data_clean, direction_phys[0: -1],
                           direction_phys[2])
beam_bin_phys = cube_nifs.bin_spx(data_clean, beam_phys[1], beam_phys[0])
beam_bin_phys_plot = cube_nifs.bin_spx(data, beam_phys[1], beam_phys[0])
k_fit_phys = fit_nifs.fit_k(wvl_clean, beam_bin_phys, wvl_range_cont)
cube_nifs.plot_spectrum(wvl, beam_bin_phys_plot, sky=sky, fit_mod=k_fit_phys,
                        zoom=k_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"), title="Beam spectrum fitted",
                        save=path.join(plot_f, 'beam_spectr_fit_phys.png'))

# Show the physical beam spectrum zoomed, with fitted spectrum, for printing
cube_nifs.plot_spectrum(wvl, beam_bin_phys_plot, sky=sky, fit_mod=k_fit_phys,
                        zoom=k_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       'print_beam_spectr_fit_phys.png'))

# Bin the physical beam spectrum by wavelength
wvl_bin_phys, beam_bin_phys = cube_nifs.bin_spectrum(wvl, beam_bin_phys_plot,
                                                     2)

# Show the physical beam spectrum zoomed, binned by wavelength, with fitted
# spectrum, for printing
cube_nifs.plot_spectrum(wvl_bin_phys, beam_bin_phys, sky=sky,
                        fit_mod=k_fit_phys, zoom=k_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       'print_beam_spectr_fit_phys_bin.png'))

# Calculate the physical beam physical characteristics
cont_phys = k_fit_phys.cont.value
hab_i_phys = k_fit_phys.hab_i.value
han_i_phys = k_fit_phys.han_i.value
n2_i_phys = k_fit_phys.n2_i.value
s2_i_phys = k_fit_phys.s2_i.value
z_phys, d_a_phys, v_hab_phys, v_n_phys, s_hab_phys, s_n_phys, fwhm_hab_phys, \
    fwhm_n_phys, m_bh_phys = fit_nifs.phys_lines_k(k_fit_phys)
dv_phys = v_hab_phys - v_n_phys

# Show the physical beam physical characteristics
line1 = "   Redshift: {0:.4f}".format(z_phys)
line2 = ("   LOS velocity difference between broad and narrow H_a: {0:.0f} " +
         "km/s").format(dv_phys * 1e-3)
line3 = "   LOS velocity dispersion of broad H_a: {0:.0f} km/s".format(
    s_hab_phys * 1e-3)
line4 = "   LOS velocity dispersion of narrow lines: {0:.0f} km/s".format(
    s_n_phys * 1e-3)
line5 = "   LOS velocity FWHM of broad H_a: {0:.0f} km/s".format(fwhm_hab_phys
                                                                 * 1e-3)
line6 = "   LOS velocity FWHM of narrow lines: {0:.0f} km/s".format(fwhm_n_phys
                                                                    * 1e-3)
line7 = "   Continuum density: {0:.3e} erg/s/cm^2/um".format(cont_phys)
line8 = "   Broad H_a integral: {0:.3e} erg/s/cm^2".format(hab_i_phys)
line9 = "   Narrow H_a integral: {0:.3e} erg/s/cm^2".format(han_i_phys)
line10 = "   [NII] integral: {0:.3e} erg/s/cm^2".format(n2_i_phys)
line11 = "   [SII] integral: {0:.3e} erg/s/cm^2".format(s2_i_phys)
line12 = "   Angular scale: {0:.3f} kpc/\"".format(d_a_phys)
line13 = "   Angular scale: {0:.4f} \"/kpc".format(1 / d_a_phys)
line14 = "   SMBH mass: {0:.3f} x 10^9 M_Sun".format(m_bh_phys * 1e-9)
print(line1)
print(line2)
print(line3)
print(line4)
print(line5)
print(line6)
print(line7)
print(line8)
print(line9)
print(line10)
print(line11)
print(line12)
print(line13)
print(line14)

# Save the physical beam spectrum characteristics
file_txt = open(path.join(plot_f, 'spectr_phys.txt'), 'w')
file_txt.write(line1 + "\n")
file_txt.write(line2 + "\n")
file_txt.write(line3 + "\n")
file_txt.write(line4 + "\n")
file_txt.write(line5 + "\n")
file_txt.write(line6 + "\n")
file_txt.write(line7 + "\n")
file_txt.write(line8 + "\n")
file_txt.write(line9 + "\n")
file_txt.write(line10 + "\n")
file_txt.write(line11 + "\n")
file_txt.write(line12 + "\n")
file_txt.write(line13 + "\n")
file_txt.write(line14 + "\n")
file_txt.close()

# Show Ivison beam
print("\nAnalyzing Ivison beam ...")
direction_ivison = np.array(direction_m)
direction_ivison[2] = params_nifs.rad_cal / sps
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                   beam_circ=np.expand_dims(direction_ivison, 0),
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   title="Collapsed cube with beam",
                   save=path.join(plot_f, 'coll_beams_ivison.png'))

# Show Ivison beam for printing
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                   beam_circ=np.expand_dims(direction_ivison, 0),
                   ax_labels=False, c_bar=False, perc=0.5,
                   save=path.join(plot_f, 'print_coll_beams_ivison.png'))

# Show the Ivison beam spectrum zoomed, with fitted spectrum
beam_ivison = cube_nifs.beam(data_clean, direction_ivison[0: -1],
                             direction_ivison[2])
beam_bin_ivison = cube_nifs.bin_spx(data_clean, beam_ivison[1], beam_ivison[0])
beam_bin_ivison_plot = cube_nifs.bin_spx(data, beam_ivison[1], beam_ivison[0])
k_fit_ivison = fit_nifs.fit_k(wvl_clean, beam_bin_ivison, wvl_range_cont)
cube_nifs.plot_spectrum(wvl, beam_bin_ivison_plot, sky=sky,
                        fit_mod=k_fit_ivison, zoom=k_range_plot,
                        i_range=i_range, i_range_res=i_range_res,
                        label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"), title="Beam spectrum fitted",
                        save=path.join(plot_f, 'beam_spectr_fit_ivison.png'))

# Show the Ivison beam spectrum zoomed, with fitted spectrum, for printing
cube_nifs.plot_spectrum(wvl, beam_bin_ivison_plot, sky=sky,
                        fit_mod=k_fit_ivison, zoom=k_range_plot,
                        i_range=i_range, i_range_res=i_range_res,
                        label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       'print_beam_spectr_fit_ivison.png'))

# Calculate the Ivison beam physical characteristics
cont_ivison = k_fit_ivison.cont.value
hab_i_ivison = k_fit_ivison.hab_i.value
han_i_ivison = k_fit_ivison.han_i.value
n2_i_ivison = k_fit_ivison.n2_i.value
s2_i_ivison = k_fit_ivison.s2_i.value
z_ivison, d_a_ivison, v_hab_ivison, v_n_ivison, s_hab_ivison, s_n_ivison, \
    fwhm_hab_ivison, fwhm_n_ivison, m_bh_ivison = \
    fit_nifs.phys_lines_k(k_fit_ivison)
dv_ivison = v_hab_ivison - v_n_ivison

# Show the Ivison beam physical characteristics
line1 = "   Redshift: {0:.4f}".format(z_ivison)
line2 = ("   LOS velocity difference between broad and narrow H_a: {0:.0f} " +
         "km/s").format(dv_ivison * 1e-3)
line3 = "   LOS velocity dispersion of broad H_a: {0:.0f} km/s".format(
    s_hab_ivison * 1e-3)
line4 = "   LOS velocity dispersion of narrow lines: {0:.0f} km/s".format(
    s_n_ivison * 1e-3)
line5 = "   LOS velocity FWHM of broad H_a: {0:.0f} km/s".format(
    fwhm_hab_ivison * 1e-3)
line6 = "   LOS velocity FWHM of narrow lines: {0:.0f} km/s".format(
    fwhm_n_ivison * 1e-3)
line7 = "   Continuum density: {0:.3e} erg/s/cm^2/um".format(cont_ivison)
line8 = "   Broad H_a integral: {0:.3e} erg/s/cm^2".format(hab_i_ivison)
line9 = "   Narrow H_a integral: {0:.3e} erg/s/cm^2".format(han_i_ivison)
line10 = "   [NII] integral: {0:.3e} erg/s/cm^2".format(n2_i_ivison)
line11 = "   [SII] integral: {0:.3e} erg/s/cm^2".format(s2_i_ivison)
line12 = "   Angular scale: {0:.3f} kpc/\"".format(d_a_ivison)
line13 = "   Angular scale: {0:.4f} \"/kpc".format(1 / d_a_ivison)
line14 = "   SMBH mass: {0:.3f} x 10^9 M_Sun".format(m_bh_ivison * 1e-9)
print(line1)
print(line2)
print(line3)
print(line4)
print(line5)
print(line6)
print(line7)
print(line8)
print(line9)
print(line10)
print(line11)
print(line12)
print(line13)
print(line14)

# Save the Ivison beam spectrum characteristics
file_txt = open(path.join(plot_f, 'spectr_ivison.txt'), 'w')
file_txt.write(line1 + "\n")
file_txt.write(line2 + "\n")
file_txt.write(line3 + "\n")
file_txt.write(line4 + "\n")
file_txt.write(line5 + "\n")
file_txt.write(line6 + "\n")
file_txt.write(line7 + "\n")
file_txt.write(line8 + "\n")
file_txt.write(line9 + "\n")
file_txt.write(line10 + "\n")
file_txt.write(line11 + "\n")
file_txt.write(line12 + "\n")
file_txt.write(line13 + "\n")
file_txt.write(line14 + "\n")
file_txt.close()

# Map lines
print("\nAnalyzing maps ...")
maps = fit_nifs.fit_map_k(wvl_clean, data_clean, wvl_range_cont, n_spaces=3)

# Show uncleaned maps
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont',
                   save=path.join(plot_f, 'map_cont_unclean.png'))

# Clean maps
print("Cleaning maps ...")
map_cont_tmp, _ = cube_nifs.grab_map_k(maps, 'cont')
fit_psf_cont_tmp, _ = fit_nifs.moffat2d_fit(map_cont_tmp, pos_guess='center')
maps = cube_nifs.clean_maps(maps, fit_psf_cont_tmp[2: 4], ps=sps,
                            ticks=sp_ticks, save=plot_f)

# Show maps
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont',
                   save=path.join(plot_f, 'map_cont.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'hab_i',
                   save=path.join(plot_f, 'map_hab_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'han_i',
                   save=path.join(plot_f, 'map_han_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'n2_i',
                   save=path.join(plot_f, 'map_n2_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 's2_i',
                   save=path.join(plot_f, 'map_s2_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'han_c', mask_map='han_i',
                   mask_thr=mask_vel[0], vel=True,
                   save=path.join(plot_f, 'map_han_c.png'))

# Show maps, for printing
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont', printing=True,
                   save=path.join(plot_f, 'print_map_cont.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'hab_i', printing=True,
                   save=path.join(plot_f, 'print_map_hab_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'han_i', printing=True,
                   save=path.join(plot_f, 'print_map_han_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'n2_i', printing=True,
                   save=path.join(plot_f, 'print_map_n2_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 's2_i', printing=True,
                   save=path.join(plot_f, 'print_map_s2_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'han_c', mask_map='han_i',
                   mask_thr=mask_vel[0], vel=True, printing=True,
                   save=path.join(plot_f, 'print_map_han_c.png'))

# Show maps with fitted Moffat
map_cont, _ = cube_nifs.grab_map_k(maps, 'cont')
map_hab_i, _ = cube_nifs.grab_map_k(maps, 'hab_i')
map_han_i, _ = cube_nifs.grab_map_k(maps, 'han_i')
map_n2_i, _ = cube_nifs.grab_map_k(maps, 'n2_i')
map_s2_i, _ = cube_nifs.grab_map_k(maps, 's2_i')
fit_psf_cont, err_psf_cont = fit_nifs.moffat2d_fit(map_cont,
                                                   pos_guess='center')
fit_psf_cont_uni, err_psf_cont_uni = fit_nifs.moffat2d_fit(map_cont,
                                                           bivariate=False,
                                                           pos_guess='center')
fit_psf_hab_i, err_psf_hab_i = fit_nifs.moffat2d_fit(map_hab_i,
                                                     pos_guess='center')
fit_psf_han_i, err_psf_han_i = fit_nifs.moffat2d_fit(map_han_i,
                                                     pos_guess='center')
fit_psf_n2_i, err_psf_n2_i = fit_nifs.moffat2d_fit(map_n2_i,
                                                   pos_guess='center')
fit_psf_s2_i, err_psf_s2_i = fit_nifs.moffat2d_fit(map_s2_i,
                                                   pos_guess='center')
fwhms_psf_cont = 2 * np.array(fit_psf_cont[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_cont[7])) - 1) * sps
fwhms_psf_cont_uni = 2 * np.array(fit_psf_cont_uni[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_cont_uni[7])) - 1) * sps
fwhms_psf_hab_i = 2 * np.array(fit_psf_hab_i[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_hab_i[7])) - 1) * sps
fwhms_psf_han_i = 2 * np.array(fit_psf_han_i[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_han_i[7])) - 1) * sps
fwhms_psf_n2_i = 2 * np.array(fit_psf_n2_i[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_n2_i[7])) - 1) * sps
fwhms_psf_s2_i = 2 * np.array(fit_psf_s2_i[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_s2_i[7])) - 1) * sps
fwhm_psf_cont = np.sqrt(fwhms_psf_cont[0] * fwhms_psf_cont[1])
fwhm_psf_cont_uni = fwhms_psf_cont_uni[0]
fwhm_psf_hab_i = np.sqrt(fwhms_psf_hab_i[0] * fwhms_psf_hab_i[1])
fwhm_psf_han_i = np.sqrt(fwhms_psf_han_i[0] * fwhms_psf_han_i[1])
fwhm_psf_n2_i = np.sqrt(fwhms_psf_n2_i[0] * fwhms_psf_n2_i[1])
fwhm_psf_s2_i = np.sqrt(fwhms_psf_s2_i[0] * fwhms_psf_s2_i[1])
pa_psf_cont = fit_psf_cont[6]
pa_psf_cont_uni = 0
pa_psf_hab_i = fit_psf_hab_i[6]
pa_psf_han_i = fit_psf_han_i[6]
pa_psf_n2_i = fit_psf_n2_i[6]
pa_psf_s2_i = fit_psf_s2_i[6]
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont',
                   model=[fit_psf_cont[2], fit_psf_cont[3],
                          (fwhms_psf_cont[0] / sps), (fwhms_psf_cont[1] / sps),
                          np.rad2deg(pa_psf_cont), fwhm_psf_cont],
                   save=path.join(plot_f, 'map_cont_moffat.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'hab_i',
                   model=[fit_psf_hab_i[2], fit_psf_hab_i[3],
                          (fwhms_psf_hab_i[0] / sps),
                          (fwhms_psf_hab_i[1] / sps), np.rad2deg(pa_psf_hab_i),
                          fwhm_psf_hab_i],
                   save=path.join(plot_f, 'map_hab_i_moffat.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'han_i',
                   model=[fit_psf_han_i[2], fit_psf_han_i[3],
                          (fwhms_psf_han_i[0] / sps),
                          (fwhms_psf_han_i[1] / sps), np.rad2deg(pa_psf_han_i),
                          fwhm_psf_han_i],
                   save=path.join(plot_f, 'map_han_i_moffat.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'n2_i',
                   model=[fit_psf_n2_i[2], fit_psf_n2_i[3],
                          (fwhms_psf_n2_i[0] / sps), (fwhms_psf_n2_i[1] / sps),
                          np.rad2deg(pa_psf_n2_i), fwhm_psf_n2_i],
                   save=path.join(plot_f, 'map_n2_i_moffat.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 's2_i',
                   model=[fit_psf_s2_i[2], fit_psf_s2_i[3],
                          (fwhms_psf_s2_i[0] / sps), (fwhms_psf_s2_i[1] / sps),
                          np.rad2deg(pa_psf_s2_i), fwhm_psf_s2_i],
                   save=path.join(plot_f, 'map_s2_i_moffat.png'))

# Save profiles
profile_cont = fit_psf_cont[2:]
profile_cont_uni = fit_psf_cont_uni[2:]
profile_hab_i = fit_psf_hab_i[2:]
profile_han_i = fit_psf_han_i[2:]
profile_n2_i = fit_psf_n2_i[2:]
profile_s2_i = fit_psf_s2_i[2:]
err_cont = fit_psf_cont[2:]
err_cont_uni = err_psf_cont_uni[2:]
err_hab_i = err_psf_hab_i[2:]
err_han_i = err_psf_han_i[2:]
err_n2_i = err_psf_n2_i[2:]
err_s2_i = err_psf_s2_i[2:]
profiles = {'band': 'K', 'sps': sps,
            'tag': ['cont', 'cont_uni', 'hab_i', 'han_i', 'n2_i', 's2_i'],
            'name': ["Continuum", "Continuum (uni)", r"H$_{\alpha,b}$",
                     r"H$_{\alpha,n}$", "[NII]", "[SII]"],
            'profile': np.stack((np.array(profile_cont),
                                 np.array(profile_cont_uni),
                                 np.array(profile_hab_i),
                                 np.array(profile_han_i),
                                 np.array(profile_n2_i),
                                 np.array(profile_s2_i))),
            'error': np.stack((np.array(err_cont), np.array(err_cont_uni),
                               np.array(err_hab_i), np.array(err_han_i),
                               np.array(err_n2_i), np.array(err_s2_i)))}
file_profile = open(path.join(plot_f, 'profiles_k.pkl'), 'wb')
pickle.dump(profiles, file_profile)
file_profile.close()

# Show radial profiles
map_cont_center_where = np.where(map_cont == np.max(map_cont))
map_hab_i_center_where = np.where(map_hab_i == np.max(map_hab_i))
map_han_i_center_where = np.where(map_han_i == np.max(map_han_i))
map_n2_i_center_where = np.where(map_n2_i == np.max(map_n2_i))
map_s2_i_center_where = np.where(map_s2_i == np.max(map_s2_i))
map_cont_center = [map_cont_center_where[1][0], map_cont_center_where[0][0]]
map_hab_i_center = [map_hab_i_center_where[1][0], map_hab_i_center_where[0][0]]
map_han_i_center = [map_han_i_center_where[1][0], map_han_i_center_where[0][0]]
map_n2_i_center = [map_n2_i_center_where[1][0], map_n2_i_center_where[0][0]]
map_s2_i_center = [map_s2_i_center_where[1][0], map_s2_i_center_where[0][0]]
model_data_cont = [(fit_psf_cont[0] / (sps ** 2)), np.mean(fit_psf_cont[4: 6]),
                   fit_psf_cont[7], (fit_psf_cont[1] / (sps ** 2)),
                   fwhm_psf_cont]
model_data_hab_i = [(fit_psf_hab_i[0] / (sps ** 2)),
                    np.mean(fit_psf_hab_i[4: 6]), fit_psf_hab_i[7],
                    (fit_psf_hab_i[1] / (sps ** 2)), fwhm_psf_hab_i]
model_data_han_i = [(fit_psf_han_i[0] / (sps ** 2)),
                    np.mean(fit_psf_han_i[4: 6]), fit_psf_han_i[7],
                    (fit_psf_han_i[1] / (sps ** 2)), fwhm_psf_han_i]
model_data_n2_i = [(fit_psf_n2_i[0] / (sps ** 2)), np.mean(fit_psf_n2_i[4: 6]),
                   fit_psf_n2_i[7], (fit_psf_n2_i[1] / (sps ** 2)),
                   fwhm_psf_n2_i]
model_data_s2_i = [(fit_psf_s2_i[0] / (sps ** 2)), np.mean(fit_psf_s2_i[4: 6]),
                   fit_psf_s2_i[7], (fit_psf_s2_i[1] / (sps ** 2)),
                   fwhm_psf_s2_i]
cube_nifs.plot_rad_map(maps, sps, map_cont_center, 'cont', model_type='moffat',
                       model_data=model_data_cont,
                       y_label=("Flux density (" +
                                params_nifs.flux_dens_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_cont.png'))
cube_nifs.plot_rad_map(maps, sps, map_hab_i_center, 'hab_i',
                       model_type='moffat',
                       model_data=model_data_hab_i,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_hab_i.png'))
cube_nifs.plot_rad_map(maps, sps, map_han_i_center, 'han_i',
                       model_type='moffat',
                       model_data=model_data_han_i,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_han_i.png'))
cube_nifs.plot_rad_map(maps, sps, map_n2_i_center, 'n2_i', model_type='moffat',
                       model_data=model_data_n2_i,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_n2_i.png'))
cube_nifs.plot_rad_map(maps, sps, map_s2_i_center, 's2_i', model_type='moffat',
                       model_data=model_data_s2_i,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_s2_i.png'))

# Show radial profiles, for printing
cube_nifs.plot_rad_map(maps, sps, map_cont_center, 'cont', model_type='moffat',
                       model_data=model_data_cont, zoom=max_r,
                       y_label=("Flux density (" +
                                params_nifs.flux_dens_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_cont.png'))
cube_nifs.plot_rad_map(maps, sps, map_hab_i_center, 'hab_i',
                       model_type='moffat',
                       model_data=model_data_hab_i, zoom=max_r,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_hab_i.png'))
cube_nifs.plot_rad_map(maps, sps, map_han_i_center, 'han_i',
                       model_type='moffat',
                       model_data=model_data_han_i, zoom=max_r,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_han_i.png'))
cube_nifs.plot_rad_map(maps, sps, map_n2_i_center, 'n2_i', model_type='moffat',
                       model_data=model_data_n2_i, zoom=max_r,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_n2_i.png'))
cube_nifs.plot_rad_map(maps, sps, map_s2_i_center, 's2_i', model_type='moffat',
                       model_data=model_data_s2_i, zoom=max_r,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_s2_i.png'))

# Calculate fitted integrals
print("\nAnalyzing integrated fluxes ...")
integr_cont = fit_nifs.moffat_integr(fit_psf_cont)
integr_hab_i = fit_nifs.moffat_integr(fit_psf_hab_i)
integr_han_i = fit_nifs.moffat_integr(fit_psf_han_i)
integr_n2_i = fit_nifs.moffat_integr(fit_psf_n2_i)
integr_s2_i = fit_nifs.moffat_integr(fit_psf_s2_i)
_, _, _, _, _, _, _, _, m_bh_integr = fit_nifs.phys_lines_k(k_fit_phys,
                                                            hab_i=integr_hab_i)

# Show the fitted integrals
line1 = "   Continuum fitted integral: {0:.3e} erg/s/cm^2/um".format(
    integr_cont)
line2 = "   Broad H_a fitted integral: {0:.3e} erg/s/cm^2".format(
    integr_hab_i)
line3 = "   Narrow H_a fitted integral: {0:.3e} erg/s/cm^2".format(
    integr_han_i)
line4 = "   [NII] fitted integral: {0:.3e} erg/s/cm^2".format(integr_n2_i)
line5 = "   [SII] fitted integral: {0:.3e} erg/s/cm^2".format(integr_s2_i)
line6 = "   SMBH mass: {0:.3f} x 10^9 M_Sun".format(m_bh_integr * 1e-9)
print(line1)
print(line2)
print(line3)
print(line4)
print(line5)
print(line6)

# Save the fitted integrals
file_txt = open(path.join(plot_f, 'spectr_integr.txt'), 'w')
file_txt.write(line1 + "\n")
file_txt.write(line2 + "\n")
file_txt.write(line3 + "\n")
file_txt.write(line4 + "\n")
file_txt.write(line5 + "\n")
file_txt.write(line6 + "\n")
file_txt.close()

# Show quadrant radial profiles
steps_quad1 = np.arange(5) * avg_step
steps_quad2 = np.arange(steps_quad1[-1], dist_range[1], (2 * avg_step))
steps_quad = np.concatenate([steps_quad1, steps_quad2[1:], [dist_range[1]]])
cube_nifs.plot_rad_quad_map(maps, sps, map_cont_center, 'cont', steps_quad,
                            y_label=("Flux density(" +
                                     params_nifs.flux_dens_spat_unit + ")"),
                            save=path.join(plot_f, 'rad_quad_cont.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_hab_i_center, 'hab_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"),
                            save=path.join(plot_f, 'rad_quad_hab_i.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_han_i_center, 'han_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"),
                            save=path.join(plot_f, 'rad_quad_han_i.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_n2_i_center, 'n2_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"),
                            save=path.join(plot_f, 'rad_quad_n2_i.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_s2_i_center, 's2_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"),
                            save=path.join(plot_f, 'rad_quad_s2_i.png'))

# Show quadrant radial profiles, for printing
cube_nifs.plot_rad_quad_map(maps, sps, map_cont_center, 'cont', steps_quad,
                            y_label=("Flux density(" +
                                     params_nifs.flux_dens_spat_unit + ")"),
                            printing=True,
                            save=path.join(plot_f, 'print_rad_quad_cont.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_hab_i_center, 'hab_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"), printing=True,
                            save=path.join(plot_f, 'print_rad_quad_hab_i.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_han_i_center, 'han_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"), printing=True,
                            save=path.join(plot_f, 'print_rad_quad_han_i.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_n2_i_center, 'n2_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"), printing=True,
                            save=path.join(plot_f, 'print_rad_quad_n2_i.png'))
cube_nifs.plot_rad_quad_map(maps, sps, map_s2_i_center, 's2_i', steps_quad,
                            y_label=("Flux (" + params_nifs.flux_spat_unit +
                                     ")"), printing=True,
                            save=path.join(plot_f, 'print_rad_quad_s2_i.png'))

# Show azimuthal profiles
beam_az_cont = np.array([[map_cont_center[0], map_cont_center[1],
                          (dist_range[0] / sps)],
                         [map_cont_center[0], map_cont_center[1],
                          (dist_range[1] / sps)]])
beam_az_hab_i = np.array([[map_hab_i_center[0], map_hab_i_center[1],
                           (dist_range[0] / sps)],
                          [map_hab_i_center[0], map_hab_i_center[1],
                           (dist_range[1] / sps)]])
beam_az_han_i = np.array([[map_han_i_center[0], map_han_i_center[1],
                           (dist_range[0] / sps)],
                          [map_han_i_center[0], map_han_i_center[1],
                           (dist_range[1] / sps)]])
beam_az_n2_i = np.array([[map_n2_i_center[0], map_n2_i_center[1],
                          (dist_range[0] / sps)],
                         [map_n2_i_center[0], map_n2_i_center[1],
                          (dist_range[1] / sps)]])
beam_az_s2_i = np.array([[map_s2_i_center[0], map_s2_i_center[1],
                          (dist_range[0] / sps)],
                         [map_s2_i_center[0], map_s2_i_center[1],
                          (dist_range[1] / sps)]])
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont', beam_circ=beam_az_cont,
                   save=path.join(plot_f, 'map_az_cont.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'hab_i', beam_circ=beam_az_hab_i,
                   save=path.join(plot_f, 'map_az_hab_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'han_i', beam_circ=beam_az_han_i,
                   save=path.join(plot_f, 'map_az_han_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'n2_i', beam_circ=beam_az_n2_i,
                   save=path.join(plot_f, 'map_az_n2_i.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 's2_i', beam_circ=beam_az_s2_i,
                   save=path.join(plot_f, 'map_az_s2_i.png'))
cube_nifs.plot_az(maps, sps, map_cont_center, 'cont', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_dens_range_az,
                  y_label=("Average flux density\n(" +
                           params_nifs.flux_dens_spat_unit + ")"),
                  save=path.join(plot_f, 'az_cont.png'))
cube_nifs.plot_az(maps, sps, map_hab_i_center, 'hab_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_b,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_hab_i.png'))
cube_nifs.plot_az(maps, sps, map_han_i_center, 'han_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_han_i.png'))
cube_nifs.plot_az(maps, sps, map_n2_i_center, 'n2_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_n2_i.png'))
cube_nifs.plot_az(maps, sps, map_s2_i_center, 's2_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_s2_i.png'))

# Show azimuthal profiles, for printing
cube_nifs.plot_az(maps, sps, map_cont_center, 'cont', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_dens_range_az,
                  y_label=("Average flux density\n(" +
                           params_nifs.flux_dens_spat_unit + ")"),
                  printing=True,
                  save=path.join(plot_f, 'print_az_cont.png'))
cube_nifs.plot_az(maps, sps, map_hab_i_center, 'hab_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_b,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), printing=True,
                  save=path.join(plot_f, 'print_az_hab_i.png'))
cube_nifs.plot_az(maps, sps, map_han_i_center, 'han_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), printing=True,
                  save=path.join(plot_f, 'print_az_han_i.png'))
cube_nifs.plot_az(maps, sps, map_n2_i_center, 'n2_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), printing=True,
                  save=path.join(plot_f, 'print_az_n2_i.png'))
cube_nifs.plot_az(maps, sps, map_s2_i_center, 's2_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), printing=True,
                  save=path.join(plot_f, 'print_az_s2_i.png'))

# Calculate line ratios
print("\n Line ratios")
n2_han = np.zeros([len(beam), 2])
file_txt = open(path.join(plot_f, 'line_ratio.txt'), 'w')

for i_beam in range(len(beam)):
    han_bin, han_bin_err = cube_nifs.bin_map(maps, 'han_i', beam[i_beam])
    n2_bin, n2_bin_err = cube_nifs.bin_map(maps, 'n2_i', beam[i_beam])
    n2_han[i_beam, 0] = n2_bin / han_bin
    n2_han[i_beam, 1] = n2_han[i_beam, 0] * \
        np.sqrt(((n2_bin_err / n2_bin) ** 2) + ((han_bin_err / han_bin) ** 2))
    line1 = "[NII] / H_a,n ({0}): {1:.2f} +/- {2:.2f}".format(
        direction_label[i_beam], n2_han[i_beam, 0], n2_han[i_beam, 1])
    print(line1)
    file_txt.write(line1 + "\n")

file_txt.close()

# Save maps as FITS files
hdu_cont = fits.PrimaryHDU(map_cont)
hdu_hab_i = fits.PrimaryHDU(map_hab_i)
hdu_han_i = fits.PrimaryHDU(map_han_i)
hdu_n2_i = fits.PrimaryHDU(map_n2_i)
hdu_s2_i = fits.PrimaryHDU(map_s2_i)
hdu_cont.writeto(path.join(fits_f, 'map_cont.fits'), overwrite=True)
hdu_hab_i.writeto(path.join(fits_f, 'map_hab_i.fits'), overwrite=True)
hdu_han_i.writeto(path.join(fits_f, 'map_han_i.fits'), overwrite=True)
hdu_n2_i.writeto(path.join(fits_f, 'map_n2_i.fits'), overwrite=True)
hdu_s2_i.writeto(path.join(fits_f, 'map_s2_i.fits'), overwrite=True)

# Convolve maps
print("\nAnalyzing convolved maps ...")
maps_conv = cube_nifs.convolve_maps(maps, conv_spx)

# Show convolved maps
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'cont',
                   save=path.join(plot_f, 'map_cont_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'hab_i',
                   save=path.join(plot_f, 'map_hab_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'han_i',
                   save=path.join(plot_f, 'map_han_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'n2_i',
                   save=path.join(plot_f, 'map_n2_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 's2_i',
                   save=path.join(plot_f, 'map_s2_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'han_c', mask_map='han_i',
                   mask_thr=mask_vel[0], vel=True,
                   save=path.join(plot_f, 'map_han_c_conv.png'))

# Show convolved peak radial profiles
map_cont_conv, _ = cube_nifs.grab_map_k(maps_conv, 'cont')
map_hab_i_conv, _ = cube_nifs.grab_map_k(maps_conv, 'hab_i')
map_han_i_conv, _ = cube_nifs.grab_map_k(maps_conv, 'han_i')
map_n2_i_conv, _ = cube_nifs.grab_map_k(maps_conv, 'n2_i')
map_s2_i_conv, _ = cube_nifs.grab_map_k(maps_conv, 's2_i')
map_cont_center_where_conv = np.where(map_cont_conv == np.max(map_cont_conv))
map_hab_i_center_where_conv = np.where(map_hab_i_conv ==
                                       np.max(map_hab_i_conv))
map_han_i_center_where_conv = np.where(map_han_i_conv ==
                                       np.max(map_han_i_conv))
map_n2_i_center_where_conv = np.where(map_n2_i_conv == np.max(map_n2_i_conv))
map_s2_i_center_where_conv = np.where(map_s2_i_conv == np.max(map_s2_i_conv))
map_cont_center_conv = [map_cont_center_where_conv[1][0],
                        map_cont_center_where_conv[0][0]]
map_hab_i_center_conv = [map_hab_i_center_where_conv[1][0],
                         map_hab_i_center_where_conv[0][0]]
map_han_i_center_conv = [map_han_i_center_where_conv[1][0],
                         map_han_i_center_where_conv[0][0]]
map_n2_i_center_conv = [map_n2_i_center_where_conv[1][0],
                        map_n2_i_center_where_conv[0][0]]
map_s2_i_center_conv = [map_s2_i_center_where_conv[1][0],
                        map_s2_i_center_where_conv[0][0]]
cube_nifs.plot_rad_map(maps_conv, sps, map_cont_center_conv, 'cont',
                       y_label=("Flux density (" +
                                params_nifs.flux_dens_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_cont_conv.png'))
cube_nifs.plot_rad_map(maps_conv, sps, map_hab_i_center_conv, 'hab_i',
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_hab_i_conv.png'))
cube_nifs.plot_rad_map(maps_conv, sps, map_han_i_center_conv, 'han_i',
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_han_i_conv.png'))
cube_nifs.plot_rad_map(maps_conv, sps, map_n2_i_center_conv, 'n2_i',
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_n2_i_conv.png'))
cube_nifs.plot_rad_map(maps_conv, sps, map_s2_i_center_conv, 's2_i',
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_s2_i_conv.png'))

# Save convolved maps as FITS files
hdu_cont_conv = fits.PrimaryHDU(map_cont_conv)
hdu_hab_i_conv = fits.PrimaryHDU(map_hab_i_conv)
hdu_han_i_conv = fits.PrimaryHDU(map_han_i_conv)
hdu_n2_i_conv = fits.PrimaryHDU(map_n2_i_conv)
hdu_s2_i_conv = fits.PrimaryHDU(map_s2_i_conv)
hdu_cont_conv.writeto(path.join(fits_f, 'map_cont_conv.fits'), overwrite=True)
hdu_hab_i_conv.writeto(path.join(fits_f, 'map_hab_i_conv.fits'),
                       overwrite=True)
hdu_han_i_conv.writeto(path.join(fits_f, 'map_han_i_conv.fits'),
                       overwrite=True)
hdu_n2_i_conv.writeto(path.join(fits_f, 'map_n2_i_conv.fits'), overwrite=True)
hdu_s2_i_conv.writeto(path.join(fits_f, 'map_s2_i_conv.fits'), overwrite=True)

# Show convolved peak azimuthal profiles
beam_az_cont_conv = np.array([[map_cont_center_conv[0],
                               map_cont_center_conv[1], (dist_range[0] / sps)],
                              [map_cont_center_conv[0],
                               map_cont_center_conv[1],
                               (dist_range[1] / sps)]])
beam_az_hab_i_conv = np.array([[map_hab_i_center_conv[0],
                                map_hab_i_center_conv[1],
                                (dist_range[0] / sps)],
                               [map_hab_i_center_conv[0],
                                map_hab_i_center_conv[1],
                                (dist_range[1] / sps)]])
beam_az_han_i_conv = np.array([[map_han_i_center_conv[0],
                                map_han_i_center_conv[1],
                                (dist_range[0] / sps)],
                               [map_han_i_center_conv[0],
                                map_han_i_center_conv[1],
                                (dist_range[1] / sps)]])
beam_az_n2_i_conv = np.array([[map_n2_i_center_conv[0],
                               map_n2_i_center_conv[1], (dist_range[0] / sps)],
                              [map_n2_i_center_conv[0],
                               map_n2_i_center_conv[1],
                               (dist_range[1] / sps)]])
beam_az_s2_i_conv = np.array([[map_s2_i_center_conv[0],
                               map_s2_i_center_conv[1], (dist_range[0] / sps)],
                              [map_s2_i_center_conv[0],
                               map_s2_i_center_conv[1],
                               (dist_range[1] / sps)]])
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'cont',
                   beam_circ=beam_az_cont_conv,
                   save=path.join(plot_f, 'map_az_cont_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'hab_i',
                   beam_circ=beam_az_hab_i_conv,
                   save=path.join(plot_f, 'map_az_hab_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'han_i',
                   beam_circ=beam_az_han_i_conv,
                   save=path.join(plot_f, 'map_az_han_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'n2_i',
                   beam_circ=beam_az_n2_i_conv,
                   save=path.join(plot_f, 'map_az_n2_i_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 's2_i',
                   beam_circ=beam_az_s2_i_conv,
                   save=path.join(plot_f, 'map_az_s2_i_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_cont_center_conv, 'cont',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_dens_range_az,
                  y_label=("Average flux density\n(" +
                           params_nifs.flux_dens_spat_unit + ")"),
                  save=path.join(plot_f, 'az_cont_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_hab_i_center_conv, 'hab_i',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_range_az_b,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_hab_i_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_han_i_center_conv, 'han_i',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_han_i_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_n2_i_center_conv, 'n2_i',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_n2_i_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_s2_i_center_conv, 's2_i',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_range_az_n,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_s2_i_conv.png'))

# Binning with different factors
for i_bin_spx in range(len(bin_spxs)):

    # Bin spaxels
    bin_spx = bin_spxs[i_bin_spx]
    print("\nAnalyzing binned maps ({0}x{0}) ...".format(bin_spx))
    data_bin = cube_nifs.bin_cube(data_clean, bin_spx)
    data_bin_coll = cube_nifs.collapse_cube(data_bin)
    data_bin_coll *= (k_range[1] - k_range[0])

    # Show image binned by spaxels
    data_bin_coll_plot = data_bin_coll / ((sps * bin_spx) ** 2)
    cube_nifs.plot_img(data_bin_coll_plot, (sps * bin_spx), sp_ticks,
                       c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       title="Collapsed cube, binned by spaxels",
                       save=path.join(plot_f,
                                      'coll_bin_{0}.png'.format(bin_spx)))

    # Map binned lines
    maps_bin = fit_nifs.fit_map_k(wvl_clean, data_bin, wvl_range_cont,
                                  fix_pars=k_fit_m, n_spaces=3)

    # Show binned maps
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'cont',
                       save=path.join(plot_f,
                                      'map_cont_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'hab_i',
                       save=path.join(plot_f,
                                      'map_hab_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'han_i',
                       save=path.join(plot_f,
                                      'map_han_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'n2_i',
                       save=path.join(plot_f,
                                      'map_n2_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 's2_i',
                       save=path.join(plot_f,
                                      'map_s2_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'han_c',
                       mask_map='han_i', mask_thr=mask_vel[1], vel=True,
                       save=path.join(plot_f,
                                      'map_han_c_bin_{0}.png'.format(bin_spx)))

    # Measure LOS velocity STD
    map_han_c_bin, _ = cube_nifs.grab_map_k(maps_bin, 'han_c')
    han_c_std = np.std(map_han_c_bin)

    # Show binned peak radial profiles
    map_cont_bin, _ = cube_nifs.grab_map_k(maps_bin, 'cont')
    map_hab_i_bin, _ = cube_nifs.grab_map_k(maps_bin, 'hab_i')
    map_han_i_bin, _ = cube_nifs.grab_map_k(maps_bin, 'han_i')
    map_n2_i_bin, _ = cube_nifs.grab_map_k(maps_bin, 'n2_i')
    map_s2_i_bin, _ = cube_nifs.grab_map_k(maps_bin, 's2_i')
    map_cont_center_where_bin = np.where(map_cont_bin ==
                                         np.max(map_cont_bin))
    map_hab_i_center_where_bin = np.where(map_hab_i_bin ==
                                          np.max(map_hab_i_bin))
    map_han_i_center_where_bin = np.where(map_han_i_bin ==
                                          np.max(map_han_i_bin))
    map_n2_i_center_where_bin = np.where(map_n2_i_bin == np.max(map_n2_i_bin))
    map_s2_i_center_where_bin = np.where(map_s2_i_bin == np.max(map_s2_i_bin))
    map_cont_center_bin = [map_cont_center_where_bin[1][0],
                           map_cont_center_where_bin[0][0]]
    map_hab_i_center_bin = [map_hab_i_center_where_bin[1][0],
                            map_hab_i_center_where_bin[0][0]]
    map_han_i_center_bin = [map_han_i_center_where_bin[1][0],
                            map_han_i_center_where_bin[0][0]]
    map_n2_i_center_bin = [map_n2_i_center_where_bin[1][0],
                           map_n2_i_center_where_bin[0][0]]
    map_s2_i_center_bin = [map_s2_i_center_where_bin[1][0],
                           map_s2_i_center_where_bin[0][0]]
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_cont_center_bin,
                           'cont',
                           y_label=("Flux density (" +
                                    params_nifs.flux_dens_spat_unit + ")"),
                           save=path.join(plot_f,
                                          'rad_cont_bin_{0}.png'.format(
                                              bin_spx)))
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_hab_i_center_bin,
                           'hab_i',
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           save=path.join(plot_f,
                                          'rad_hab_i_bin_{0}.png'.format(
                                              bin_spx)))
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_han_i_center_bin,
                           'han_i',
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           save=path.join(plot_f,
                                          'rad_han_i_bin_{0}.png'.format(
                                              bin_spx)))
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_n2_i_center_bin,
                           'n2_i',
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           save=path.join(plot_f,
                                          'rad_n2_i_bin_{0}.png'.format(
                                              bin_spx)))
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_s2_i_center_bin,
                           's2_i',
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           save=path.join(plot_f,
                                          'rad_s2_i_bin_{0}.png'.format(
                                              bin_spx)))

    # Show binned peak azimuthal profiles
    beam_az_cont_bin = np.array([[map_cont_center_bin[0],
                                  map_cont_center_bin[1],
                                  (dist_range[0] / (sps * bin_spx))],
                                 [map_cont_center_bin[0],
                                  map_cont_center_bin[1],
                                  (dist_range[1] / (sps * bin_spx))]])
    beam_az_hab_i_bin = np.array([[map_hab_i_center_bin[0],
                                   map_hab_i_center_bin[1],
                                   (dist_range[0] / (sps * bin_spx))],
                                  [map_hab_i_center_bin[0],
                                   map_hab_i_center_bin[1],
                                   (dist_range[1] / (sps * bin_spx))]])
    beam_az_han_i_bin = np.array([[map_han_i_center_bin[0],
                                   map_han_i_center_bin[1],
                                   (dist_range[0] / (sps * bin_spx))],
                                  [map_han_i_center_bin[0],
                                   map_han_i_center_bin[1],
                                   (dist_range[1] / (sps * bin_spx))]])
    beam_az_n2_i_bin = np.array([[map_n2_i_center_bin[0],
                                  map_n2_i_center_bin[1],
                                  (dist_range[0] / (sps * bin_spx))],
                                 [map_n2_i_center_bin[0],
                                  map_n2_i_center_bin[1],
                                  (dist_range[1] / (sps * bin_spx))]])
    beam_az_s2_i_bin = np.array([[map_s2_i_center_bin[0],
                                  map_s2_i_center_bin[1],
                                  (dist_range[0] / (sps * bin_spx))],
                                 [map_s2_i_center_bin[0],
                                  map_s2_i_center_bin[1],
                                  (dist_range[1] / (sps * bin_spx))]])
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'cont',
                       beam_circ=beam_az_cont_bin,
                       save=path.join(plot_f,
                                      'map_az_cont_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'hab_i',
                       beam_circ=beam_az_hab_i_bin,
                       save=path.join(plot_f,
                                      'map_az_hab_i_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'han_i',
                       beam_circ=beam_az_han_i_bin,
                       save=path.join(plot_f,
                                      'map_az_han_i_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'n2_i',
                       beam_circ=beam_az_n2_i_bin,
                       save=path.join(plot_f,
                                      'map_az_n2_i_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 's2_i',
                       beam_circ=beam_az_s2_i_bin,
                       save=path.join(plot_f,
                                      'map_az_s2_i_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_cont_center_bin, 'cont',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_dens_range_az_bin,
                      y_label=("Average flux density\n(" +
                               params_nifs.flux_dens_spat_unit + ")"),
                      save=path.join(plot_f,
                                     'az_cont_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_hab_i_center_bin, 'hab_i',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_range_az_bin_b,
                      y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                               ")"),
                      save=path.join(plot_f,
                                     'az_hab_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_han_i_center_bin, 'han_i',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_range_az_bin_n,
                      y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                               ")"),
                      save=path.join(plot_f,
                                     'az_han_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_n2_i_center_bin, 'n2_i',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_range_az_bin_n,
                      y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                               ")"),
                      save=path.join(plot_f,
                                     'az_n2_i_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_s2_i_center_bin, 's2_i',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_range_az_bin_n,
                      y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                               ")"),
                      save=path.join(plot_f,
                                     'az_s2_i_bin_{0}.png'.format(bin_spx)))

    # Save binned maps as FITS files
    hdu_cont_bin = fits.PrimaryHDU(map_cont_bin)
    hdu_hab_i_bin = fits.PrimaryHDU(map_hab_i_bin)
    hdu_han_i_bin = fits.PrimaryHDU(map_han_i_bin)
    hdu_n2_i_bin = fits.PrimaryHDU(map_n2_i_bin)
    hdu_s2_i_bin = fits.PrimaryHDU(map_s2_i_bin)
    hdu_cont_bin.writeto(path.join(fits_f,
                                   'map_cont_bin_{0}.fits'.format(bin_spx)),
                         overwrite=True)
    hdu_hab_i_bin.writeto(path.join(fits_f,
                                    'map_hab_i_bin_{0}.fits'.format(bin_spx)),
                          overwrite=True)
    hdu_han_i_bin.writeto(path.join(fits_f,
                                    'map_han_i_bin_{0}.fits'.format(bin_spx)),
                          overwrite=True)
    hdu_n2_i_bin.writeto(path.join(fits_f,
                                   'map_n2_i_bin_{0}.fits'.format(bin_spx)),
                         overwrite=True)
    hdu_s2_i_bin.writeto(path.join(fits_f,
                                   'map_n2_i_bin_{0}.fits'.format(bin_spx)),
                         overwrite=True)

    # Show grid of binned spectra
    wvl_bin_bin_test, _ = cube_nifs.bin_spectrum(wvl_clean, data_bin[:, 0, 0],
                                                 bin_wvl)
    data_bin_bin = np.zeros([len(wvl_bin_bin_test), data_bin.shape[1],
                             data_bin.shape[2]])
    wvl_bin_bin = []

    for i_row in range(data_bin.shape[1]):
        for i_col in range(data_bin.shape[2]):
            wvl_bin_bin, data_bin_bin[:, i_row, i_col] = \
                cube_nifs.bin_spectrum(wvl_clean, data_bin[:, i_row, i_col],
                                       bin_wvl)

    y1 = int((data_bin_bin.shape[1] - bin_grids) / 2)
    y2 = int((data_bin_bin.shape[1] + bin_grids) / 2)
    x1 = int((data_bin_bin.shape[2] - bin_grids) / 2)
    x2 = int((data_bin_bin.shape[2] + bin_grids) / 2)
    data_bin_bin_grid = data_bin_bin[:, y1: y2, x1: x2]
    maps_bin_grid = maps_bin[y1: y2, x1: x2]
    cube_nifs.grid_spectrum(wvl_bin_bin, data_bin_bin_grid,
                            fit_mod=maps_bin_grid, zoom=k_range_grid,
                            i_range=i_range_grid,
                            title="Grid of fitted binned spectra",
                            save=path.join(plot_f,
                                           ('grid_spectr_fit_bin_{0}' +
                                            '.png').format(bin_spx)))

# Subtract PSF
print("\nSubtracting maps ...")
n_psfs = 3
maps_psf = [np.array([])] * 3

for i_psf in range(n_psfs):

    # Choose profile
    if i_psf == 0:
        fit_psf = fit_psf_cont
        fwhms_psf = fwhms_psf_cont
        fwhm_psf = fwhm_psf_cont
        pa_psf = pa_psf_cont
        psf_str = profiles['tag'][0]
    elif i_psf == 1:
        fit_psf = fit_psf_cont_uni
        fwhms_psf = fwhms_psf_cont_uni
        fwhm_psf = fwhm_psf_cont_uni
        pa_psf = pa_psf_cont_uni
        psf_str = profiles['tag'][1]
    else:
        fit_psf = fit_psf_hab_i
        fwhms_psf = fwhms_psf_hab_i
        fwhm_psf = fwhm_psf_hab_i
        pa_psf = pa_psf_hab_i
        psf_str = profiles['tag'][2]

    # Create PSF map
    over_psf = 10  # Must be an even number
    psf_size = (np.max(data_shape[1:]) * over_psf) + 1
    psf_center = int(np.floor(psf_size / 2))
    y_psf, x_psf = np.mgrid[: psf_size, : psf_size]
    map_psf_model = fit_nifs.moffat2d(x_0=psf_center, y_0=psf_center,
                                      s1=(fit_psf[4] * over_psf),
                                      s2=(fit_psf[5] * over_psf),
                                      theta=fit_psf[6], alpha=fit_psf[7])
    map_psf = map_psf_model(x_psf, y_psf)
    map_psf_integr = np.sum(map_psf)
    maps_psf[i_psf] = map_psf / map_psf_integr

    # Calculate residual images
    map_cont_sub, title_cont_sub = cube_nifs.map_sub(maps, 'cont', fit_psf)
    map_hab_i_sub, title_hab_i_sub = cube_nifs.map_sub(maps, 'hab_i', fit_psf)
    map_han_i_sub, title_han_i_sub = cube_nifs.map_sub(maps, 'han_i', fit_psf)
    map_n2_i_sub, title_n2_i_sub = cube_nifs.map_sub(maps, 'n2_i', fit_psf)
    map_s2_i_sub, title_s2_i_sub = cube_nifs.map_sub(maps, 's2_i', fit_psf)

    # Show residual images
    map_cont_sub_plot = map_cont_sub / (sps ** 2)
    map_hab_i_sub_plot = map_hab_i_sub / (sps ** 2)
    map_han_i_sub_plot = map_han_i_sub / (sps ** 2)
    map_n2_i_sub_plot = map_n2_i_sub / (sps ** 2)
    map_s2_i_sub_plot = map_s2_i_sub / (sps ** 2)
    cube_nifs.plot_map_sub(map_cont_sub_plot, title_cont_sub,
                           ("Flux density (" + params_nifs.flux_dens_spat_unit
                            + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_cont_sub_' + psf_str +
                                                   '.png')))
    cube_nifs.plot_map_sub(map_hab_i_sub_plot, title_hab_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_hab_i_sub_' + psf_str +
                                                   '.png')))
    cube_nifs.plot_map_sub(map_han_i_sub_plot, title_han_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_han_i_sub_' + psf_str +
                                                   '.png')))
    cube_nifs.plot_map_sub(map_n2_i_sub_plot, title_n2_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_n2_i_sub_' + psf_str +
                                                   '.png')))
    cube_nifs.plot_map_sub(map_s2_i_sub_plot, title_s2_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_s2_i_sub_' + psf_str +
                                                   '.png')))

    # Show residual images, for printing
    cube_nifs.plot_map_sub(map_cont_sub_plot, title_cont_sub,
                           ("Flux density (" + params_nifs.flux_dens_spat_unit
                            + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           printing=True,
                           save=path.join(plot_f, ('print_map_cont_sub_' +
                                                   psf_str + '.png')))
    cube_nifs.plot_map_sub(map_hab_i_sub_plot, title_hab_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           printing=True,
                           save=path.join(plot_f, ('print_map_hab_i_sub_' +
                                                   psf_str + '.png')))
    cube_nifs.plot_map_sub(map_han_i_sub_plot, title_han_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           printing=True,
                           save=path.join(plot_f, ('print_map_han_i_sub_' +
                                                   psf_str + '.png')))
    cube_nifs.plot_map_sub(map_n2_i_sub_plot, title_n2_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           printing=True,
                           save=path.join(plot_f, ('print_map_n2_i_sub_' +
                                                   psf_str + '.png')))
    cube_nifs.plot_map_sub(map_s2_i_sub_plot, title_s2_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           printing=True,
                           save=path.join(plot_f, ('print_map_s2_i_sub_' +
                                                   psf_str + '.png')))

    # Save maps as FITS files
    hdu_cont_sub = fits.PrimaryHDU(map_cont_sub)
    hdu_hab_i_sub = fits.PrimaryHDU(map_hab_i_sub)
    hdu_han_i_sub = fits.PrimaryHDU(map_han_i_sub)
    hdu_n2_i_sub = fits.PrimaryHDU(map_n2_i_sub)
    hdu_s2_i_sub = fits.PrimaryHDU(map_s2_i_sub)
    hdu_cont_sub.writeto(path.join(fits_f,
                                   ('map_cont_sub_' + psf_str + '.fits')),
                         overwrite=True)
    hdu_hab_i_sub.writeto(path.join(fits_f,
                                    ('map_hab_i_sub_' + psf_str + '.fits')),
                          overwrite=True)
    hdu_han_i_sub.writeto(path.join(fits_f,
                                    ('map_han_i_sub_' + psf_str + '.fits')),
                          overwrite=True)
    hdu_n2_i_sub.writeto(path.join(fits_f,
                                   ('map_n2_i_sub_' + psf_str + '.fits')),
                         overwrite=True)
    hdu_s2_i_sub.writeto(path.join(fits_f,
                                   ('map_s2_i_sub_' + psf_str + '.fits')),
                         overwrite=True)

    # Show peak azimuthal subtracted profiles
    cube_nifs.plot_az_map(map_cont_sub, sps, map_cont_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_dens_range_az,
                          y_label=("Average flux density\n(" +
                                   params_nifs.flux_dens_spat_unit + ")"),
                          save=path.join(plot_f,
                                         ('az_cont_sub_' + psf_str + '.png')))
    cube_nifs.plot_az_map(map_hab_i_sub, sps, map_hab_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          save=path.join(plot_f,
                                         ('az_hab_i_sub_' + psf_str + '.png')))
    cube_nifs.plot_az_map(map_han_i_sub, sps, map_han_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          save=path.join(plot_f,
                                         ('az_han_i_sub_' + psf_str + '.png')))
    cube_nifs.plot_az_map(map_n2_i_sub, sps, map_n2_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          save=path.join(plot_f,
                                         ('az_n2_i_sub_' + psf_str + '.png')))
    cube_nifs.plot_az_map(map_s2_i_sub, sps, map_s2_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          save=path.join(plot_f,
                                         ('az_s2_i_sub_' + psf_str + '.png')))

    # Show peak azimuthal subtracted profiles, for printing
    cube_nifs.plot_az_map(map_cont_sub, sps, map_cont_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_dens_range_az,
                          y_label=("Average flux density\n(" +
                                   params_nifs.flux_dens_spat_unit + ")"),
                          printing=True,
                          save=path.join(plot_f,
                                         ('print_az_cont_sub_' + psf_str +
                                          '.png')))
    cube_nifs.plot_az_map(map_hab_i_sub, sps, map_hab_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          printing=True,
                          save=path.join(plot_f,
                                         ('print_az_hab_i_sub_' + psf_str +
                                          '.png')))
    cube_nifs.plot_az_map(map_han_i_sub, sps, map_han_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          printing=True,
                          save=path.join(plot_f,
                                         ('print_az_han_i_sub_' + psf_str +
                                          '.png')))
    cube_nifs.plot_az_map(map_n2_i_sub, sps, map_n2_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          printing=True,
                          save=path.join(plot_f,
                                         ('print_az_n2_i_sub_' + psf_str +
                                          '.png')))
    cube_nifs.plot_az_map(map_s2_i_sub, sps, map_s2_i_center, "",
                          n_slices=n_slices, dist_range=dist_range_sub,
                          range_flux=flux_range_az_n,
                          y_label=("Average flux\n(" +
                                   params_nifs.flux_spat_unit + ")"),
                          printing=True,
                          save=path.join(plot_f,
                                         ('print_az_s2_i_sub_' + psf_str +
                                          '.png')))

    # Convolve residual images
    map_cont_sub_conv = cube_nifs.convolve_map(map_cont_sub, conv_spx)
    map_hab_i_sub_conv = cube_nifs.convolve_map(map_hab_i_sub, conv_spx)
    map_han_i_sub_conv = cube_nifs.convolve_map(map_han_i_sub, conv_spx)
    map_n2_i_sub_conv = cube_nifs.convolve_map(map_n2_i_sub, conv_spx)
    map_s2_i_sub_conv = cube_nifs.convolve_map(map_s2_i_sub, conv_spx)

    # Show convolved residual images
    map_cont_sub_conv_plot = map_cont_sub_conv / (sps ** 2)
    map_hab_i_sub_conv_plot = map_hab_i_sub_conv / (sps ** 2)
    map_han_i_sub_conv_plot = map_han_i_sub_conv / (sps ** 2)
    map_n2_i_sub_conv_plot = map_n2_i_sub_conv / (sps ** 2)
    map_s2_i_sub_conv_plot = map_s2_i_sub_conv / (sps ** 2)
    cube_nifs.plot_map_sub(map_cont_sub_conv_plot, title_cont_sub,
                           ("Flux density (" + params_nifs.flux_dens_spat_unit
                            + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_cont_sub_' + psf_str +
                                                   '_conv.png')))
    cube_nifs.plot_map_sub(map_hab_i_sub_conv_plot, title_hab_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_hab_i_sub_' + psf_str +
                                                   '_conv.png')))
    cube_nifs.plot_map_sub(map_han_i_sub_conv_plot, title_han_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_han_i_sub_' + psf_str +
                                                   '_conv.png')))
    cube_nifs.plot_map_sub(map_n2_i_sub_conv_plot, title_n2_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_n2_i_sub_' + psf_str +
                                                   '_conv.png')))
    cube_nifs.plot_map_sub(map_s2_i_sub_conv_plot, title_s2_i_sub,
                           ("Flux (" + params_nifs.flux_spat_unit + ")"), sps,
                           sp_ticks,
                           model=[fit_psf[2], fit_psf[3], (fwhms_psf[0] / sps),
                                  (fwhms_psf[1] / sps), np.rad2deg(pa_psf),
                                  fwhm_psf],
                           save=path.join(plot_f, ('map_s2_i_sub_' + psf_str +
                                                   '_conv.png')))

    # Save convolved maps as FITS files
    hdu_cont_sub_conv = fits.PrimaryHDU(map_cont_sub_conv)
    hdu_hab_i_sub_conv = fits.PrimaryHDU(map_hab_i_sub_conv)
    hdu_han_i_sub_conv = fits.PrimaryHDU(map_han_i_sub_conv)
    hdu_n2_i_sub_conv = fits.PrimaryHDU(map_n2_i_sub_conv)
    hdu_s2_i_sub_conv = fits.PrimaryHDU(map_s2_i_sub_conv)
    hdu_cont_sub_conv.writeto(path.join(fits_f, ('map_cont_sub_' + psf_str +
                                                 '_conv.fits')),
                              overwrite=True)
    hdu_hab_i_sub_conv.writeto(path.join(fits_f, ('map_hab_i_sub_' + psf_str +
                                                  '_conv.fits')),
                               overwrite=True)
    hdu_han_i_sub_conv.writeto(path.join(fits_f, ('map_han_i_sub_' + psf_str +
                                                  '_conv.fits')),
                               overwrite=True)
    hdu_n2_i_sub_conv.writeto(path.join(fits_f, ('map_n2_i_sub_' + psf_str +
                                                 '_conv.fits')),
                              overwrite=True)
    hdu_s2_i_sub_conv.writeto(path.join(fits_f, ('map_s2_i_sub_' + psf_str +
                                                 '_conv.fits')),
                              overwrite=True)

# Show PSFs
max_maps_psf = max([np.max(a) for a in maps_psf])

for i_psf in range(n_psfs):
    if i_psf == 0:
        psf_str = profiles['tag'][0]
        psf_title = profiles['name'][0]
    if i_psf == 1:
        psf_str = profiles['tag'][1]
        psf_title = profiles['name'][1]
    else:
        psf_str = profiles['tag'][2]
        psf_title = profiles['name'][2]

    cube_nifs.plot_img(maps_psf[i_psf], (sps / 10), sp_ticks, imin=0,
                       imax=max_maps_psf, c_bar=False,
                       title="{0} PSF".format(psf_title),
                       save=path.join(plot_f,
                                      'psf_{0}.png'.format(psf_str)))
    cube_nifs.plot_img(maps_psf[i_psf], (sps / 10), sp_ticks, imin=0,
                       imax=max_maps_psf, c_bar=False, printing=False,
                       save=path.join(plot_f,
                                      'print_psf_{0}.png'.format(psf_str)))

# Compare PSFs
psfs_combo = list(itertools.combinations(range(n_psfs), 2))

for psf_combo in psfs_combo:
    map_psf_diff = maps_psf[psf_combo[0]] - maps_psf[psf_combo[1]]
    map_psf_diff_max = np.max(maps_psf[psf_combo[0]])
    map_psf_diff /= map_psf_diff_max
    cube_nifs.plot_img(map_psf_diff, (sps / 10), sp_ticks, imin=0,
                       c_label="({0} PSF peak)".format(
                           profiles['name'][psf_combo[0]]),
                       title="{0} and {1} PSF difference".format(
                           profiles['name'][psf_combo[0]],
                           profiles['name'][psf_combo[1]]),
                       save=path.join(plot_f,
                                      'psf_diff_{0}_{1}.png'.format(
                                          profiles['tag'][psf_combo[0]],
                                          profiles['tag'][psf_combo[1]])))
    cube_nifs.plot_img(map_psf_diff, (sps / 10), sp_ticks, imin=0,
                       printing=False,
                       save=path.join(plot_f,
                                      'print_psf_diff_{0}_{1}.png'.format(
                                          profiles['tag'][psf_combo[0]],
                                          profiles['tag'][psf_combo[1]])))
print("\nProgram terminated\n")
