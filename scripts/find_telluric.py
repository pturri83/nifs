"""Find a suitable telluric star near the target from a table.
"""


from os import path

from astropy.io import ascii
from matplotlib import pyplot as plt
import numpy as np


# Parameters
folder = '/Volumes/Astro/Data/NIFS'  # Folder of the FITS table file
file = 'hipparcos.dat'  # Filename of the FITS table
spectr = "A0"  # Spectral type
mag_lim = 8  # Maximum V magnitude
stars = [45167, 48414, 43234]  # Names of stars of interest

# Load file
tab = ascii.read(path.join(folder, file))

# Select valid stars
idx_spectr = np.array([spectr not in i for i in tab["Spectral type"]])
idx_mag = tab["Vmag"] > mag_lim
tab.remove_rows(idx_spectr | idx_mag)

# Plot telluric stars
dist = tab["distance"] / 60
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
ax.set_position([0.15, 0.1, 0.7, 0.7])
plt.scatter(tab["Vmag"], dist, c='y')
ax.set_ylim([0, ax.get_ylim()[1]])

for i_star in range(len(tab)):
    ax.annotate(tab[i_star]["HIP catalog number"],
                [tab[i_star]["Vmag"], dist[i_star]], xytext=[-15, 5],
                textcoords='offset points')

plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
ax.set_xlabel("V (mag)", fontsize=20, labelpad=0)
ax.set_ylabel("distance (deg)", fontsize=20, labelpad=10)
plt.title("Telluric stars ({}-type)".format(spectr), y=1.1, fontsize=30)

# Print stars of interest
print()

for star in stars:
    i_star = np.where(tab["HIP catalog number"] == star)[0][0]
    print("HIP{0}:   Type {1},   V = {2:.2f},   distance = {3:.2f} deg".format(
          star, tab[i_star]["Spectral type"].strip(), tab[i_star]["Vmag"],
          dist[i_star]))

print()
