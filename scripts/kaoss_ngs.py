"""Program to find which target in a FITS table of KAOSS targets has a suitable
TTS for ALTAIR.
The VizieR service is used with the UCAC4 [1] and PPMXL [2] catalogs to find
distances and R magnitudes. These are the same catalogs used by the Gemini OT
for NIFS [3]. The UCAC4 catalog has V and r magnitudes. The r filter is used in
place of R; if no r is available, the V band is used with a conservative color
(magnitude difference) between the two. The PPMXL catalog has r1 and r2
magnitudes; if the former is not available, the latter is used in place of R.
The parameters for the TTS can be chosen from the ALTAIR webpage [4].
Images of the neighborhood of the targets are generated from the PanSTARRS DR1
r-band survey [5].

References:
    [1] https://vizier.cds.unistra.fr/viz-bin/VizieR?-source=I/322A
    [2] https://vizier.cds.unistra.fr/viz-bin/VizieR?-source=I/317
    [3] https://www.gemini.edu/instrumentation/nifs/observation-preparation#OT
    [4] http://www.gemini.edu/instrumentation/altair/modes#Performance
    [5] https://aladin.u-strasbg.fr/hips/list
"""


import os
import shutil
import warnings

from astropy import coordinates, table, units
from astroquery import vizier
import numpy as np

import misc_nifs


# File parameters
kaoss_directory = '/Volumes/Astro/Data/KAOSS'  # Data folder
kaoss_file = 'kaoss_targets.fits'  # Name of the list file
# subfolder
sr_mode = 'low'  # SR mode ('low' or 'high')

# TTS parameters
mag_min = 8.5  # Minimum TTS magnitude limit (R band)
mag_max_low = 17.8  # Maximum TTS magnitude limit (R band) for high-SR guiding
mag_max_high = 15  # Maximum TTS magnitude limit (R band) for high-SR guiding
rad_low = 25  # TTS distance limit for low-SR guiding [arcsec]
rad_high = 15  # TTS distance limit for high-SR guiding [arcsec]
mag_vr = -1  # Conservative V-R color

# Plot parameters
fov = 60  # FOV side [arcsec]
width = 500  # Image width [px]
height = 500  # Image height [px]
px_ticks = 10  # Distance of the pixel axes' ticks (")
ucac4_sym = 's'  # UCAC4 stars plotting symbol
ppmxl_sym = 'D'  # PPMXL stars plotting symbol
ucac4_col = 'm'  # UCAC4 stars plotting color
ppmxl_col = 'y'  # PPMXL stars plotting color
ok_col = 'r'  # Candidate stars plotting color

# Select SR mode
if sr_mode == 'low':
    mag_max = mag_max_low
    rad = rad_low
else:
    mag_max = mag_max_high
    rad = rad_high

# Load file
in_path = os.path.join(kaoss_directory, kaoss_file)
kaoss_tab = table.Table.read(in_path)
file_p = os.path.splitext(in_path)[0]
out_path = os.path.join(os.path.dirname(file_p),
                        (file_p.split(os.sep)[-1] + "_" + sr_mode))

if os.path.exists(out_path):
    shutil.rmtree(out_path)

os.makedirs(out_path)

# Search
rad_u = rad * units.arcsecond
fov_u = fov * units.arcsecond / np.sqrt(2)
ucac4_tab = None
ppmxl_tab = None
unique_tab = None
i_star = 0
print()

for target in kaoss_tab:
    i_star += 1
    target_id = target['ID'].strip()
    print("\rTarget {0:d} / {1:d} ({2})".format(i_star, len(kaoss_tab),
                                                target_id), end="")
    coord = coordinates.SkyCoord(target['RA'], target['Dec'], unit='deg')
    ucac4_vizier = vizier.Vizier(columns=['+_r', 'UCAC4', 'RAJ2000', 'DEJ2000',
                                          'pmRA', 'pmDE', 'rmag', 'Vmag'],
                                 catalog='UCAC4')
    ppmxl_vizier = vizier.Vizier(columns=['+_r', 'PPMXL', 'RAJ2000', 'DEJ2000',
                                          'pmRA', 'pmDE', 'r1mag', 'r2mag'],
                                 catalog='PPMXL')
    warnings.simplefilter('ignore')
    ucac4_query = ucac4_vizier.query_region(coord, radius=rad_u)
    ucac4_more_query = ucac4_vizier.query_region(coord, radius=fov_u)
    ppmxl_query = ppmxl_vizier.query_region(coord, radius=rad_u)
    ppmxl_more_query = ppmxl_vizier.query_region(coord, radius=fov_u)
    warnings.simplefilter('default')
    unique_ucac4 = False
    unique_ppmxl = False
    stars = None

    if len(ucac4_query) > 0:

        ucac4_query[0].add_column(np.full((len(ucac4_query[0]), 1), target_id),
                                  name='Target ID', index=0)

        for star_idx in range(len(ucac4_query[0])):
            star = ucac4_query[0][star_idx]

            if ((star['rmag'] is not np.ma.masked) and
                (mag_min < star['rmag'] < mag_max)) or \
                    ((star['Vmag'] is not np.ma.masked) and
                     ((mag_min - mag_vr) < star['Vmag'] < (mag_max + mag_vr))):
                unique_ucac4 = True

                if ucac4_tab is None:
                    ucac4_tab = table.Table(star)
                else:
                    ucac4_tab.add_row(star)

    if len(ppmxl_query) > 0:

        ppmxl_query[0].add_column(np.full((len(ppmxl_query[0]), 1), target_id),
                                  name='Target ID', index=0)

        for star_idx in range(len(ppmxl_query[0])):
            star = ppmxl_query[0][star_idx]

            if ((star['r1mag'] is not np.ma.masked) and
                (mag_min < star['r1mag'] < mag_max)) or \
                    ((star['r2mag'] is not np.ma.masked) and
                     (mag_min < star['r2mag'] < mag_max)):
                unique_ppmxl = True

                if ppmxl_tab is None:
                    ppmxl_tab = table.Table(star)
                else:
                    ppmxl_tab.add_row(star)

    if unique_ucac4 or unique_ppmxl:
        if unique_tab is None:
            unique_tab = table.Table(names=('Target ID', 'RA', 'Dec', 'UCAC4',
                                            'PPMXL'),
                                     dtype=('U', 'f8', 'f8', '?', '?'))

        unique_tab.add_row([target_id, target['RA'], target['Dec'],
                            unique_ucac4, unique_ppmxl])

    if len(ucac4_more_query) > 0:
        stars_rows = ucac4_more_query[0]['RAJ2000', 'DEJ2000']
        mag = ucac4_more_query[0]['rmag'].data
        warnings.simplefilter('ignore')
        mag_str = ["%.1f" % i for i in mag]
        warnings.simplefilter('default')
        mag_str = [i.replace('nan', '') for i in mag_str]
        stars_rows.add_column(mag_str, name='label')
        stars_rows.add_columns([np.full((len(ucac4_more_query[0]), 1),
                                        ucac4_sym),
                                np.full((len(ucac4_more_query[0]), 1),
                                        ucac4_col)], names=['symbol', 'color'])

        if ucac4_tab:
            for i_star_row in range(len(stars_rows)):
                star_where = np.where(stars_rows['RAJ2000'][i_star_row] ==
                                      ucac4_tab['RAJ2000'])

                if target_id in ucac4_tab['Target ID'][star_where]:
                    stars_rows['color'][i_star_row] = ok_col

        if stars is None:
            stars = stars_rows
        else:
            warnings.simplefilter('ignore')
            stars = table.vstack([stars, stars_rows])
            warnings.simplefilter('default')

    if len(ppmxl_more_query) > 0:
        stars_rows = ppmxl_more_query[0]['RAJ2000', 'DEJ2000']
        mags = np.transpose(np.ma.stack([ppmxl_more_query[0]['r1mag'].data,
                                         ppmxl_more_query[0]['r2mag'].data]))
        mag = np.mean(mags, axis=1)
        warnings.simplefilter('ignore')
        mag_str = ["%.1f" % i for i in mag]
        warnings.simplefilter('default')
        stars_rows.add_column(mag_str, name='label')
        stars_rows.add_columns([np.full((len(ppmxl_more_query[0]), 1),
                                        ppmxl_sym),
                                np.full((len(ppmxl_more_query[0]), 1),
                                        ppmxl_col)], names=['symbol', 'color'])

        if ppmxl_tab:
            for i_star_row in range(len(stars_rows)):
                star_where = np.where(stars_rows['RAJ2000'][i_star_row] ==
                                      ppmxl_tab['RAJ2000'])

                if target_id in ppmxl_tab['Target ID'][star_where]:
                    stars_rows['color'][i_star_row] = ok_col

        if stars is None:
            stars = stars_rows
        else:
            warnings.simplefilter('ignore')
            stars = table.vstack([stars, stars_rows])
            warnings.simplefilter('default')

    if stars:
        stars['RAJ2000'].name = 'x'
        stars['DEJ2000'].name = 'y'

    warnings.simplefilter('ignore')
    misc_nifs.plot_aladin('CDS/P/PanSTARRS/DR1/r',
                          [target['RA'], target['Dec']], fov, [width, height],
                          px_ticks, target_id, rad_fov=rad, catalog=stars,
                          c_map='gray',
                          save=os.path.join(out_path,
                                            (target_id + "_map.png")))
    warnings.simplefilter('default')

print()

# Save files
if ucac4_tab is not None:
    ucac4_tab.meta.clear()
    ucac4_tab.write(os.path.join(out_path, 'ucac4.fits'), format='fits',
                    overwrite=True)

if ppmxl_tab is not None:
    ppmxl_tab.meta.clear()
    ppmxl_tab.write(os.path.join(out_path, 'ppmxl.fits'), format='fits',
                    overwrite=True)

if unique_tab is not None:
    unique_tab.meta.clear()
    unique_tab.write(os.path.join(out_path, 'unique.fits'), format='fits',
                     overwrite=True)

print()
