"""Analyze the NIFS data cubes of the single exposures of 084933A.

References:
    [ref1] Greisen et Calabretta, 2002
"""


import copy
import datetime
import glob
import pickle
import re
from os import makedirs, pardir, path

from astropy.io import fits
from matplotlib import dates, legend_handler, pyplot as plt
import numpy as np

from source import cube_nifs, fit_nifs, params_nifs


# Data parameters
root_folder = '/Volumes/Astro/Data/NIFS/'  # Root data folder
target_folder = '084933A'  # Target subfolder
data_combine = [[2017, 'H', '084933a_17a', '20170407_obs57', 1, 'center',
                 [0, 8]],
                [2017, 'K', '084933a_17a', '20170407_obs49', 1, 'max',
                 [0, 7]],
                [2017, 'K', '084933a_17a', '20170410_obs71', 2, 'max',
                 [0, 4, 8]],
                [2019, 'K', '084933a_19b', '20200103_obs66', 1, 'max',
                 [0, 14]],
                [2021, 'K', '084933a_21b', '20220224_obs86', 1, [22, 35],
                 [0, 3, 6]],
                [2021, 'K', '084933a_21b', '20220226_obs145', 2, 'max',
                 [0, 6]]]
# Data subfolder ([year, band, semester, observation, observation number, peak
# guess, drift chunks])

# Analysis parameters
rad = 0.2  # Radius of the analyzed beam (")
stack_range_list = [[1.55, 1.75], [2.05, 2.35]]  # Range of wavelengths to
# stack [H, K] (um)
clean_std = 3  # Number of STDs to clean spectra before collapsing data cubes

# Graphic parameters
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (")
max_fwhm = 0.5  # Maximum FWHM plotted (")
max_fwhm_seeing = 0.6  # Maximum FWHM plotted (with seeing) (")

# Load excluded files
exclude_file = open(path.join(root_folder, 'exclude.lst'), 'r')
exclude_lines = exclude_file.readlines()
exclude_file.close()
exclude_lines = [line.strip('\n') for line in exclude_lines]

# Analyze all data
print("\nProgram started")

for i_data in range(len(data_combine)):

    # Find data
    year = data_combine[i_data][0]
    band = data_combine[i_data][1]
    folder = data_combine[i_data][2]
    file = data_combine[i_data][3]
    cube_n = data_combine[i_data][4]
    pos_guess = data_combine[i_data][5]
    chunks = data_combine[i_data][6]
    print("Year {0}, {1} band, observation #{2}".format(year, band, cube_n))
    complete_folder = path.join(root_folder, folder, target_folder,
                                'Merged_telCorAndFluxCalibrated')
    find_files = glob.glob(path.join(complete_folder, file, 'factfbrsn*.fits'))
    save_folder = path.join((path.abspath(path.join(complete_folder, pardir,
                                                    pardir))),
                            'plots', 'single')
    save_folder_offset = path.join((path.abspath(path.join(complete_folder,
                                                           pardir, pardir))),
                                   'offset')

    if not path.exists(save_folder):
        makedirs(save_folder)

    if not path.exists(save_folder_offset):
        makedirs(save_folder_offset)

    if band == 'H':
        stack_range = stack_range_list[0]
        lambda_c = params_nifs.lambda_h
    else:
        stack_range = stack_range_list[1]
        lambda_c = params_nifs.lambda_k

    # Exclude files
    find_lines = [path.basename(line) for line in find_files]
    for exclude_line in exclude_lines:
        if exclude_line in find_lines:
            exclude_idx = find_lines.index(exclude_line)
            find_files = np.delete(find_files, exclude_idx)

    # Analyze exposures
    n_files = len(find_files)
    fwhm_m = np.zeros(n_files)
    ell_m = np.zeros(n_files)
    pa_m = np.zeros(n_files)
    times = np.zeros(n_files)
    target_x_m = np.zeros(n_files)
    target_y_m = np.zeros(n_files)
    fwhm_header = np.zeros(n_files)
    fwhm_header_lambda = np.zeros(n_files)
    sps = 0
    date_str_all = ""
    exp_time = 0

    for i_file in range(n_files):

        # Load file
        print("\r   Analyzing exposure # {0}/{1} ...".format((i_file + 1),
                                                             n_files), end="")
        folder_file = find_files[i_file]
        hdul = fits.open(folder_file)
        header_0 = hdul[0].header
        header_1 = hdul[1].header
        data = hdul[1].data
        hdul.close()
        data_shape = data.shape

        # Read header
        sps = header_1['CD1_1']
        date_str = header_0['DATE-OBS']
        time_str = header_0['UT']
        exp_time = header_0['EXPTIME']

        if i_file == 0:
            date_str_all = date_str

        if year != 2017:
            fwhm_header[i_file] = header_0['AOSEEING']
            fwhm_header_lambda[i_file] = fwhm_header[i_file] * lambda_c / \
                params_nifs.lambda_seeing

        # Calculate time
        date_list = re.split('-', date_str)
        date_num = [int(i) for i in date_list]
        time_list = re.split('[:.]', time_str)
        time_num = [int(i) for i in time_list]
        date_obj = datetime.datetime(date_num[0], date_num[1], date_num[2],
                                     time_num[0], time_num[1], time_num[3])
        times[i_file] = dates.date2num(date_obj)

        # Calculate wavelengths
        slice_idx = np.arange(data_shape[0])
        wvl = ((slice_idx + 1 - header_1['CRPIX3']) * header_1['CD3_3']) + \
            header_1['CRVAL3']  # [ref1]
        wvl /= 10000

        # Clean and collapse data
        wvl_slice, data_slice = cube_nifs.cut_cube(wvl, data, stack_range[0],
                                                   stack_range[1])
        data_clean = copy.deepcopy(data_slice)

        for i_row in range(data_shape[1]):
            for i_col in range(data_shape[2]):
                for i_pass in [1, 2]:
                    spect = data_clean[:-1, i_row, i_col] - \
                            data_clean[1:, i_row, i_col]
                    spect_std = np.std(spect)
                    spect_abs = np.abs(spect)
                    spect_where = np.where(spect_abs[: -1] >
                                           (clean_std * spect_std))[0]
                    data_clean[(spect_where + 1), i_row, i_col] = 0

        data_coll = cube_nifs.collapse_cube(data_clean)
        data_coll *= stack_range[1] - stack_range[0]

        # Show uncleaned collapsed cube
        data_coll_plot = data_coll / (sps ** 2)
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           c_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           title=("Collapsed cube\n (" + str(year) + ", " +
                                  band + " band, obs #" + str(cube_n) +
                                  ", exp #" + str(i_file + 1) + ")"),
                           perc=0.5, compass=True,
                           save=path.join(save_folder,
                                          ('coll_' + band + '_' + str(cube_n) +
                                           '_' + str(i_file + 1) +
                                           '_unclean.png')))

        # Clean data
        fit_psf_m_tmp, _ = fit_nifs.moffat2d_fit(data_coll,
                                                 pos_guess=pos_guess)
        data_coll = cube_nifs.clean_map(data_coll, fit_psf_m_tmp[2: 4],
                                        ps=sps, ticks=sp_ticks,
                                        scale=(sps ** -2),
                                        save=path.join(save_folder,
                                                       ('coll_' + band + '_' +
                                                        str(cube_n) + '_' +
                                                        str(i_file + 1) +
                                                        '_filter.png')))

        # Show collapsed cube
        data_coll_plot = data_coll / (sps ** 2)
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           c_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           title=("Collapsed cube\n (" + str(year) + ", " +
                                  band + " band, obs #" + str(cube_n) +
                                  ", exp #" + str(i_file + 1) + ")"),
                           perc=0.5, compass=True,
                           save=path.join(save_folder,
                                          ('coll_' + band + '_' + str(cube_n) +
                                           '_' + str(i_file + 1) + '.png')))

        # Measure FWHM
        fit_psf_m, _ = fit_nifs.moffat2d_fit(data_coll, pos_guess=pos_guess)
        target_x_m[i_file] = fit_psf_m[2]
        target_y_m[i_file] = fit_psf_m[3]
        s_m = np.sqrt(fit_psf_m[4] * fit_psf_m[5])
        fwhms_m = 2 * np.array(fit_psf_m[4: 6]) * \
            np.sqrt((2 ** (1 / fit_psf_m[7])) - 1) * sps
        fwhm_m[i_file] = np.sqrt(fwhms_m[0] * fwhms_m[1])
        fwhm_m_max = np.max(fwhms_m)
        fwhm_m_min = np.min(fwhms_m)
        ell_m[i_file] = (fwhm_m_max - fwhm_m_min) / fwhm_m_max
        pa_m[i_file] = fit_psf_m[6]

        # Show collapsed cube with fit
        data_coll_plot = data_coll / (sps ** 2)
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           model=[fit_psf_m[2], fit_psf_m[3],
                                  (fwhms_m[0] / sps), (fwhms_m[1] / sps),
                                  np.rad2deg(pa_m[i_file]), fwhm_m[i_file]],
                           c_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           title=("Collapsed cube\n (" + str(year) + ", " +
                                  band + " band, obs #" + str(cube_n) +
                                  ", exp #" + str(i_file + 1) + ")"),
                           perc=0.5, compass=True,
                           save=path.join(save_folder,
                                          ('coll_fwhm_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show collapsed cube with fit for printing
        cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                           model=[fit_psf_m[2], fit_psf_m[3],
                                  (fwhms_m[0] / sps), (fwhms_m[1] / sps),
                                  np.rad2deg(pa_m[i_file]), fwhm_m[i_file]],
                           c_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           perc=0.5, compass=True, legend=False,
                           save=path.join(save_folder,
                                          ('print_coll_fwhm_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

        # Show radial profile with fit
        peak_plot = fit_psf_m[0] / (sps ** 2)
        bkg_plot = fit_psf_m[1] / (sps ** 2)
        cube_nifs.plot_rad(data_coll_plot, sps, fit_psf_m[2: 4],
                           ("collapsed cube (" + str(year) + ", " + band +
                            " band, obs #" + str(cube_n) + ", exp #" +
                            str(i_file + 1) + ")"),
                           model_type='moffat',
                           model_data=[peak_plot, s_m, fit_psf_m[7], bkg_plot,
                                       fwhm_m[i_file]], zoom=0.5,
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           save=path.join(save_folder,
                                          ('rad_m_' + band + '_' + str(cube_n)
                                           + '_' + str(i_file + 1) + '.png')))

        # Show radial profile with fit for printing
        cube_nifs.plot_rad(data_coll_plot, sps, fit_psf_m[2: 4], None,
                           model_type='moffat',
                           model_data=[peak_plot, s_m, fit_psf_m[7], bkg_plot,
                                       fwhm_m[i_file]], zoom=0.5,
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           fwhm=False, legend=False,
                           save=path.join(save_folder,
                                          ('print_rad_m_' + band + '_' +
                                           str(cube_n) + '_' + str(i_file + 1)
                                           + '.png')))

    print()

    # Calculate target drift
    delta_times = times - times[0]
    delta_target_x_m = target_x_m - target_x_m[0]
    delta_target_y_m = target_y_m - target_y_m[0]
    new_x_m = np.zeros(n_files)
    new_y_m = np.zeros(n_files)

    for i_chunk in range(len(chunks) - 1):
        chunks_img = np.arange(chunks[i_chunk], (chunks[i_chunk + 1]))
        drift_fit_x = np.polyfit(delta_times[chunks_img],
                                 delta_target_x_m[chunks_img], 1)
        drift_fit_y = np.polyfit(delta_times[chunks_img],
                                 delta_target_y_m[chunks_img], 1)
        new_x_m[chunks_img] = (delta_times[chunks_img] * drift_fit_x[0]) + \
            drift_fit_x[1]
        new_y_m[chunks_img] = (delta_times[chunks_img] * drift_fit_y[0]) + \
            drift_fit_y[1]
        vel_x = drift_fit_x[0] * sps / (24 * 60 * 60)
        vel_y = drift_fit_y[0] * sps / (24 * 60 * 60)
        vel = np.hypot(vel_x, vel_y)
        drift = vel * exp_time
        print("   Sequence {0}:".format(i_chunk + 1))
        print("      Drift speed (x): {0:.2e} \"/s".format(vel_x))
        print("      Drift speed (y): {0:.2e} \"/s".format(vel_y))
        print("      Drift speed: {0:.2e} \"/s".format(vel))
        print("      Drift during exposure: {0:.2e}\"".format(drift))

    # Show target drift
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    label1 = None
    label2 = None
    plt1 = None
    plt2 = None
    plt3 = None
    plt4 = None

    for i_chunk in range(len(chunks) - 1):
        if i_chunk == 0:
            label1 = "x"
            label2 = "y"

        chunks_img = np.arange(chunks[i_chunk], (chunks[i_chunk + 1]))
        plt1, = ax.plot_date(times[chunks_img],
                             (delta_target_x_m[chunks_img] * sps), 'sb',
                             label=label1)
        plt2, = ax.plot_date(times[chunks_img],
                             (delta_target_y_m[chunks_img] * sps), 'or',
                             label=label2)
        plt3, = ax.plot_date(times[chunks_img], (new_x_m[chunks_img] * sps),
                             '--b')
        plt4, = ax.plot_date(times[chunks_img], (new_y_m[chunks_img] * sps),
                             ':r')

    formatter = dates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(rotation=30)
    ax.set_xlabel("UT")
    ax.set_ylabel("Position (\")")
    plt.title(("Position drift\n (" + str(year) + ", obs #" + str(cube_n) +
               ")"), y=1.03)
    plt.legend([(plt1, plt3), (plt2, plt4)], [label1, label2],
               handler_map={plt1: legend_handler.HandlerLine2D(numpoints=1),
                            plt2: legend_handler.HandlerLine2D(numpoints=1)},
               loc='lower left')
    plt.savefig(path.join(save_folder,
                          ('drift_' + band + '_' + str(cube_n) + '.png')))
    plt.savefig(path.join(save_folder,
                          ('drift_' + band + '_' + str(cube_n) + '.pdf')))
    plt.close()

    # Show target drift for printing
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.2, 0.7, 0.7])

    for i_chunk in range(len(chunks) - 1):
        if i_chunk == 0:
            label1 = "x"
            label2 = "y"
        else:
            label1 = '_nolegend_'
            label2 = '_nolegend_'

        chunks_img = np.arange(chunks[i_chunk], (chunks[i_chunk + 1]))
        plt1, = ax.plot_date(times[chunks_img],
                             (delta_target_x_m[chunks_img] * sps), 'sb',
                             label=label1)
        plt2, = ax.plot_date(times[chunks_img],
                             (delta_target_y_m[chunks_img] * sps), 'or',
                             label=label2)
        plt3, = ax.plot_date(times[chunks_img], (new_x_m[chunks_img] * sps),
                             '--b')
        plt4, = ax.plot_date(times[chunks_img], (new_y_m[chunks_img] * sps),
                             ':r')

    formatter = dates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(rotation=30)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.set_xlabel("UT", fontsize=16)
    ax.set_ylabel("Position (\")", fontsize=16)
    plt.title((date_str_all + ", " + band + " grating"), y=1.02, fontsize=16,
              fontweight='bold')

    if i_data == 0:
        plt.legend([(plt1, plt3), (plt2, plt4)], [label1, label2],
                   handler_map={
                       plt1: legend_handler.HandlerLine2D(numpoints=1),
                       plt2: legend_handler.HandlerLine2D(numpoints=1)},
                   fontsize=16, loc='lower left')

    plt.savefig(path.join(save_folder,
                          ('print_drift_' + band + '_' + str(cube_n) +
                           '.png')))
    plt.savefig(path.join(save_folder,
                          ('print_drift_' + band + '_' + str(cube_n) +
                           '.pdf')))
    plt.close()

    # Save offset
    offset_x = new_x_m + target_x_m[0]
    offset_y = new_y_m + target_y_m[0]
    offset = [offset_x, offset_y]
    file_out = open(path.join(save_folder_offset,
                              ('offset_' + band + '_' + str(cube_n) + '.pkl')),
                    'wb')
    pickle.dump(offset, file_out)
    file_out.close()

    # Calculate FWHM
    fwhm_m_mean = np.mean(fwhm_m)
    fwhm_m_min = np.min(fwhm_m)
    fwhm_m_max = np.max(fwhm_m)
    print("   Minimum FWHM: {0:.3f}\"".format(fwhm_m_min))
    print("   Maximum FWHM: {0:.3f}\"".format(fwhm_m_max))
    print("   Mean FWHM: {0:.3f}\"".format(fwhm_m_mean))
    fwhm_header_mean = 0

    if year == 2019:
        fwhm_header_mean = np.mean(fwhm_header)
        print("   Mean seeing ({0:.1f} um): {1:.3f}\"".format(
            params_nifs.lambda_seeing, fwhm_header_mean))

    # Calculate position angles
    for i_angle in range(n_files - 1):
        if pa_m[i_angle + 1] - pa_m[i_angle] > (np.pi / 2):
            pa_m[i_angle + 1] -= np.pi
        elif pa_m[i_angle + 1] - pa_m[i_angle] < (-np.pi / 2):
            pa_m[i_angle + 1] += np.pi

    # Show FWHM
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.plot_date(times, fwhm_m, '-om',
                 label=(r"AO ($\overline{FWHM}$" +
                        "={0:.3f}\")".format(fwhm_m_mean)))

    if year == 2019:
        ax.plot_date(times, fwhm_header, '-og',
                     label=(r"Seeing ({0:.1f} $\mu$m) ".format(
                         params_nifs.lambda_seeing) + r"($\overline{FWHM}$" +
                            "={0:.3f}\")".format(fwhm_header_mean)))

    ax.set_ylim(0, max_fwhm_seeing)
    formatter = dates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(rotation=30)
    ax.set_xlabel("UT")
    ax.set_ylabel("FWHM (\")")
    plt.title(("PSF FWHM\n (" + str(year) + ", " + band + " band, obs #" +
               str(cube_n) + ")"), y=1.03)
    ax.legend()
    plt.savefig(path.join(save_folder,
                          ('fwhm_m_' + band + '_' + str(cube_n) + '.png')))
    plt.savefig(path.join(save_folder,
                          ('fwhm_m_' + band + '_' + str(cube_n) + '.pdf')))
    plt.close()

    # Show FWHM for printing
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111)
    ax.set_position([0.15, 0.15, 0.7, 0.7])
    ax.plot_date(times, fwhm_m, '-om')
    ax.set_ylim(0, max_fwhm)
    formatter = dates.DateFormatter('%H:%M:%S')
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_tick_params(rotation=30)
    ax.set_xlabel("UT")
    ax.set_ylabel("FWHM (\")")
    plt.title((date_str_all + ", " + band + " grating"), y=1.03)
    plt.savefig(path.join(save_folder,
                          ('print_fwhm_m_' + band + '_' + str(cube_n) +
                           '.png')))
    plt.savefig(path.join(save_folder,
                          ('print_fwhm_m_' + band + '_' + str(cube_n) +
                           '.pdf')))
    plt.close()

    # Show elongation
    fig = plt.figure(figsize=(7, 5))
    ax1 = fig.add_subplot(111)
    ax1.set_position([0.15, 0.15, 0.7, 0.7])
    ax1.plot_date(times, ell_m, '-ob')
    ylim = ax1.get_ylim()
    plt.ylim(0, ylim[1])
    ax1.xaxis.set_major_formatter(formatter)
    ax1.xaxis.set_tick_params(rotation=30)
    ax1.tick_params(axis='y', labelcolor='b')
    ax1.set_xlabel("UT")
    ax1.set_ylabel("Ellipticity", c='b')
    ax2 = ax1.twinx()
    ax2.set_position([0.15, 0.15, 0.7, 0.7])
    ax2.plot_date(times, pa_m, '-or')
    ax2.tick_params(axis='y', labelcolor='r')
    ax2.set_ylabel("Angle (rad)", c='r')
    plt.title("PSF Elongation\n (" + str(year) + ", " + band + " band, obs #" +
              str(cube_n) + ")")
    plt.savefig(path.join(save_folder,
                          ('elong_m_' + band + '_' + str(cube_n) + '.png')))
    plt.savefig(path.join(save_folder,
                          ('elong_m_' + band + '_' + str(cube_n) + '.pdf')))
    plt.close()

print("Program terminated\n")
