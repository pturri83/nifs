"""Calculate the encircled energy of a univariate Moffat profile.
"""


from matplotlib import pyplot as plt
import numpy as np

from source import fit_nifs


# Data parameters
alpha = 2.5  # Exponent of the Moffat function
fwhm = 0.7  # FWHM (in any unit)

# Result parameters
rad = 0.5  # Radius for calculating a specific EE (in the same units as 'fwhm')

# Calculate Moffat
print("\nProgram started")
s = 10  # Profile width (px)
side_s = 20  # Domain width (times s)
side = (((s * side_s) // 2) * 2) + 1
center = (side - 1) / 2
side_xy = np.arange(side) - center
x, y = np.meshgrid(side_xy, side_xy)
moff = fit_nifs.moffat2d_prof(x, y, s1=s, s2=s, theta=0, alpha=alpha)
moff /= np.sum(moff)

# Calculate EE
r = np.arange(center)
dist = np.hypot(x, y)
ee = np.zeros(len(r))

for i_r in r:
    dist_where = np.where(dist <= i_r)
    ee[int(i_r)] = np.sum(moff[dist_where])

fwhm_px = fit_nifs.moffat_fwhm(s, alpha)
r *= fwhm / fwhm_px
ee_r_where = np.where(np.abs(r - rad) == np.min(np.abs(r - rad)))[0][0]
ee_r = ee[ee_r_where]

# Return results
print("EE at {0}: {1:.3f}".format(rad, ee_r))
fig = plt.figure(figsize=(6, 5))
ax = fig.add_subplot(111)
ax.set_position([0.15, 0.15, 0.7, 0.7])
ax.plot(r, ee, color='b')
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
ax.set_xlabel("r", fontsize=16)
ax.set_ylabel("EE", fontsize=16)
print("Program terminated\n")
