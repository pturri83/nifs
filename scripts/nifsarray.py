"""Create a 'nifsarray.fits' file with the correct bias voltage for program
years 2017 and 2019, used in the Nifty pipeline.
"""


from os import path

from astropy.io import fits
import numpy as np


# Parameters
folder = \
    '/Users/paolo/opt/anaconda3/envs/geminiconda/iraf_extern/gemini/nifs/data'
# Folder of the file with the NIFS detector parameters
file = 'nifsarray'  # File with the NIFS detector parameters (without .fits
# extension)

# Load file
hdul = fits.open(path.join(folder, (file + '_orig.fits')))

# Modify file
hdul[1].data = np.concatenate((hdul[1].data, hdul[1].data), axis=0)
hdul[1].data[1][0] = 3.2991452991453

# Save file
hdul.writeto(path.join(folder, (file + '.fits')), overwrite='True')
hdul.close()
