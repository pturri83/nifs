"""Analyze a NIFS H band spectrum of 084933A.

References:
    [ref1] Greisen et Calabretta, 2002
"""


from os import makedirs, path
import pickle

from astropy.io import fits
import numpy as np

from source import cube_nifs, fit_nifs, params_nifs


# Data parameters
data_f = '/Volumes/Astro/Data/NIFS/stack/stack_H.fits'  # Data folder and file
plot_p = '/Volumes/Astro/Data/NIFS/stack/plots/H'  # Plots parent folder

# Analysis parameters
rad_m = 0.5  # Radius of the main beam (arcsec)
rad_s = 0.5  # Radius of a secondary beam (arcsec)
rad_phys = 0.45  # Radius of the beam to calculate the physical properties
# (arcsec)
h_range = [1.55, 1.77]  # Range of wavelengths to fit and stack and plot (um)
bin_wvl = 3  # Wavelength binning factor
bin_spxs = [2, 3, 4]  # Spaxel binning factors
conv_spx = 2  # Radius of the convolution kernel (px)
n_slices = 24  # Number of slices in azimuthal profiles
dist_range = [0.5, 1]  # Range of distances in radial and azimuthal profiles
# ([min, max]) (arcsec)
fit_range = [1.69, 1.73]  # Range of wavelengths to fit the profile
# (min, max) (um)
bb_0_range = [1.59, 1.63]  # Range of wavelengths to measure the black body
# offset (min, max) (um)
fit_b_beam = True  # Fit a broad line on the beams
fit_b_map = False  # Fit a broad line on the maps

# Graphic parameters
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (arcsec)
i_range = [-2e-14, 1e-13]  # Spectrum intensity limits of main and secondary
# beams (erg/s/cm^2/um)
i_range_sec = [-3e-14, 5e-14]  # Spectrum intensity limits of secondary only
# beams (erg/s/cm^2/um)
i_range_res = [-3e-14, 5e-14]  # Residual spectrum intensity limits of main and
# secondary beams (erg/s/cm^2/um)
h_range_plot = [1.55, 1.77]  # Range of wavelengths to print the main beam (um)
h_range_grid = [1.6, 1.73]  # Range of wavelengths for the grid (um)
flux_dens_range_az = [-1e-14, 2e-14]  # Range of flux densities in azimuthal
# plots (min, max) (erg/s/cm^2/um/"^2)
flux_range_az = [-2e-16, 4e-16]  # Range of fluxes in azimuthal plots (min,
# max) (erg/s/cm^2/"^2)
flux_dens_range_az_bin = [-2e-14, 1e-14]  # Range of flux densities in binned
# azimuthal plots (min, max) (erg/s/cm^2/um/"^2)
flux_range_az_bin = [-2e-16, 3e-16]  # Range of fluxes in binned azimuthal
# plots (min, max) (erg/s/cm^2/"^2)
bin_grids = 8  # Side of grid of contiguous spectra
i_range_grid = [-1e-15, 4e-15]  # Spectrum flux density limits for the grid of
# contiguous spectra (erg/s/cm^2/um)
h_range_s = [1.55, 1.77]  # Range of wavelengths to print secondary beams (um)
max_r = 1  # Maximum distance plotted in radial profiles (")

# Manage files
print("\nProgram started")
plot_f = path.join(plot_p, 'spectrum')
fits_f = path.join(plot_p, 'spectrum', 'fits')

if not path.exists(plot_f):
    makedirs(plot_f)

if not path.exists(fits_f):
    makedirs(fits_f)

# Load data
print("Preparing data ...")
hdul = fits.open(data_f)
header = hdul[1].header
data = hdul[1].data
hdul.close()
data_shape = data.shape
sps = header['CD1_1']

# Calculate wavelengths
slice_idx = np.arange(data_shape[0])
wvl = ((slice_idx + 1 - header['CRPIX3']) * header['CD3_3']) + header['CRVAL3']
# [ref1]
wvl /= 10000
sky = params_nifs.sky_compile(params_nifs.sky_c, params_nifs.sky_w[0])

# Integrate flux density
wvl_steps = wvl[1:] - wvl[:-1]
wvl_step = np.mean(wvl_steps)
data /= wvl_step

# Clean data
wvl_clean, data_clean = cube_nifs.cut_cube(wvl, data, h_range[0], h_range[1])
wvl_clean, data_clean = cube_nifs.clean_cube(wvl_clean, data_clean,
                                             params_nifs.sky_c,
                                             params_nifs.sky_w[0])

# Select beams
data_coll = cube_nifs.collapse_cube(data_clean)
data_coll *= np.max(wvl_clean) - np.min(wvl_clean)
fit_psf_m, _ = fit_nifs.moffat2d_fit(data_coll, pos_guess='max')
direction_tmp = fit_psf_m[2: 4]
direction_m = [direction_tmp[0], direction_tmp[1], (rad_m / sps)]
beam_m = cube_nifs.beam(data_clean, direction_m[0: -1], (rad_m / sps))
beam_bin_m = cube_nifs.bin_spx(data_clean, beam_m[1], beam_m[0])
beam_bin_m_plot = cube_nifs.bin_spx(data, beam_m[1], beam_m[0])
direction_s = np.zeros([4, 3])
direction_offset = [[-1, -1], [-1, 1], [1, -1], [1, 1]]
direction_label = ["Center", "SW", "NW", "SE", "NE"]
direction_suffix = ['c', 'sw', 'nw', 'se', 'ne']
beam_s = [np.array([])] * 4
beam_bin_s = [np.array([])] * 4
beam_bin_s_plot = [np.array([])] * 4
dist_s = rad_m + rad_s

for i_dir in range(len(direction_offset)):
    direction_s[i_dir, 0] = (direction_tmp[0] +
                             (direction_offset[i_dir][0] * dist_s / sps /
                              np.sqrt(2)))
    direction_s[i_dir, 1] = (direction_tmp[1] +
                             (direction_offset[i_dir][1] * dist_s / sps /
                              np.sqrt(2)))
    direction_s[i_dir, 2] = rad_s / sps
    beam_s[i_dir] = cube_nifs.beam(data_clean, direction_s[i_dir, :-1],
                                   direction_s[i_dir, 2])
    beam_bin_s[i_dir] = cube_nifs.bin_spx(data_clean, beam_s[i_dir][0],
                                          beam_s[i_dir][1])
    beam_bin_s_plot[i_dir] = cube_nifs.bin_spx(data, beam_s[i_dir][0],
                                               beam_s[i_dir][1])

beam_circ = np.vstack([direction_m, direction_s])
beam_bin = np.vstack([beam_bin_m, beam_bin_s])
beam_bin_plot = np.vstack([beam_bin_m_plot, beam_bin_s_plot])

# Show beams
data_coll_plot = data_coll / (sps ** 2)
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, beam_circ=beam_circ,
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   title="Collapsed cube with beams",
                   save=path.join(plot_f, 'coll_beams.png'))

# Show beams for printing
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks, beam_circ=beam_circ,
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   save=path.join(plot_f, 'print_coll_beams.png'))

# Fit the main beam
print("\nAnalyzing primary beam ...\n")
h_fit_m = fit_nifs.fit_h(wvl_clean, beam_bin[0], fit_range, bb_0_range,
                         fit_b=fit_b_beam)

# Show the main beam spectrum zoomed, with fitted spectrum
cube_nifs.plot_spectrum(wvl, beam_bin_plot[0], sky=sky, fit_mod=h_fit_m,
                        zoom=h_range_plot, i_range=i_range,
                        i_range_res=i_range_res,
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        title="Beam spectrum fitted ({0})".format(
                            direction_label[0]),
                        save=path.join(plot_f,
                                       ('beam_spectr_fit_' +
                                        direction_suffix[0] + '.png')))

# Show the main beam spectrum zoomed, with fitted spectrum, for printing
cube_nifs.plot_spectrum(wvl, beam_bin_plot[0], sky=sky, fit_mod=h_fit_m,
                        zoom=h_range_plot, i_range=i_range,
                        i_range_res=i_range_res,
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       ('print_beam_spectr_fit_' +
                                        direction_suffix[0] + '.png')))

# Bin the main beam spectrum by wavelength
wvl_bin2_m_plot, beam_bin2_m_plot = cube_nifs.bin_spectrum(wvl,
                                                           beam_bin_plot[0],
                                                           bin_wvl)

# Show the main beam spectrum zoomed, binned by wavelength, with fitted
# spectrum
cube_nifs.plot_spectrum(wvl_bin2_m_plot, beam_bin2_m_plot, sky=sky,
                        fit_mod=h_fit_m, zoom=h_range_plot, i_range=i_range,
                        i_range_res=i_range_res,
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        title="Binned beam spectrum fitted ({0})".format(
                            direction_label[0]),
                        save=path.join(plot_f,
                                       ('beam_spectr_fit_' +
                                        direction_suffix[0] + '_bin.png')))

# Analyze secondary beams
for i_dir in range(1, beam_circ.shape[0]):

    # Prepare secondary beam spectrum for fitting
    print("\rAnalyzing secondary beams ({0}/{1}) ...".format(
        i_dir, (beam_circ.shape[0] - 1)), end="")

    # Fit the secondary beam
    h_fit_s = fit_nifs.fit_h(wvl_clean, beam_bin[i_dir], fit_range,
                             bb_0_range, fit_b=fit_b_beam)

    # Show the secondary beam spectrum zoomed, with fitted spectrum
    cube_nifs.plot_spectrum(wvl, beam_bin_plot[i_dir], sky=sky,
                            fit_mod=h_fit_s, zoom=h_range_plot,
                            i_range=i_range, i_range_res=i_range_res,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            title="Beam spectrum fitted ({0})".format(
                                direction_label[i_dir]),
                            save=path.join(plot_f,
                                           ('beam_spectr_fit_' +
                                            direction_suffix[i_dir] + '.png')))

    # Bin the secondary beam spectrum by wavelength
    wvl_bin2_s_plot, beam_bin2_s_plot = \
        cube_nifs.bin_spectrum(wvl, beam_bin_plot[i_dir], bin_wvl)

    # Show the main beam spectrum zoomed, binned by wavelength, with fitted
    # spectrum
    cube_nifs.plot_spectrum(wvl_bin2_s_plot, beam_bin2_s_plot, sky=sky,
                            fit_mod=h_fit_s, zoom=h_range_plot,
                            i_range=i_range, i_range_res=i_range_res,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            title="Binned beam spectrum fitted ({0})".format(
                                direction_label[i_dir]),
                            save=path.join(plot_f,
                                           ('beam_spectr_fit_' +
                                            direction_suffix[i_dir] +
                                            '_bin.png')))

    # Show the secondary beam spectrum zoomed, binned by wavelength, with
    # fitted spectrum, for printing
    cube_nifs.plot_spectrum(wvl_bin2_s_plot, beam_bin2_s_plot, sky=sky,
                            fit_mod=h_fit_s, zoom=h_range_s, i_range=i_range,
                            i_range_res=i_range_res,
                            y_label=("Flux density (" +
                                     params_nifs.flux_dens_unit + ")"),
                            save=path.join(plot_f,
                                           ('print_beam_spectr_fit_' +
                                            direction_suffix[i_dir] +
                                            '_bin.png')))

print()

# Show physical beam
print("\nAnalyzing physical beam ...")
direction_phys = np.array(direction_m)
direction_phys[2] = rad_phys / sps
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                   beam_circ=np.expand_dims(direction_phys, 0),
                   c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                   perc=0.5, compass=True,
                   title="Collapsed cube with beam",
                   save=path.join(plot_f, 'coll_beams_phys.png'))

# Show physical beam for printing
cube_nifs.plot_img(data_coll_plot, sps, sp_ticks,
                   beam_circ=np.expand_dims(direction_phys, 0),
                   ax_labels=False, c_bar=False, perc=0.5,
                   save=path.join(plot_f, 'print_coll_beams_phys.png'))

# Show the physical beam spectrum zoomed, with fitted spectrum
beam_phys = cube_nifs.beam(data_clean, direction_phys[0: -1],
                           direction_phys[2])
beam_bin_phys = cube_nifs.bin_spx(data_clean, beam_phys[1], beam_phys[0])
beam_bin_phys_plot = cube_nifs.bin_spx(data, beam_phys[1], beam_phys[0])
h_fit_phys = fit_nifs.fit_h(wvl_clean, beam_bin_phys, fit_range, bb_0_range,
                            fit_b=fit_b_beam)
cube_nifs.plot_spectrum(wvl, beam_bin_phys_plot, sky=sky, fit_mod=h_fit_phys,
                        zoom=h_range_plot, i_range=i_range,
                        i_range_res=i_range_res,
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"), title="Beam spectrum fitted",
                        save=path.join(plot_f, 'beam_spectr_fit_phys.png'))

# Show the physical beam spectrum zoomed, with fitted spectrum, for printing
cube_nifs.plot_spectrum(wvl, beam_bin_phys_plot, sky=sky, fit_mod=h_fit_phys,
                        zoom=h_range_plot, i_range=i_range,
                        i_range_res=i_range_res,
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       'print_beam_spectr_fit_phys.png'))

# Bin the physical beam spectrum by wavelength
wvl_bin_phys, beam_bin_phys = cube_nifs.bin_spectrum(wvl, beam_bin_phys_plot,
                                                     2)

# Show the physical beam spectrum zoomed, binned by wavelength, with fitted
# spectrum, for printing
cube_nifs.plot_spectrum(wvl_bin_phys, beam_bin_phys, sky=sky,
                        fit_mod=h_fit_phys, zoom=h_range_plot, i_range=i_range,
                        i_range_res=i_range_res, label_lines='bottom',
                        y_label=("Flux density (" + params_nifs.flux_dens_unit
                                 + ")"),
                        save=path.join(plot_f,
                                       'print_beam_spectr_fit_phys_bin.png'))

# Calculate the physical beam physical characteristics
cont_phys = h_fit_phys.cont.value
o3n_i_phys = h_fit_phys.o3n_i.value
b_i_phys = h_fit_phys.b_i.value
z_phys, _, s_n_phys, s_b_phys, fwhm_n_phys, fwhm_b_phys, = \
    fit_nifs.phys_lines_h(h_fit_phys)

# Show the physical beam physical characteristics
line1 = "   Redshift: {0:.4f}".format(z_phys)
line2 = "   LOS velocity dispersion of the broad line {0:.0f} km/s".format(
    s_b_phys * 1e-3)
line3 = "   LOS velocity dispersion of narrow [OIII]: {0:.0f} km/s".format(
    s_n_phys * 1e-3)
line4 = "   LOS velocity FWHM of the broad line: {0:.0f} km/s".format(
    fwhm_b_phys * 1e-3)
line5 = "   LOS velocity FWHM of narrow [OIII]: {0:.0f} km/s".format(
    fwhm_n_phys * 1e-3)
line6 = "   Continuum density: {0:.3e} erg/s/cm^2/um".format(cont_phys)
line7 = "   Broad line integral: {0:.3e} erg/s/cm^2".format(b_i_phys)
line8 = "   Narrow [OIII] integral: {0:.3e} erg/s/cm^2".format(o3n_i_phys)
print(line1)
print(line2)
print(line3)
print(line4)
print(line5)
print(line6)
print(line7)
print(line8)

# Save the physical beam spectrum characteristics
file_txt = open(path.join(plot_f, 'spectr_phys.txt'), 'w')
file_txt.write(line1 + "\n")
file_txt.write(line2 + "\n")
file_txt.write(line3 + "\n")
file_txt.write(line4 + "\n")
file_txt.write(line5 + "\n")
file_txt.write(line6 + "\n")
file_txt.write(line7 + "\n")
file_txt.write(line8 + "\n")
file_txt.close()

# Map lines
print("\nAnalyzing maps ...")
maps = fit_nifs.fit_map_h(wvl_clean, data_clean, fit_range, bb_0_range,
                          fit_b=fit_b_map, n_spaces=3)

# Show uncleaned maps
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont',
                   save=path.join(plot_f, 'map_cont_unclean.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'o3n_i', perc=0.2,
                   save=path.join(plot_f, 'map_o3n_i_unclean.png'))

# Clean maps
print("Cleaning maps ...")
map_cont_tmp, _ = cube_nifs.grab_map_h(maps, 'cont')
fit_psf_cont_tmp, _ = fit_nifs.moffat2d_fit(map_cont_tmp, pos_guess='center')
maps = cube_nifs.clean_maps(maps, fit_psf_cont_tmp[2: 4], ps=sps,
                            ticks=sp_ticks, save=plot_f)

# Show maps
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont',
                   save=path.join(plot_f, 'map_cont.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'o3n_i', perc=0.2,
                   save=path.join(plot_f, 'map_o3n_i.png'))

# Show maps, for printing
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont', printing=True,
                   save=path.join(plot_f, 'print_map_cont.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'o3n_i', perc=0.2, printing=True,
                   save=path.join(plot_f, 'print_map_o3n_i.png'))

# Show maps with fitted Moffat
map_cont, _ = cube_nifs.grab_map_h(maps, 'cont')
map_o3n_i, _ = cube_nifs.grab_map_h(maps, 'o3n_i')
fit_psf_cont, err_psf_cont, = fit_nifs.moffat2d_fit(map_cont,
                                                    pos_guess='center')
fit_psf_o3n_i, err_psf_o3n_i = fit_nifs.moffat2d_fit(map_o3n_i,
                                                     pos_guess='center')
fwhms_psf_cont = 2 * np.array(fit_psf_cont[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_cont[7])) - 1) * sps
fwhms_psf_o3n_i = 2 * np.array(fit_psf_o3n_i[4: 6]) * \
    np.sqrt((2 ** (1 / fit_psf_o3n_i[7])) - 1) * sps
fwhm_psf_cont = np.sqrt(fwhms_psf_cont[0] * fwhms_psf_cont[1])
fwhm_psf_o3n_i = np.sqrt(fwhms_psf_o3n_i[0] * fwhms_psf_o3n_i[1])
pa_psf_cont = fit_psf_cont[6]
pa_psf_o3n_i = fit_psf_o3n_i[6]
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont',
                   model=[fit_psf_cont[2], fit_psf_cont[3],
                          (fwhms_psf_cont[0] / sps), (fwhms_psf_cont[1] / sps),
                          np.rad2deg(pa_psf_cont), fwhm_psf_cont],
                   save=path.join(plot_f, 'map_cont_moffat.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'o3n_i', perc=0.2,
                   model=[fit_psf_o3n_i[2], fit_psf_o3n_i[3],
                          (fwhms_psf_o3n_i[0] / sps),
                          (fwhms_psf_o3n_i[1] / sps),
                          np.rad2deg(pa_psf_o3n_i), fwhm_psf_o3n_i],
                   save=path.join(plot_f, 'map_o3n_i_moffat.png'))

# Save profiles
profile_cont = fit_psf_cont[2:]
profile_o3n_i = fit_psf_o3n_i[2:]
err_cont = err_psf_cont[2:]
err_o3n_i = err_psf_o3n_i[2:]
profiles = {'band': 'H', 'sps': sps, 'tag': ['cont', 'o3n_i'],
            'name': ["Continuum", "[OIII]"],
            'profile': np.stack((np.array(profile_cont),
                                 np.array(profile_o3n_i))),
            'error': np.stack((np.array(err_cont), np.array(err_o3n_i)))}
file_profile = open(path.join(plot_f, 'profiles_h.pkl'), 'wb')
pickle.dump(profiles, file_profile)
file_profile.close()

# Show radial profiles
map_cont, _ = cube_nifs.grab_map_h(maps, 'cont')
map_o3n_i, _ = cube_nifs.grab_map_h(maps, 'o3n_i')
map_cont_center_where = np.where(map_cont == np.max(map_cont))
map_cont_center = [map_cont_center_where[1][0], map_cont_center_where[0][0]]
model_data_cont = [(fit_psf_cont[0] / (sps ** 2)), np.mean(fit_psf_cont[4: 6]),
                   fit_psf_cont[7], (fit_psf_cont[1] / (sps ** 2)),
                   fwhm_psf_cont]
model_data_o3n_i = [(fit_psf_o3n_i[0] / (sps ** 2)),
                    np.mean(fit_psf_o3n_i[4: 6]), fit_psf_o3n_i[7],
                    (fit_psf_o3n_i[1] / (sps ** 2)), fwhm_psf_o3n_i]
cube_nifs.plot_rad_map(maps, sps, map_cont_center, 'cont', model_type='moffat',
                       model_data=model_data_cont,
                       y_label=("Flux density (" +
                                params_nifs.flux_dens_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_cont.png'))
cube_nifs.plot_rad_map(maps, sps, map_cont_center, 'o3n_i',
                       model_type='moffat', model_data=model_data_o3n_i,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_o3n_i.png'))

# Show radial profiles, for printing
cube_nifs.plot_rad_map(maps, sps, map_cont_center, 'cont', model_type='moffat',
                       model_data=model_data_cont, zoom=max_r,
                       y_label=("Flux density (" +
                                params_nifs.flux_dens_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_cont.png'))
cube_nifs.plot_rad_map(maps, sps, map_cont_center, 'o3n_i',
                       model_type='moffat', model_data=model_data_o3n_i,
                       zoom=max_r,
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       fwhm=False, legend=False,
                       save=path.join(plot_f, 'print_rad_o3n_i.png'))

# Calculate fitted integrals
print("\nAnalyzing integrated fluxes ...")
integr_cont = fit_nifs.moffat_integr(fit_psf_cont)
integr_o3n_i = fit_nifs.moffat_integr(fit_psf_o3n_i)

# Show the fitted integrals
line1 = "   Continuum fitted integral: {0:.3e} erg/s/cm^2/um".format(
    integr_cont)
line2 = "   Narrow [OIII] fitted integral: {0:.3e} erg/s/cm^2".format(
    integr_o3n_i)
print(line1)
print(line2)

# Save the fitted integrals
file_txt = open(path.join(plot_f, 'spectr_integr.txt'), 'w')
file_txt.write(line1 + "\n")
file_txt.write(line2 + "\n")
file_txt.close()

# Show azimuthal profiles
beam_az_cont = np.array([[map_cont_center[0], map_cont_center[1],
                          (dist_range[0] / sps)],
                         [map_cont_center[0], map_cont_center[1],
                          (dist_range[1] / sps)]])
cube_nifs.plot_map(maps, sps, sp_ticks, 'cont', beam_circ=beam_az_cont,
                   save=path.join(plot_f, 'map_az_cont.png'))
cube_nifs.plot_map(maps, sps, sp_ticks, 'o3n_i', beam_circ=beam_az_cont,
                   save=path.join(plot_f, 'map_az_o3n_i.png'))
cube_nifs.plot_az(maps, sps, map_cont_center, 'cont', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_dens_range_az,
                  y_label=("Average flux density\n(" +
                           params_nifs.flux_dens_spat_unit + ")"),
                  save=path.join(plot_f, 'az_cont.png'))
cube_nifs.plot_az(maps, sps, map_cont_center, 'o3n_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), save=path.join(plot_f, 'az_o3n_i.png'))

# Show azimuthal profiles, for printing
cube_nifs.plot_az(maps, sps, map_cont_center, 'cont', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_dens_range_az,
                  y_label=("Average flux density\n(" +
                           params_nifs.flux_dens_spat_unit + ")"),
                  printing=True,
                  save=path.join(plot_f, 'print_az_cont.png'))
cube_nifs.plot_az(maps, sps, map_cont_center, 'o3n_i', n_slices=n_slices,
                  dist_range=dist_range, range_flux=flux_range_az,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"), printing=True,
                  save=path.join(plot_f, 'print_az_o3n_i.png'))

# Save maps as FITS files
hdu_cont = fits.PrimaryHDU(map_cont)
hdu_o3n_i = fits.PrimaryHDU(map_o3n_i)
hdu_cont.writeto(path.join(fits_f, 'map_cont.fits'), overwrite=True)
hdu_o3n_i.writeto(path.join(fits_f, 'map_o3n_i.fits'), overwrite=True)

# Convolve maps
print("\nAnalyzing convolved maps ...")
maps_conv = cube_nifs.convolve_maps(maps, conv_spx)

# Show convolved maps
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'cont',
                   save=path.join(plot_f, 'map_cont_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'o3n_i',
                   save=path.join(plot_f, 'map_o3n_i_conv.png'))

# Show convolved peak radial profiles
map_cont_conv, _ = cube_nifs.grab_map_h(maps_conv, 'cont')
map_o3n_i_conv, _ = cube_nifs.grab_map_h(maps_conv, 'o3n_i')
map_cont_center_where_conv = np.where(map_cont_conv == np.max(map_cont_conv))
map_cont_center_conv = [map_cont_center_where_conv[1][0],
                        map_cont_center_where_conv[0][0]]
cube_nifs.plot_rad_map(maps_conv, sps, map_cont_center_conv, 'cont',
                       y_label=("Flux density (" +
                                params_nifs.flux_dens_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_cont_conv.png'))
cube_nifs.plot_rad_map(maps_conv, sps, map_cont_center_conv, 'o3n_i',
                       y_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       save=path.join(plot_f, 'rad_o3n_i_conv.png'))

# Show convolved peak azimuthal profiles
beam_az_cont_conv = np.array([[map_cont_center_conv[0],
                               map_cont_center_conv[1], (dist_range[0] / sps)],
                              [map_cont_center_conv[0],
                               map_cont_center_conv[1],
                               (dist_range[1] / sps)]])
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'cont',
                   beam_circ=beam_az_cont_conv,
                   save=path.join(plot_f, 'map_az_cont_conv.png'))
cube_nifs.plot_map(maps_conv, sps, sp_ticks, 'o3n_i',
                   beam_circ=beam_az_cont_conv,
                   save=path.join(plot_f, 'map_az_o3n_i_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_cont_center_conv, 'cont',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_dens_range_az,
                  y_label=("Average flux density\n(" +
                           params_nifs.flux_dens_spat_unit + ")"),
                  save=path.join(plot_f, 'az_cont_conv.png'))
cube_nifs.plot_az(maps_conv, sps, map_cont_center_conv, 'o3n_i',
                  n_slices=n_slices, dist_range=dist_range,
                  range_flux=flux_range_az,
                  y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                           ")"),
                  save=path.join(plot_f, 'az_o3n_i_conv.png'))

# Save convolved maps as FITS files
hdu_cont_conv = fits.PrimaryHDU(map_cont_conv)
hdu_o3n_i_conv = fits.PrimaryHDU(map_o3n_i_conv)
hdu_cont_conv.writeto(path.join(fits_f, 'map_cont_conv.fits'), overwrite=True)
hdu_o3n_i_conv.writeto(path.join(fits_f, 'map_o3n_i_conv.fits'),
                       overwrite=True)

# Binning with different factors
for i_bin_spx in range(len(bin_spxs)):

    # Bin spaxels
    bin_spx = bin_spxs[i_bin_spx]
    print("\nAnalyzing binned maps ({0}x{0}) ...".format(bin_spx))
    data_bin = cube_nifs.bin_cube(data_clean, bin_spx)
    data_bin_coll = cube_nifs.collapse_cube(data_bin)
    data_bin_coll *= (h_range[1] - h_range[0])

    # Show image binned by spaxels
    data_bin_coll_plot = data_bin_coll / ((sps * bin_spx) ** 2)
    cube_nifs.plot_img(data_bin_coll_plot, (sps * bin_spx), sp_ticks,
                       c_label=("Flux (" + params_nifs.flux_spat_unit + ")"),
                       title="Collapsed cube, binned by spaxels",
                       save=path.join(plot_f,
                                      'coll_bin_{0}.png'.format(bin_spx)))

    # Map binned lines
    maps_bin = fit_nifs.fit_map_h(wvl_clean, data_bin, fit_range, bb_0_range,
                                  fit_b=fit_b_map, n_spaces=3)

    # Show binned maps
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'cont',
                       save=path.join(plot_f,
                                      'map_cont_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'o3n_i',
                       save=path.join(plot_f,
                                      'map_o3n_i_bin_{0}.png'.format(bin_spx)))

    # Show binned peak radial profiles
    map_cont_bin, _ = cube_nifs.grab_map_h(maps_bin, 'cont')
    map_o3n_i_bin, _ = cube_nifs.grab_map_h(maps_bin, 'o3n_i')
    map_cont_center_where_bin = np.where(map_cont_bin ==
                                         np.max(map_cont_bin))
    map_cont_center_bin = [map_cont_center_where_bin[1][0],
                           map_cont_center_where_bin[0][0]]
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_cont_center_bin,
                           'cont',
                           y_label=("Flux density (" +
                                    params_nifs.flux_dens_spat_unit + ")"),
                           save=path.join(plot_f,
                                          'rad_cont_bin_{0}.png'.format(
                                              bin_spx)))
    cube_nifs.plot_rad_map(maps_bin, (sps * bin_spx), map_cont_center_bin,
                           'o3n_i',
                           y_label=("Flux (" + params_nifs.flux_spat_unit +
                                    ")"),
                           save=path.join(plot_f,
                                          'rad_o3n_i_bin_{0}.png'.format(
                                              bin_spx)))

    # Show binned peak azimuthal profiles
    beam_az_cont_bin = np.array([[map_cont_center_bin[0],
                                  map_cont_center_bin[1],
                                  (dist_range[0] / (sps * bin_spx))],
                                 [map_cont_center_bin[0],
                                  map_cont_center_bin[1],
                                  (dist_range[1] / (sps * bin_spx))]])
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'cont',
                       beam_circ=beam_az_cont_bin,
                       save=path.join(plot_f,
                                      'map_az_cont_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_map(maps_bin, (sps * bin_spx), sp_ticks, 'o3n_i',
                       beam_circ=beam_az_cont_bin,
                       save=path.join(plot_f,
                                      'map_az_o3n_i_bin_{0}.png'.format(
                                          bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_cont_center_bin, 'cont',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_dens_range_az_bin,
                      y_label=("Average flux density\n(" +
                               params_nifs.flux_dens_spat_unit + ")"),
                      save=path.join(plot_f,
                                     'az_cont_bin_{0}.png'.format(bin_spx)))
    cube_nifs.plot_az(maps_bin, (sps * bin_spx), map_cont_center_bin, 'o3n_i',
                      n_slices=n_slices, dist_range=dist_range,
                      range_flux=flux_range_az_bin,
                      y_label=("Average flux\n(" + params_nifs.flux_spat_unit +
                               ")"),
                      save=path.join(plot_f,
                                     'az_o3n_i_bin_{0}.png'.format(bin_spx)))

    # Save binned maps as FITS files
    hdu_cont_bin = fits.PrimaryHDU(map_cont_bin)
    hdu_o3n_i_bin = fits.PrimaryHDU(map_o3n_i_bin)
    hdu_cont_bin.writeto(path.join(fits_f,
                                   'map_cont_bin_{0}.fits'.format(bin_spx)),
                         overwrite=True)
    hdu_o3n_i_bin.writeto(path.join(fits_f,
                                    'map_o3n_i_bin_{0}.fits'.format(bin_spx)),
                          overwrite=True)

    # Show grid of binned spectra
    wvl_bin_bin_test, _ = cube_nifs.bin_spectrum(wvl_clean, data_bin[:, 0, 0],
                                                 bin_wvl)
    data_bin_bin = np.zeros([len(wvl_bin_bin_test), data_bin.shape[1],
                             data_bin.shape[2]])
    wvl_bin_bin = []

    for i_row in range(data_bin.shape[1]):
        for i_col in range(data_bin.shape[2]):
            wvl_bin_bin, data_bin_bin[:, i_row, i_col] = \
                cube_nifs.bin_spectrum(wvl_clean, data_bin[:, i_row, i_col],
                                       bin_wvl)

    y1 = int((data_bin_bin.shape[1] - bin_grids) / 2)
    y2 = int((data_bin_bin.shape[1] + bin_grids) / 2)
    x1 = int((data_bin_bin.shape[2] - bin_grids) / 2)
    x2 = int((data_bin_bin.shape[2] + bin_grids) / 2)
    data_bin_bin_grid = data_bin_bin[:, y1: y2, x1: x2]
    maps_bin_grid = maps_bin[y1: y2, x1: x2]
    cube_nifs.grid_spectrum(wvl_bin_bin, data_bin_bin_grid,
                            fit_mod=maps_bin_grid, zoom=h_range_grid,
                            i_range=i_range_grid,
                            title="Grid of fitted binned spectra",
                            save=path.join(plot_f,
                                           ('grid_spectr_fit_bin_{0}' +
                                            '.png').format(bin_spx)))

print("\nProgram terminated\n")
