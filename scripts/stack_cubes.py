"""Stack NIFS data cubes of single exposures of 084933A, taken during different
nights.

References:
    [ref1] Greisen et Calabretta, 2002
"""


import glob
import pickle
from os import makedirs, pardir, path
import warnings

from astropy.io import fits
import numpy as np
from scipy import ndimage


# Data parameters
root_folder = '/Volumes/Astro/Data/NIFS/'  # Root data folder
target_folder = '084933A'  # Target subfolder
data_combine = [[2017, 'H', '084933a_17a', '20170407_obs57', 1, 0],
                [2017, 'K', '084933a_17a', '20170407_obs49', 1, 0],
                [2017, 'K', '084933a_17a', '20170410_obs71', 1, 0],
                [2019, 'K', '084933a_19b', '20200103_obs66', 1, 0],
                [2021, 'K', '084933a_21b', '20220224_obs86', 2, 1],
                [2021, 'K', '084933a_21b', '20220226_obs145', 3, 2]]
# Data subfolder ([year, band, semester, observation, stack batch,
# offset file])

# Analysis parameters
zoom_wvl = False  # Zoom the cubes in the wavelength directions
stack_range_list = [[1.55, 1.75], [2.05, 2.35]]  # Range of wavelengths to
# stack [H, K] (um)

# Graphic parameters
sp_ticks = 0.5  # Distance of the spaxel axes' ticks (")

# Load excluded files
print("\nProgram started")
exclude_file = open(path.join(root_folder, 'exclude.lst'), 'r')
exclude_lines = exclude_file.readlines()
exclude_file.close()
exclude_lines = [line.strip('\n') for line in exclude_lines]

# Analyze all data
wvls = np.unique([data_combine[i][1] for i in range(len(data_combine))])

for band in wvls:

    # Find data
    print("- {0} band".format(band))

    if band == 'H':
        stack_range = stack_range_list[0]
    else:
        stack_range = stack_range_list[1]

    obss = [i for i in data_combine if i[1] == band]
    batches = np.unique([obss[i][4] for i in range(len(obss))])

    # Analyze batches
    for batch in batches:

        # Find data
        print("   - Batch #{0}".format(batch))
        obsss = [i for i in obss if i[4] == batch]
        years = np.array([i[0] for i in obsss])
        years_unique = np.unique(years)
        obs_extend = [obsss]

        for year in years_unique:
            obs_extend.append([i for i in obsss if i[0] == year])

        for i_obs_extend in range(len(obs_extend)):

            if i_obs_extend != 0 or batch == 1:

                if i_obs_extend == 0:
                    str_year = "All epochs"
                else:
                    str_year = years_unique[i_obs_extend - 1]

                print("      - {0}".format(str_year))
                obs_tmp = obs_extend[i_obs_extend]
                find_files = []
                find_files_unc = []
                find_offset = []
                fold = ''
                fold_unc = ''

                for obs_i in obs_tmp:
                    fold = path.join(root_folder, obs_i[2], target_folder,
                                     'Merged_telCorAndFluxCalibrated',
                                     obs_i[3])
                    fold_unc = path.join(root_folder, obs_i[2], target_folder,
                                         'Merged_telluricCorrected', obs_i[3])
                    find_files.extend(glob.glob(path.join(fold,
                                                          'factfbrsn*.fits')))
                    find_files_unc.extend(glob.glob(path.join(
                        fold_unc, 'actfbrsn*.fits')))

                    if batch == 1:
                        find_offset.extend(glob.glob(path.join(path.abspath(
                            path.join(fold, pardir, pardir, pardir)), 'offset',
                            ('offset_' + band + '_*.pkl'))))
                    else:
                        find_offset.extend([path.join(path.abspath(
                            path.join(fold, pardir, pardir, pardir)), 'offset',
                            ('offset_' + band + '_' + str(obs_i[5]) +
                             '.pkl'))])

                find_offset = np.unique(find_offset)

                if i_obs_extend == 0:
                    save_folder = path.join(root_folder, 'stack')
                    save_folder_plots = path.join(save_folder, 'plots')
                else:
                    save_folder = path.join(path.abspath(path.join(fold,
                                                                   pardir,
                                                                   pardir,
                                                                   pardir)),
                                            'stack')
                    save_folder_plots = path.join(path.abspath(path.join(
                        save_folder, pardir)), 'plots', 'stack')

                if not path.exists(save_folder):
                    makedirs(save_folder)

                if not path.exists(save_folder_plots):
                    makedirs(save_folder_plots)

                # Exclude files
                find_lines = [path.basename(line) for line in find_files]
                for exclude_line in exclude_lines:
                    if exclude_line in find_lines:
                        exclude_idx = find_lines.index(exclude_line)
                        find_files = np.delete(find_files, exclude_idx)
                        find_files_unc = np.delete(find_files_unc, exclude_idx)

                # Load offset
                offset_x = []
                offset_y = []

                for i_offset in find_offset:
                    file_in = open(i_offset, 'rb')
                    offset = pickle.load(file_in)
                    file_in.close()
                    offset_x = np.append(offset_x, offset[0])
                    offset_y = np.append(offset_y, offset[1])

                offset_x -= offset_x[0]
                offset_y -= offset_y[0]

                # Load exposures
                n_files = len(find_files)
                wvl_shift = np.zeros(n_files)
                wvl_zoom = np.zeros(n_files)
                data = []
                data_unc = []
                sps = 0
                wvl_coeff_1_0 = 0
                wvl_coeff_2_0 = 0
                wvl_coeff_3_0 = 0

                for i_file in range(n_files):

                    # Load file
                    print(
                        "\r         - Loading exposure # {0}/{1} ...".format((
                            i_file + 1), n_files), end="")
                    folder_file = find_files[i_file]
                    folder_file_unc = find_files_unc[i_file]
                    hdul = fits.open(folder_file)
                    header_1 = hdul[1].header
                    data = hdul[1].data
                    hdul.close()
                    hdul = fits.open(folder_file_unc)
                    data_unc = hdul[1].data
                    hdul.close()
                    data_shape = data.shape
                    sps = header_1['CD1_1']

                    # Measure wavelengths
                    wvl_coeff_1 = header_1['CRPIX3']
                    wvl_coeff_2 = header_1['CD3_3']
                    wvl_coeff_3 = header_1['CRVAL3']
                    wvl_shift[i_file] = ((1 - wvl_coeff_1) * wvl_coeff_2) + \
                        wvl_coeff_3  # [ref1]
                    wvl_zoom[i_file] = wvl_coeff_2

                    if i_file == 0:
                        wvl_coeff_1_0 = wvl_coeff_1
                        wvl_coeff_2_0 = wvl_coeff_2
                        wvl_coeff_3_0 = wvl_coeff_3

                print()

                # Calculate wavelength offset
                new_wvl_shift = (wvl_shift - wvl_shift[0]) / wvl_zoom
                new_wvl_zoom = wvl_zoom / wvl_zoom[0]

                # Shift and zoom images
                zoomed = np.zeros([n_files, data.shape[0], data.shape[1],
                                   data.shape[2]])
                zoomed_unc = np.zeros([n_files, data_unc.shape[0],
                                       data_unc.shape[1], data_unc.shape[2]])

                for i_file in range(n_files):

                    # Load file
                    print(
                        "\r         - Stacking exposure # {0}/{1} ...".format((
                            i_file + 1), n_files), end="")
                    folder_file = find_files[i_file]
                    folder_file_unc = find_files_unc[i_file]
                    hdul = fits.open(folder_file)
                    data = hdul[1].data
                    hdul.close()
                    hdul = fits.open(folder_file_unc)
                    data_unc = hdul[1].data
                    hdul.close()

                    # Shift and zoom image
                    shifted = ndimage.interpolation.shift(
                        data, [new_wvl_shift[i_file], -offset_y[i_file],
                               -offset_x[i_file]], mode='nearest')
                    shifted_unc = ndimage.interpolation.shift(
                        data_unc, [new_wvl_shift[i_file], -offset_y[i_file],
                                   -offset_x[i_file]], mode='nearest')

                    if zoom_wvl:
                        warnings.simplefilter('ignore')
                        zoomed[i_file, :, :, :] = \
                            ndimage.zoom(shifted, [(1 / new_wvl_zoom[i_file]),
                                                   1, 1], mode='nearest')
                        zoomed_unc[i_file, :, :, :] = \
                            ndimage.zoom(shifted_unc,
                                         [(1 / new_wvl_zoom[i_file]), 1, 1],
                                         mode='nearest')
                        warnings.simplefilter('default')
                    else:
                        zoomed[i_file, :, :, :] = shifted
                        zoomed_unc[i_file, :, :, :] = shifted_unc

                # Stack images
                stack = np.median(zoomed, axis=0)
                stack_median_unc = np.median(zoomed_unc, axis=0)
                stack_sum_unc = np.sum(zoomed_unc, axis=0)
                stack_norm_unc = np.median(stack_sum_unc / stack_median_unc)
                stack_unc = stack_median_unc * stack_norm_unc

                # Save data cube
                print("\n         - Saving data cube ...")
                cube_in = fold + '_merged.fits'
                cube_in_unc = fold_unc + '_merged.fits'

                if i_obs_extend == 0:
                    stack_suff = ''
                else:
                    stack_suff = '_' + str(batch)

                cube_out = path.join(save_folder, ('stack_' + band + stack_suff
                                                   + '.fits'))
                cube_out_unc = path.join(save_folder,
                                         ('stack_' + band + stack_suff +
                                          '_uncalib.fits'))
                hdul = fits.open(cube_in)
                hdul[1].data = stack
                hdul.writeto(cube_out, overwrite=True)
                hdul.close()
                hdul = fits.open(cube_in_unc)
                hdul[1].data = stack_unc
                hdul.writeto(cube_out_unc, overwrite=True)
                hdul.close()

print("Program terminated\n")
