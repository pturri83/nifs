"""Analyze NIFS H and K band maps profiles.
"""


from os import path
import pickle

from source import cube_nifs


# Data parameters
folder = '/Volumes/Astro/Data/NIFS/stack/plots'  # Profiles parent folder
exclude_k = [1]  # Excluded K profiles

# Graphic parameters
max_dist = 1  # Distance of the spaxel axes' ticks (")

# Load data
print("\nProgram started")
print("Loading data ...")
file_h = open(path.join(folder, 'H', 'spectrum', 'profiles_h.pkl'), 'rb')
file_k = open(path.join(folder, 'K', 'spectrum', 'profiles_k.pkl'), 'rb')
profiles_h = pickle.load(file_h)
profiles_k = pickle.load(file_k)
file_h.close()
file_k.close()

# Print
print("Printing ...\n")
cube_nifs.print_fit(profiles_h, profiles_k, save=path.join(folder, 'prof'))

# Plot
print("Plotting ...\n")
cube_nifs.plot_fwhm(profiles_h, profiles_k,
                    save=path.join(folder, 'prof_fwhm.png'))
cube_nifs.plot_fwhm(profiles_h, profiles_k, exclude_k=exclude_k, printing=True,
                    save=path.join(folder, 'print_prof_fwhm.png'))
cube_nifs.plot_alpha(profiles_h, profiles_k,
                     save=path.join(folder, 'prof_alpha.png'))
cube_nifs.plot_alpha(profiles_h, profiles_k, exclude_k=exclude_k,
                     printing=True,
                     save=path.join(folder, 'print_prof_alpha.png'))
cube_nifs.plot_pos(profiles_h, profiles_k,
                   save=path.join(folder, 'prof_pos.png'))
cube_nifs.plot_pos(profiles_h, profiles_k, exclude_k=exclude_k, printing=True,
                   save=path.join(folder, 'print_prof_pos.png'))
print("\nProgram terminated\n")
